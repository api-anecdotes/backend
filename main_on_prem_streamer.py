import base64
from flask import Flask, request
import os
import sys
import logging
from OnPrem.streamer import Streamer


logging.basicConfig(level=10)
app = Flask(__name__)

# USE THESE LINES TO CREATE: STORAGE STIGGER -> TOPIC MESSAGE -> CLOUD RUN INVOKATION
# gcloud projects add-iam-policy-binding revv-poc-1574867026155 --member=serviceAccount:service-1063241535274@gcp-sa-pubsub.iam.gserviceaccount.com --role=roles/iam.serviceAccountTokenCreator
# gcloud iam service-accounts create cloud-run-pubsub-invoker --display-name "Cloud Run Pub/Sub Invoker"
# gcloud run services add-iam-policy-binding on-prem-streamer --member=serviceAccount:cloud-run-pubsub-invoker@revv-poc-1574867026155.iam.gserviceaccount.com --role=roles/run.invoker
# gcloud pubsub subscriptions create inbox-storage --topic on-prem-inbox-storage --push-endpoint=https://on-prem-streamer-nipvgsvkfa-ez.a.run.app/ --push-auth-service-account=cloud-run-pubsub-invoker@revv-poc-1574867026155.iam.gserviceaccount.com
# gsutil notification create -f json -e OBJECT_METADATA_UPDATE -t on-prem-inbox-storage gs://poc-in-data-storage

# LOCALY TEST WITH
# requests.post('http://localhost:8082', json={'message': {'data': {}, 'attributes': {'bucketId': 'poc-in-data-storage', 'objectId': 'test_csv_1.csv'}}})

@app.route('/', methods=['POST'])
def index():
	envelope = request.get_json()
	if not envelope:
		msg = 'no Pub/Sub message received'
		logging.debug(f'error: {msg}')
		return f'Bad Request: {msg}', 400

	if not isinstance(envelope, dict) or 'message' not in envelope:
		msg = 'invalid Pub/Sub message format'
		logging.debug(f'error: {msg}')
		return f'Bad Request: {msg}', 400

	pubsub_message = envelope['message']

	if isinstance(pubsub_message, dict) and 'data' in pubsub_message:
		attributes = pubsub_message.get('attributes', {})
		bucket_id = attributes.get('bucketId')
		object_id = attributes.get('objectId')

		streamer = Streamer(bucket_id)
		streamer.handle_raw_data(object_id)

	logging.debug('Done')
	return ('', 204)


if __name__ == "__main__":
	app.run(debug=True,host='0.0.0.0',port=int(os.environ.get('PORT', 8082)))