import logging
import traceback

from collections import defaultdict

from Tagging.tags import TAG_CATEGORIES, GenericKeys
from Tagging.data_origin import get_dict_with_origin
from Tagging.fb_categories import category_to_tags
from Tagging.tags import InterestTags


def get_all_tags():
	all_tags = defaultdict(defaultdict)
	for tag_category in TAG_CATEGORIES:
		all_tags[tag_category.__name__] = defaultdict(lambda : defaultdict(int))
	return all_tags

def merge_all_tags(current_tags, new_tags):
	if current_tags == {}:
		current_tags = get_all_tags()
	for tag_category in TAG_CATEGORIES:
		category = tag_category.__name__
		for key, value in new_tags[category].items():
			current_value = current_tags[category].get(key, None)

			if current_value is None:
				current_tags[category][key] = value
			else:
				if type(current_value['data']) is list:
					current_tags[category][key]['data'].extend(value['data'])
				elif type(current_value['data']) is int:
					current_tags[category][key]['data'] += value['data']
				# TODO: check if a dict scenario is possible, and whether "update" is the right way to handle it
				# elif type(current_value) is dict:
				# 	current_tags[category][key].update(value)
				else:
					traceback.print_stack()
					logging.error('Key-Value mismatches current value: {}-{}:{}'.format(key, value, current_value))

				current_tags[category][key]['data_origin'] = {**current_tags[category][key]['data_origin'], **value['data_origin']}
	return current_tags

def merge_generic_info(current_info, new_info):
	# Override age range, gender, frequency, budget and home-country

	if GenericKeys.HomeCountry.value in new_info:
		current_info[GenericKeys.HomeCountry.value] = new_info[GenericKeys.HomeCountry.value]

	if GenericKeys.AgeRange.value in new_info:
		current_info[GenericKeys.AgeRange.value] = new_info[GenericKeys.AgeRange.value]

	if GenericKeys.Gender.value in new_info:
		current_info[GenericKeys.Gender.value] = new_info[GenericKeys.Gender.value]

	if GenericKeys.Frequency.value in new_info:
		current_info[GenericKeys.Frequency.value] = new_info[GenericKeys.Frequency.value]

	if GenericKeys.Budget.value in new_info:
		current_info[GenericKeys.Budget.value] = new_info[GenericKeys.Budget.value]

	if GenericKeys.HotLead.value in new_info:
		if GenericKeys.HotLead.value in current_info:
			current_info[GenericKeys.HotLead.value]['data'].extend(new_info[GenericKeys.HotLead.value]['data'])
			current_info[GenericKeys.HotLead.value]['data'] = list(set(current_info[GenericKeys.HotLead.value]['data']))
			current_info[GenericKeys.HotLead.value]['data_origin'] = \
				{**current_info[GenericKeys.HotLead.value]['data_origin'],
				 **new_info[GenericKeys.HotLead.value]['data_origin']}
		else:
			current_info[GenericKeys.HotLead.value] = new_info[GenericKeys.HotLead.value]

	if GenericKeys.Languages.value in new_info:
		if GenericKeys.Languages.value in current_info:
			current_info[GenericKeys.Languages.value]['data'].extend(new_info[GenericKeys.Languages.value]['data'])
			current_info[GenericKeys.Languages.value]['data'] = list(set(current_info[GenericKeys.Languages.value]['data']))
			current_info[GenericKeys.Languages.value]['data_origin'] = \
				{**current_info[GenericKeys.Languages.value]['data_origin'],
				 **new_info[GenericKeys.Languages.value]['data_origin']}
		else:
			current_info[GenericKeys.Languages.value] = new_info[GenericKeys.Languages.value]
	return current_info

def get_flat_tags(tags, all_keys=False, categories_whitelist=None, categories_blacklist=None):
	flat_tags = []
	
	categories = TAG_CATEGORIES
	if categories_whitelist != None:
		categories = categories_whitelist

	if categories_blacklist != None:
		categories = list(filter(lambda x: x not in categories_blacklist, categories))

	for category in categories:
		for key in category:
			# TODO: and != 0 ??
			if all_keys or tags.get(category.__name__, {}).get(key.value) != None:
				flat_tags.append(key.value)

	return flat_tags

def build_tags_with_origin(tags, data_origin):
	tagged_dict = get_all_tags()
	for category in TAG_CATEGORIES:
		for key in category:
			if tags.get(category.__name__, {}).get(key.value) != None:
				tagged_dict[category.__name__] = get_dict_with_origin(tags.get(category.__name__, {}), data_origin)

	return tagged_dict

def build_generics_with_origin(generics, data_origin):
	return get_dict_with_origin(generics, data_origin)

def increment_tag(tags, category_key, data_origin, delta=None):
	if delta is None:
		delta = 1
	tags[category_key.__class__.__name__][category_key.value]['data'] += delta
	if tags[category_key.__class__.__name__][category_key.value]['data_origin'] != 0:
		tags[category_key.__class__.__name__][category_key.value]['data_origin'] = \
			{**tags[category_key.__class__.__name__][category_key.value]['data_origin'], **data_origin}
	else:
		tags[category_key.__class__.__name__][category_key.value]['data_origin'] = data_origin

def override_tag(tags, category_key, data_origin, data):
	tags[category_key.__class__.__name__][category_key.value]['data'] = data
	if tags[category_key.__class__.__name__][category_key.value]['data_origin'] != 0:
		tags[category_key.__class__.__name__][category_key.value]['data_origin'] = \
			{**tags[category_key.__class__.__name__][category_key.value]['data_origin'], **data_origin}
	else:
		tags[category_key.__class__.__name__][category_key.value]['data_origin'] = data_origin

def add_tags_from_item_list_of_texts(item_text_list, text_per_tag, tag_category, tags, data_origin, description='no description'):
	for tag in tag_category:
		for text in item_text_list:
			matches = [tag_text for tag_text in text_per_tag[tag] if tag_text in text]
			if len(matches) > 0:
				# if there's a match - increment the counter of this interest and move on to next interest tag
				logging.debug("increment tag '{tag}' due to '{description}'".format(tag=tag.value, description=description))
				increment_tag(tags, tag, data_origin)
				break

def add_tags_by_categories(category_list, tags, data_origin, description='no description'):
	for category in category_list:
		for tag in category_to_tags.get(category, []):
			logging.debug("increment tag '{tag}' due to '{description}'".format(tag=tag.value, description=description))
			increment_tag(tags, tag, data_origin)

		# special cases - 'contains X'
		if 'church' in category:
			logging.debug("increment tag '{tag}' due to '{description}'".format(tag=InterestTags.temple.value, description=description))
			increment_tag(tags, InterestTags.temple, data_origin)
			continue
		if 'restaurant' in category:
			logging.debug("increment tag '{tag}' due to '{description}'".format(tag=InterestTags.restaurant.value, description=description))
			increment_tag(tags, InterestTags.restaurant, data_origin)
			continue
		if 'museum' in category:
			logging.debug("increment tag '{tag}' due to '{description}'".format(tag=InterestTags.museum.value, description=description))
			increment_tag(tags, InterestTags.museum, data_origin)
			continue