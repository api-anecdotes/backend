import logging
from abc import ABC, abstractmethod
from collections import defaultdict

import Utils.cloud as cloud_utils
import Utils.consts as consts


class GenericdWidget(ABC):
	def __init__(self, widget_type, query_fields, title):
		self._widget_type = widget_type
		self._query_fields = query_fields
		self._title = title
		self._widget_data = None

	def prepare_data(self):
		# Read cached results
		cached_result, labels = cloud_utils.get_bigquery_cached_result(self._title)

		if cached_result is None:
			# Create query
			query = self._craft_bigquery_query()
			
			# Execute query
			logging.debug('Execute BQ query: {}'.format(query))
			raw_query_data = cloud_utils.get_bigquery_result(query)

			# Process results
			self._widget_data, self._labels = self._process_query_data(raw_query_data)
			cloud_utils.BatchFirestoreWrite().update_bigquecy_cached_result(self._title, self._widget_data, self._labels, query)
		
		else:
			self._widget_data = cached_result
			self._labels = labels

	def get_widget_view_dict(self):
		return {
			'type': self._widget_type,
			'title': self._title,
			'data': self._widget_data,
			'labels': self._labels,
		}

	@abstractmethod
	def _craft_bigquery_query(self):
		pass

	@abstractmethod
	def _process_query_data(self, raw_query_data):
		pass


class PieChartWidget1(GenericdWidget):
	def __init__(self, query_fields, title, field_filter):
		super().__init__('PieChart', query_fields, title)
		self._field_filter = field_filter

	def _craft_bigquery_query(self):
		counters = []
		for field, name in self._query_fields.items():
			counters.append(consts.GENERIC_BQ_QUERY_COUNT.format(field=field, name=name.replace(' ', '_')))
		str_counters_query = ''.join(counters)
		
		query = consts.GENERIC_BQ_QUERY_PIE_1_CHART.format(
			counters=str_counters_query,
			filter=self._field_filter)

		return query

	def _process_query_data(self, raw_query_data):
		results = defaultdict(int)
		try:
			for row in raw_query_data:
				for key, value in row.items():
					results[key.replace('_', ' ')] = value

		except Exception as exc:
			logging.info('PieChart widget excepion')
			logging.error(str(exc))

		return list(results.values()), list(results.keys())


class PieChartWidget2(GenericdWidget):
	def __init__(self, query_fields, title, field_filter):
		super().__init__('PieChart', query_fields, title)
		self._field_filter = field_filter

	def _craft_bigquery_query(self):
		counters = '{}, {}'.format(
			self._query_fields,
			consts.GENERIC_BQ_QUERY_COUNT.format(field=self._query_fields, name='counter'))
		
		query = consts.GENERIC_BQ_QUERY_PIE_2_CHART.format(
			fields_counters=counters,
			filter=self._field_filter,
			grouping_field=self._query_fields)

		return query

	def _process_query_data(self, raw_query_data):
		results = defaultdict(int)
		try:
			for row in raw_query_data:
				results[row[self._query_fields]] = row['counter']

		except Exception as exc:
			logging.info('PieChart widget excepion')
			logging.error(str(exc))

		return list(results.values()), list(results.keys())


class StackBar100Widget1(GenericdWidget):
	def __init__(self, query_fields, title, y_axis):
		super().__init__('StackBar100', query_fields, title)
		self._y_axis = y_axis

	def _craft_bigquery_query(self):
		counters = []
		for field, name in self._query_fields.items():
			counters.append(consts.GENERIC_BQ_QUERY_COUNT.format(field=field, name=name.replace(' ', '_')))
		str_counters_query = ''.join(counters)
		
		query = consts.GENERIC_BQ_QUERY_SB100_1_CHART.format(
			selector=self._y_axis,
			counters=str_counters_query,
			group_name=self._y_axis)

		return query

	def _process_query_data(self, raw_query_data):
		results = {}
		try:
			for row in raw_query_data:
				main_key = row[self._y_axis]
				
				if main_key is None or main_key == 0 or main_key == '0':
					continue

				other_keys = list(row.keys())
				other_keys.remove(self._y_axis)
				result = {}

				for key in other_keys:
					result[key] = row[key]

				results[main_key] = result

		except Exception as exc:
			logging.info('StackBar1001 widget excepion')
			logging.error(str(exc))

		return results, list(results.keys())

class StackBar100Widget2(GenericdWidget):
	def __init__(self, query_fields, title, filters):
		super().__init__('StackBar100', query_fields, title)
		self._filters = filters

	def _craft_bigquery_query(self):
		counters = []
		for field, name in self._query_fields.items():
			counters.append(consts.GENERIC_BQ_QUERY_COUNT.format(field=field, name=name.replace(' ', '_')))
		str_counters_query = ''.join(counters)
		
		sub_queries = []
		for query_filter in self._filters:
			sub_queries.append(consts.GENERIC_BQ_QUERY_SB100_2_CHART.format(
				name=query_filter,
				counters=str_counters_query,
				columns=','.join(list(self._query_fields.keys())),
				filter=query_filter))

		query = consts.GENERIC_BQ_UNION_ALL.join(sub_queries)
		return query

	def _process_query_data(self, raw_query_data):
		results = {}
		try:
			for row in raw_query_data:
				main_key = row['name']
				
				if main_key is None or main_key == 0 or main_key == '0':
					continue

				other_keys = list(row.keys())
				other_keys.remove('name')
				result = {}

				for key in other_keys:
					result[key] = row[key]

				results[main_key] = result

		except Exception as exc:
			logging.info('StackBar1002 widget excepion')
			logging.error(str(exc))

		return results, list(results.keys())

class HeatMapWidget1(GenericdWidget):
	def __init__(self, query_fields, title):
		super().__init__('HeatMap', query_fields, title)

	def _craft_bigquery_query(self):
		x_axis = self._query_fields.get('x', [])
		y_axis = self._query_fields.get('y', [])
		filter_field = self._query_fields.get('filter', '')

		sub_queries = []
		for x_field in x_axis:
			for y_field in y_axis:
				sub_queries.append(consts.GENERIC_BQ_QUERY_CORR_COUNT.format(
					param_1=x_field, param_2=y_field, filter=filter_field))

		inner_query = consts.GENERIC_BQ_UNION_ALL.join(sub_queries)
		
		query = consts.GENERIC_BQ_QUERY_CORR_TABLE.format(table=inner_query)
		return query

	def _process_query_data(self, raw_query_data):
		data_series = defaultdict(defaultdict)

		try:
			for row in raw_query_data:
				if row['name_2'] == row['name_1']:
					data_series[row['name_2']][row['name_1']] = [0, 0]
				else:
					data_series[row['name_2']][row['name_1']] = [row['corr_data'], row['counter']]

		except Exception as exc:
			logging.info('DistributedColumns widget excepion')
			logging.error(str(exc))

		labels = list(data_series.keys())
		labels.sort()
		return data_series, labels