import json
import zlib
import hashlib
import traceback
import logging
import requests
import random
import re

from enum import Enum
from collections import defaultdict
from datetime import datetime, timedelta
from io import StringIO

from google.cloud import storage
from google.cloud import firestore
from google.cloud import bigquery
from google.cloud.firestore_v1 import Increment
from google.cloud.exceptions import NotFound
from google.cloud import pubsub_v1

from Utils.dashboard_data import DashboardData
from Utils.consts import Storage, WriteThreshold, Firestore, USER_AGENT_LIST, PROJECT_NAME

g_db_connector = firestore.Client()
g_storage_connector = storage.Client() 
g_bigquery_client = bigquery.Client()
g_pubsub_client = pubsub_v1.PublisherClient(
	publisher_options = pubsub_v1.types.PublisherOptions(enable_message_ordering=True))


def get_hash(token):
	hash_object = hashlib.sha256(bytes(token.lower(), "utf-8"))
	return hash_object.hexdigest()

########################################
################ Pubsub ################
########################################
def send_message(data, attributes, topic):
	# TODO: compress data!!
	topic_path = g_pubsub_client.topic_path(PROJECT_NAME, topic)
	g_pubsub_client.publish(
        topic_path,
		data=data.encode("utf-8"),
		**attributes
    )

########################################
################ Storage ###############
########################################
def upload_raw_data(blob_name, data, origin, compress=True, bucket_name=None, content_type=None):
	if bucket_name is None:
		bucket_name = Storage.raw_data.value

	bucket = g_storage_connector.bucket(bucket_name)
	blob = bucket.blob(blob_name)

	if compress:
		data = zlib.compress(data, level=9)
		
	if content_type is None:
		content_type = 'application/json'

	if content_type == 'application/json':
		data = json.dumps(data).encode("utf-8")

	blob.upload_from_string(data=data, content_type=content_type)

	metadata = get_metadata(blob_name, bucket_name)
	metadata['origin'] = origin
	update_metadata(blob_name, bucket_name, metadata)

def upload_raw_csv(blob_name, data_frame, origin, compress=False, bucket_name=None):
	buffer = buffer = StringIO()
	data_frame.to_csv(buffer)
	buffer.seek(0)
	value = str.encode(buffer.getvalue()) 

	upload_raw_data(blob_name, value, origin, bucket_name=bucket_name, compress=compress, content_type='text/csv')

def file_exists(blob_name, bucket_name=None):
	if bucket_name is None:
		bucket_name = Storage.raw_data.value
	bucket = g_storage_connector.bucket(bucket_name)
	blob = bucket.blob(blob_name)

	return blob.exists()

def get_metadata(blob_name, bucket_name):
	bucket = g_storage_connector.bucket(bucket_name)
	blob = bucket.get_blob(blob_name)
	if blob is None:
		return {}

	metadata = blob.metadata
	if metadata is None:
		metadata = {}

	return metadata

def update_metadata(blob_name, bucket_name, metadata):
	bucket = g_storage_connector.bucket(bucket_name)
	blob = bucket.get_blob(blob_name)
	if blob is None:
		return

	blob.metadata = metadata
	blob.update()

########################################
############### BigQuery ###############
########################################
def get_bigquery_result(query_str):
	job = g_bigquery_client.query(query_str)
	return job.result()

########################################
########### Firestore - read ###########
########################################
def get_traveler_local_id(identifiers, fallback_to_personal_ids=False):
	traveler_id_ref = g_db_connector.collection(Firestore.identifiers_collection.value)
	# go over given identifiers and find matching local_id
	local_id = None
	for identifier in identifiers:
		try:
			traveler_local_id = traveler_id_ref.document(identifier).get().to_dict()
			if traveler_local_id is not None:
				if local_id and local_id != traveler_local_id['local_id']:
					logging.error("Error: found two local_ids for the same identifier. IDs {0} and {1} both match the"
								  "identifier {2}".format(local_id, traveler_local_id['local_id'], identifier))
				local_id = traveler_local_id['local_id']

		except Exception as exc:
			logging.error(traceback.format_exc())
			logging.error(str(exc))

	# TODO: remove this fallback flow, it searches local_id by personal_id which isn't unique
	if local_id is None and fallback_to_personal_ids == True:
		# fallback to search in personal_ids collection
		local_id = _get_first_local_id_from_personal_ids(identifiers)
	return local_id

def _get_first_local_id_from_personal_ids(personal_ids):
	traveler_id_ref = g_db_connector.collection(Firestore.personal_ids_collection.value)
	# go over given personal_ids and find matching local_id
	local_id = None
	for personal_id in personal_ids:
		try:
			traveler_local_ids = traveler_id_ref.document(personal_id).get().to_dict()
			if traveler_local_ids:
				local_id = traveler_local_ids['local_ids'][0]
				break
		except:
			logging.error(traceback.format_exc())

	return local_id

# get local_ids (representing unique profiles) associated with a specific non-unique personal_id (e.g. name/phone/email)
def get_local_ids_list_by_personal_id(personal_id):
	local_ids = g_db_connector.collection(Firestore.personal_ids_collection.value).document(personal_id).get().get_dict()
	if local_ids is None:
		return None
	else:
		return local_ids['local_ids']

def get_traveler_profile(traveler_local_id):
	traveler_profile_ref = g_db_connector.collection(Firestore.profiles_collection.value).document(traveler_local_id)

	try:
		profile = traveler_profile_ref.get()
		return profile.to_dict()
	except NotFound:
		return None

def get_counters_data(company_id):
	counters = {}
	company_stats_ref = \
		g_db_connector.collection(Firestore.statistics_collection.value).document(company_id)
	try:
		stats = company_stats_ref.get()
		counters = stats.to_dict()
	except NotFound as exc:
		logging.error(str(exc))
	
	return counters
	
def get_route(airline_id, route_id):
	airline_route_id = '{}{}'.format(airline_id, route_id).lower()
	airline_route_ref = \
		g_db_connector.collection(Firestore.airline_routes_collection.value).document(airline_route_id)

	try:
		route = airline_route_ref.get()
		data = route.to_dict()
		if data != None and 2 == len(data.get('data', [])):
			return data.get('data', [])
	except NotFound:
		pass

	user_agent = random.choice(USER_AGENT_LIST)
	headers = {'User-Agent': user_agent}
	flightradar24_route = requests.get(r"https://www.flightradar24.com/data/flights/{}{}".format(airline_id, route_id),
										headers=headers)
	
	airports = re.findall(r'/data/airports/[a-z][a-z][a-z]" target="_self" class="fbold">\(([A-Z][A-Z][A-Z])\) </a>',
							str(flightradar24_route.content))
	airports = list(set(airports))

	# Fallback to another mapping service
	if len(airports) != 2:
		flightmapper_route = requests.get(r"https://info.flightmapper.net/flight/{}_{}".format(airline_id, route_id),
										headers=headers)
	
		airports = re.findall(r'/airport/[A-Z][A-Z][A-Z]">.*?\(([A-Z][A-Z][A-Z])\)</a>',
								str(flightmapper_route.content))
		airports = list(set(airports))

		if len(airports) != 2:
			return None

	BatchFirestoreWrite().update_route(airline_id, route_id, airports)
	return airports

def get_bigquery_cached_result(query_name):
	bigquery_cache_ref = \
		g_db_connector.collection(Firestore.dashboard_queries_cache.value).document(query_name)

	try:
		result = bigquery_cache_ref.get()
		data = result.to_dict()
		if data != None:
			timestamp = data.get('timestamp', None)
			if timestamp != None and (datetime.now().astimezone() - timestamp) < timedelta(days=30):
				return data.get('data', None), data.get('labels', None)
	except NotFound:
		pass

	return None, None

def get_segments(company_id):
	bigquery_segments_ref = \
		g_db_connector.collection(Firestore.segments_collection.value).document(company_id)

	try:
		result = bigquery_segments_ref.get()
		data = result.to_dict()
		if dict != type(data):
			return []
	
		for segment in data.get('segments', []):
			segment['tags'] = list(segment['tags'].values())
		
		return data.get('segments', [])
	except NotFound:
		pass

	return []

def get_segment_potential(company_id, query):
	bigquery_segments_ref = \
		g_db_connector.collection(Firestore.segments_collection.value).document(company_id)

	try:
		result = bigquery_segments_ref.get()
		data = result.to_dict()
		if dict is type(data):
			if query in list(data.get('potential', {}).keys()):
				query_data = data.get('potential', {}).get(query)
				timestamp = query_data.get('timestamp', None)
				if timestamp != None and (datetime.now().astimezone() - timestamp) < timedelta(days=30):
					return query_data.get('data') 
	except NotFound:
		pass

	raw_query_data = get_bigquery_result(query)
	for row in raw_query_data:
		potential = row['potential']
		BatchFirestoreWrite().update_segment_potential(company_id, query, potential)

		return potential

def get_segment_identifiers(company_id, query):
	phones = []
	emails = []

	raw_query_data = get_bigquery_result(query)

	for row in raw_query_data:
		identifiers = row['personal_ids']
		for identifier in eval(identifiers):
			if '@' in identifier:
				emails.append(identifier)
			elif '05' in identifier:
				phones.append(identifier)

	emails = list(set(emails))
	phones = list(set(phones))
	return {'emails': emails, 'phones': phones}

def get_company_info(company_id):
	company_info_ref = \
		g_db_connector.collection(Firestore.company_info_collection.value).document(company_id)

	try:
		info = company_info_ref.get()
		data = info.to_dict()
		if data != None:
			return data.get('data', {})
	except NotFound:
		pass

	return None

def get_access_key_info(access_key):
	access_key_info_ref = \
		g_db_connector.collection(Firestore.access_key_info_collection.value).document(access_key)

	try:
		info = access_key_info_ref.get()
		data = info.to_dict()
		if data != None:
			return data.get('data', {})
	except NotFound:
		pass

	return None

def get_on_prem_process_info(data_id):
	data_ref = \
		g_db_connector.collection(Firestore.on_prem_data_info_collection.value).document(data_id)

	try:
		info = data_ref.get()
		data = info.to_dict()
		if data != None:
			return data.get('timestamp')
	except NotFound:
		pass

	return None

########################################
########### Firestore - write ##########
########################################
class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class BatchFirestoreWrite(metaclass=Singleton):
	def __init__(self):
		self._db = firestore.Client()
		self._batch = self._db.batch()
		self._counter = 0
		self._threshold = WriteThreshold.high.value
	
	def __del__(self):
		if 0 < self._counter:
			self.force_write()

	def _append_write(self, ref, data, merge):
		self._batch.set(ref, data, merge=merge)
		self._counter += 1

		if self._threshold <= self._counter:
			self.force_write()

	def force_write(self):
		self._counter = 0
		self._threshold = WriteThreshold.high.value
		self._batch.commit()

	def update_on_prem_process_info(self, data_id):
		data_ref = \
			self._db.collection(Firestore.on_prem_data_info_collection.value).document(data_id)

		self._threshold = WriteThreshold.critical.value
		self._append_write(data_ref, {'timestamp': datetime.now().astimezone()}, True)

	def update_traveler_profile(self, traveler_local_id, profile_dict):
		traveler_profile_ref = \
			self._db.collection(Firestore.profiles_collection.value).document(traveler_local_id)

		self._append_write(traveler_profile_ref, profile_dict, True)

	def update_traveler_identifiers(self, identifiers, local_id):
		for identifier in identifiers:
			traveler_id_ref = self._db.collection(Firestore.identifiers_collection.value).document(identifier)

			self._threshold = WriteThreshold.low.value
			self._append_write(traveler_id_ref, {'local_id' : local_id}, True)

	def update_personal_ids(self, local_id, personal_ids, should_hash=False):
		if should_hash:
			personal_ids = [get_hash(personal_id) for personal_id in personal_ids]

		personal_ids_ref = self._db.collection(Firestore.personal_ids_collection.value)
		for personal_id in personal_ids:
			traveler_personal_ids_ref = personal_ids_ref.document(personal_id)
			personal_id_to_local_ids_dict = traveler_personal_ids_ref.get().to_dict() or {}
			local_ids = personal_id_to_local_ids_dict.get('local_ids', [])
			if local_id not in local_ids:
				local_ids.append(local_id)

				self._threshold = WriteThreshold.low.value
				self._append_write(traveler_personal_ids_ref, {'local_ids' : local_ids}, True)

	def update_company_stats(self, company_id, counter_name, delta):
		company_stats_ref = \
			self._db.collection(Firestore.statistics_collection.value).document(company_id)
		
		self._append_write(company_stats_ref, {counter_name: Increment(delta)}, True)

	def update_route(self, airline_id, route_id, airports):
		airline_route_id = '{}{}'.format(airline_id, route_id).lower()
		routes_ref = \
			self._db.collection(Firestore.airline_routes_collection.value).document(airline_route_id)

		self._append_write(routes_ref, {'data': airports}, True)
		
	def update_bigquecy_cached_result(self, query_name, result, labels, query):
		cache_ref = \
			self._db.collection(Firestore.dashboard_queries_cache.value).document(query_name)

		self._threshold = WriteThreshold.medium.value
		self._append_write(
			cache_ref,
			{
				'data': result,
				'labels': labels,
				'query': query,
				'timestamp': datetime.now().astimezone(),
			},
			False)

	def update_segments(self, company_id, segment):
		segments = get_segments(company_id)
		segments.append(segment)
		
		for temp_segment in segments:
			temp_dict = {}
			for key, value in enumerate(temp_segment['tags']):
				temp_dict[str(key)] = value

			temp_segment['tags'] = temp_dict

		segments_ref = \
			self._db.collection(Firestore.segments_collection.value).document(company_id)

		self._threshold = WriteThreshold.critical.value
		self._append_write(segments_ref, {'segments': segments}, True)

	def update_segment_potential(self, company_id, query, potential):
		segments_ref = \
			self._db.collection(Firestore.segments_collection.value).document(company_id)

		self._threshold = WriteThreshold.medium.value
		self._append_write(
			segments_ref,
			{'potential': {
					query: {
						'data':potential,
						'timestamp': datetime.now().astimezone(),
					},
				}
			},
			True)

	def update_profile_for_bq(self, timestamp, local_profile_id, tag_name, tag_group,
								tag_value, tag_counter, origin, row_id=None):
		if row_id is None:
			row_id = get_hash('{} {} {}'.format(local_profile_id, tag_name, origin))

		bq_tags_row_ref = \
			self._db.collection(Firestore.bq_tags_collection.value).document(row_id)

		self._append_write(
			bq_tags_row_ref,
			{
				'timestamp': timestamp,
				'local_profile_id': local_profile_id,
				'tag_name': tag_name,
				'tag_group': tag_group,
				'tag_value': tag_value,
				'tag_counter': tag_counter,
				'origin': origin,
			},
			False)

	def update_company_info(self, company_id, name, logo_url):
		company_info_ref = \
			self._db.collection(Firestore.company_info_collection.value).document(company_id)

		self._threshold = WriteThreshold.critical.value
		self._append_write(
			company_info_ref,
			{
				'data': {
					'name': name,
					'logo_url': logo_url,
					},
			},
			True)

	def update_access_key_info(self, access_key, company_id):
		access_key_info_ref = \
			self._db.collection(Firestore.access_key_info_collection.value).document(access_key)

		self._threshold = WriteThreshold.critical.value
		self._append_write(
			access_key_info_ref,
			{
				'data': {
					'company_id': company_id,
					'last_update_time': datetime.now().astimezone(),
					},
			},
			True)