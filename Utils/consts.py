import os
from enum import Enum
from collections import defaultdict

prod_env = os.environ.get('production')
if prod_env is None or prod_env != 'true':
	# TEST ENV
	class Storage(Enum):
		raw_data = 'poc-raw-data-storage'
		on_prem_inbox = 'poc-in-data-storage'

	class WriteThreshold(Enum):
		critical = 1
		low = 1
		medium = 1
		high = 1
		
else:
	# PROD ENV
	class Storage(Enum):
		raw_data = 'anecdotes-prod-raw-data-storage'
		on_prem_inbox = 'poc-in-data-storage'
		
	class WriteThreshold(Enum):
		critical = 1
		low = 5
		medium = 20
		high = 499

class Firestore(Enum):
	identifiers_collection = 'identifiers'
	personal_ids_collection = 'personal_ids'
	profiles_collection = 'profiles'
	clients_collection = 'clients'
	statistics_collection = 'statistics'
	airline_routes_collection = 'airline_routes'
	dashboard_queries_cache = 'dashboard_queries_cache'
	segments_collection = 'segments'
	company_info_collection = 'company_info'
	access_key_info_collection = 'access_keys'
	on_prem_data_info_collection = 'on_prem_data_info'
	bq_tags_collection = 'bigquery_tags'

ON_PREM_DATA_TOPIC = 'on-prem-inbox-data'

if prod_env is None or prod_env != 'true':
	BQ_DATASET = '`revv-poc-1574867026155.TagsIsrair.AllTagsDeep`'
	PROJECT_NAME = 'revv-poc-1574867026155'
else:
	BQ_DATASET = '`anecdotes-prod.TagsIsrair.AllTagsDeep`'
	PROJECT_NAME = 'anecdotes-prod'

# Generic widget consts
GENERIC_BQ_UNION_ALL = r' UNION ALL '
GENERIC_BQ_QUERY_COUNT = r'COUNT( {field} ) as {name},'

GENERIC_BQ_QUERY_PIE_1_CHART = '\
SELECT {{counters}} \
FROM {table} \
WHERE {{filter}} \
'.format(table=BQ_DATASET)

GENERIC_BQ_QUERY_PIE_2_CHART = '\
SELECT {{fields_counters}} \
FROM {table} \
WHERE {{filter}} \
GROUP BY {{grouping_field}} \
'.format(table=BQ_DATASET)

GENERIC_BQ_QUERY_SB100_1_CHART = '\
SELECT {{selector}}, {{counters}} \
FROM {table} \
GROUP BY {{group_name}} \
'.format(table=BQ_DATASET)

GENERIC_BQ_QUERY_SB100_2_CHART = '\
SELECT "{{name}}" as name, {{counters}} \
FROM (SELECT {{columns}} FROM {table} \
WHERE {{filter}} IS NOT NULL)\
'.format(table=BQ_DATASET)

GENERIC_BQ_QUERY_CORR_COUNT = '\
SELECT \
CORR({{param_1}}, {{param_2}}) as corr_data, \
COUNT({{param_1}} > 0 and {{param_2}} > 0) as counter, \
"{{param_1}}" as name_1, \
"{{param_2}}" as name_2, \
FROM {table} \
WHERE {{filter}} \
'.format(table=BQ_DATASET)

GENERIC_BQ_QUERY_CORR_TABLE = '\
SELECT \
name_1, name_2, corr_data, counter, \
FROM ({table})\
'

GENERIC_BQ_QUERY_SEGMENT = '\
SELECT {field} \
FROM ({table})\
'

GENERIC_BQ_QUERY_SEGMENT_CONDITIONS = '\
SELECT personal_ids \
FROM {table} \
WHERE {{conditions}}\
'.format(table=BQ_DATASET)

FB_GENERIC_KEYS = ['languages']

FIRST_PAGE = 1

class WeekDays(Enum):
	Monday		= 0
	Tuesday		= 1
	Wednesday	= 2
	Thursday	= 3
	Friday		= 4
	Saturday	= 5
	Sunday		= 6

AFTER_SHABBAT_TIME = 20

TAGS_VALIDATOR = {
	'_5_stars_hotel': '> 0',
	'age_range': 'IS NOT NULL',
	'budget': 'IS NOT NULL',
	'couple_traveller': '> 0',
	'destination_length': 'IS NOT NULL',
	'direct': '> 0',
	'eastern_europe': '> 0',
	'flight_hotel': '> 0',
	'frequency': 'IS NOT NULL',
	'indirect': '> 0',
	'journey_end_day': 'IS NOT NULL',
	'journey_end_month': 'IS NOT NULL',
	'journey_length': 'IS NOT NULL',
	'journey_start_day': 'IS NOT NULL',
	'journey_start_month': 'IS NOT NULL',
	'languages': 'IS NOT NULL',
	'last_minute_planner': '> 0',
	'moderate_journey': '> 0',
	'moderate_trip': '> 0',
	'multiple_luggage': '> 0',
	'no_shabbat_flights': '> 0',
	'recurrence_counter': '> 0',
	'identifiers': 'IS NOT NULL',
	'internal_profile_number': '> 0',
	'personal_ids': 'IS NOT NULL',
	'varna': '> 0',
	'coupons': '> 0',
	'gender': 'IS NOT NULL',
	'hotel': '> 0',
	'ota': '> 0',
	'short_journey': '> 0',
	'short_term_planner': '> 0',
	'short_trip': '> 0',
	'southern_europe': '> 0',
	'weekend': '> 0',
	'thessaloniki': '> 0',
	'_2_children': '> 0',
	'_4_stars_hotel': '> 0',
	'call_center': '> 0',
	'family_traveller': '> 0',
	'summer': '> 0',
	'_3_children': '> 0',
	'flight_only': '> 0',
	'athens': '> 0',
	'bucharest': '> 0',
	'beach': '> 0',
	'luggage': '> 0',
	'shabbat_flights': '> 0',
	'larnaca': '> 0',
	'long_term_planner': '> 0',
	'rome': '> 0',
	'_1_child': '> 0',
	'extra_long_term_planner': '> 0',
	'generic_direct': '> 0',
	'summer_school': '> 0',
	'western_europe': '> 0',
	'organized_tour': '> 0',
	'asia': '> 0',
	'batumi': '> 0',
	'group_trip': '> 0',
	'winter': '> 0',
	'madrid': '> 0',
	'paphos': '> 0',
	'direct_web': '> 0',
	'hot_lead': 'IS NOT NULL',
	'rhodes': '> 0',
	'belgrad': '> 0',
	'long_journey': '> 0',
	'long_trip': '> 0',
	'tbilisi': '> 0',
	'culture': '> 0',
	'premium': '> 0',
	'baku': '> 0',
	'_1_infant': '> 0',
	'sochi': '> 0',
	'bourgas': '> 0',
	'prague': '> 0',
	'tirana': '> 0',
	'_3_stars_hotel': '> 0',
	'berlin': '> 0',
	'_4_children': '> 0',
	'tenerife': '> 0',
	'sofia': '> 0',
	'northern_europe': '> 0',
	'gambling': '> 0',
	'south_east_asia': '> 0',
	'montenegro': 'IS NOT NULL',
	'resort': '> 0',
	'ljubljana': '> 0',
	'spa': '> 0',
	'south_america': 'IS NOT NULL',
	'seat': '> 0',
	'_2_infants': '> 0',
	'boutique_hotel': '> 0',
	'city_center_hotel': '> 0',
	'africa': '> 0',
	'lapland': '> 0',
	'verona': '> 0',
	'meal': 'IS NOT NULL',
	'london': '> 0',
	'tour_guide': 'IS NOT NULL',
	'art': 'IS NOT NULL',
	'kosher': 'IS NOT NULL',
	'spa_hotel': 'IS NOT NULL',
	'eastern_asia': 'IS NOT NULL',
	'middle_east': 'IS NOT NULL',
	'single_connection': 'IS NOT NULL',
	'single_traveller': 'IS NOT NULL',
	'north_america': 'IS NOT NULL',
	'boys_trip': 'IS NOT NULL',
	'_5_children': 'IS NOT NULL',
	'ski': 'IS NOT NULL',
	'multiple_infants': 'IS NOT NULL',
	'caribbean': 'IS NOT NULL',
	'airport_hotel': 'IS NOT NULL',
	'apartments_hotel': 'IS NOT NULL',
	'oceania': 'IS NOT NULL',
	'central_america': 'IS NOT NULL'
}

COUNTRY_TO_CONTINENT =\
COUNTRY_TO_CONTINENT = defaultdict(lambda: None)
COUNTRY_TO_CONTINENT.update(
{'aruba': 'Caribbean',
 'afghanistan': 'Asia',
 'angola': 'Africa',
 'anguilla': 'Caribbean',
 'albania': 'SouthernEurope',
 'andorra': 'SouthernEurope',
 'netherlands antilles': 'Caribbean',
 'united arab emirates': 'MiddleEast',
 'argentina': 'SouthAmerica',
 'armenia': 'MiddleEast',
 'american samoa': 'Oceania',
 'antarctica': 'Antarctica',
 'french southern territories': 'Antarctica',
 'antigua and barbuda': 'Caribbean',
 'australia': 'Oceania',
 'austria': 'WesternEurope',
 'azerbaijan': 'Asia',
 'burundi': 'Africa',
 'belgium': 'WesternEurope',
 'benin': 'Africa',
 'burkina faso': 'Africa',
 'bangladesh': 'Asia',
 'bulgaria': 'EasternEurope',
 'bahrain': 'MiddleEast',
 'bahamas': 'Caribbean',
 'the bahamas': 'Caribbean',
 'bosnia and herzegovina': 'SouthernEurope',
 'belarus': 'EasternEurope',
 'belize': 'CentralAmerica',
 'bermuda': 'NorthAmerica',
 'bolivia': 'SouthAmerica',
 'brazil': 'SouthAmerica',
 'barbados': 'Caribbean',
 'brunei': 'SouthEastAsia',
 'bhutan': 'Asia',
 'bouvet island': 'Antarctica',
 'botswana': 'Africa',
 'african republic': 'Africa',
 'canada': 'NorthAmerica',
 'cocos (keeling) islands': 'Oceania',
 'switzerland': 'WesternEurope',
 'chile': 'SouthAmerica',
 'china': 'EasternAsia',
 "cote d'ivoire": 'Africa',
 'cameroon': 'Africa',
 'congo, the democratic republic of the': 'Africa',
 'congo': 'Africa',
 'cook islands': 'Oceania',
 'colombia': 'SouthAmerica',
 'comoros': 'Africa',
 'cape verde': 'Africa',
 'costa rica': 'CentralAmerica',
 'cuba': 'Caribbean',
 'christmas island': 'Oceania',
 'cayman islands': 'Caribbean',
 'cyprus': 'SouthernEurope',
 'czech republic': 'EasternEurope',
 'germany': 'WesternEurope',
 'djibouti': 'Africa',
 'dominica': 'Caribbean',
 'denmark': 'NorthernEurope',
 'dominican republic': 'Caribbean',
 'algeria': 'Africa',
 'ecuador': 'SouthAmerica',
 'egypt': 'Africa',
 'eritrea': 'Africa',
 'western sahara': 'Africa',
 'spain': 'SouthernEurope',
 'estonia': 'baltic countries',
 'ethiopia': 'Africa',
 'finland': 'NorthernEurope',
 'fiji islands': 'Oceania',
 'falkland islands': 'SouthAmerica',
 'france': 'WesternEurope',
 'faroe islands': 'NorthernEurope',
 'micronesia, federated states of': 'Oceania',
 'gabon': 'Africa',
 'united kingdom': 'WesternEurope',
 'georgia': 'Asia',
 'ghana': 'Africa',
 'gibraltar': 'SouthernEurope',
 'guinea': 'Africa',
 'guadeloupe': 'Caribbean',
 'gambia': 'Africa',
 'guinea-bissau': 'Africa',
 'equatorial guinea': 'Africa',
 'greece': 'SouthernEurope',
 'grenada': 'Caribbean',
 'greenland': 'NorthAmerica',
 'guatemala': 'CentralAmerica',
 'french guiana': 'SouthAmerica',
 'guam': 'Oceania',
 'guyana': 'SouthAmerica',
 'hong kong': 'EasternAsia',
 'heard island and mcdonald islands': 'Antarctica',
 'honduras': 'CentralAmerica',
 'croatia': 'SouthernEurope',
 'haiti': 'Caribbean',
 'hungary': 'EasternEurope',
 'indonesia': 'SouthEastAsia',
 'india': 'Asia',
 'british indian ocean territory': 'Africa',
 'ireland': 'WesternEurope',
 'iran': 'Asia',
 'iraq': 'MiddleEast',
 'iceland': 'NorthernEurope',
 #'israel': 'Israel',
 'italy': 'SouthernEurope',
 'jamaica': 'Caribbean',
 'jordan': 'MiddleEast',
 'japan': 'EasternAsia',
 'kazakstan': 'Asia',
 'kenya': 'Africa',
 'kyrgyzstan': 'Asia',
 'cambodia': 'SouthEastAsia',
 'kiribati': 'Oceania',
 'saint kitts and nevis': 'Caribbean',
 'south korea': 'EasternAsia',
 'kuwait': 'MiddleEast',
 'laos': 'SouthEastAsia',
 'lebanon': 'MiddleEast',
 'liberia': 'Africa',
 'libyan arab jamahiriya': 'Africa',
 'saint lucia': 'Caribbean',
 'liechtenstein': 'WesternEurope',
 'sri lanka': 'Asia',
 'lesotho': 'Africa',
 'lithuania': 'baltic countries',
 'luxembourg': 'WesternEurope',
 'latvia': 'baltic countries',
 'macao': 'EasternAsia',
 'morocco': 'Africa',
 'monaco': 'WesternEurope',
 'moldova': 'EasternEurope',
 'madagascar': 'Africa',
 'maldives': 'Asia',
 'mexico': 'CentralAmerica',
 'marshall islands': 'Oceania',
 'macedonia': 'SouthernEurope',
 'mali': 'Africa',
 'malta': 'SouthernEurope',
 'myanmar': 'SouthEastAsia',
 'mongolia': 'EasternAsia',
 'northern mariana islands': 'Oceania',
 'mozambique': 'Africa',
 'mauritania': 'Africa',
 'montserrat': 'Caribbean',
 'martinique': 'Caribbean',
 'mauritius': 'Africa',
 'malawi': 'Africa',
 'malaysia': 'SouthEastAsia',
 'mayotte': 'Africa',
 'namibia': 'Africa',
 'new caledonia': 'Oceania',
 'niger': 'Africa',
 'norfolk island': 'Oceania',
 'nigeria': 'Africa',
 'nicaragua': 'CentralAmerica',
 'niue': 'Oceania',
 'netherlands': 'WesternEurope',
 'norway': 'NorthernEurope',
 'nepal': 'Asia',
 'nauru': 'Oceania',
 'new zealand': 'Oceania',
 'oman': 'MiddleEast',
 'pakistan': 'Asia',
 'panama': 'CentralAmerica',
 'pitcairn': 'Oceania',
 'peru': 'SouthAmerica',
 'philippines': 'SouthEastAsia',
 'palau': 'Oceania',
 'papua new guinea': 'Oceania',
 'poland': 'EasternEurope',
 'puerto rico': 'Caribbean',
 'north korea': 'EasternAsia',
 'portugal': 'SouthernEurope',
 'paraguay': 'SouthAmerica',
 'french polynesia': 'Oceania',
 'qatar': 'MiddleEast',
 'reunion': 'Africa',
 'romania': 'EasternEurope',
 'russian federation': 'EasternEurope',
 'rwanda': 'Africa',
 'saudi arabia': 'MiddleEast',
 'sudan': 'Africa',
 'senegal': 'Africa',
 'singapore': 'SouthEastAsia',
 'south georgia and the south sandwich islands': 'Antarctica',
 'saint helena': 'Africa',
 'svalbard and jan mayen': 'NorthernEurope',
 'solomon islands': 'Oceania',
 'sierra leone': 'Africa',
 'el salvador': 'CentralAmerica',
 'san marino': 'SouthernEurope',
 'somalia': 'Africa',
 'saint pierre and miquelon': 'NorthAmerica',
 'sao tome and principe': 'Africa',
 'suriname': 'SouthAmerica',
 'slovakia': 'EasternEurope',
 'slovenia': 'SouthernEurope',
 'sweden': 'NorthernEurope',
 'swaziland': 'Africa',
 'seychelles': 'Africa',
 'syria': 'MiddleEast',
 'turks and caicos islands': 'Caribbean',
 'chad': 'Africa',
 'togo': 'Africa',
 'thailand': 'SouthEastAsia',
 'tajikistan': 'Asia',
 'tokelau': 'Oceania',
 'turkmenistan': 'Asia',
 'east timor': 'SouthEastAsia',
 'tonga': 'Oceania',
 'trinidad and tobago': 'Caribbean',
 'tunisia': 'Africa',
 'turkey': 'MiddleEast',
 'tuvalu': 'Oceania',
 'taiwan': 'EasternAsia',
 'tanzania': 'Africa',
 'uganda': 'Africa',
 'ukraine': 'EasternEurope',
 'united states minor outlying islands': 'Caribbean',
 'uruguay': 'SouthAmerica',
 'united states': 'NorthAmerica',
 'uzbekistan': 'Asia',
 'holy see (vatican city state)': 'SouthernEurope',
 'saint vincent and the grenadines': 'Caribbean',
 'venezuela': 'SouthAmerica',
 'virgin islands, british': 'Caribbean',
 'virgin islands, u.s.': 'Caribbean',
 'vietnam': 'SouthEastAsia',
 'vanuatu': 'Oceania',
 'wallis and futuna': 'Oceania',
 'samoa': 'Oceania',
 'yemen': 'MiddleEast',
 'yugoslavia': 'SouthernEurope',
 'south africa': 'Africa',
 'zambia': 'Africa',
 'zimbabwe': 'Africa'
 })

USER_AGENT_LIST = [
   #Chrome
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
	'Mozilla/5.0 (Windows NT 5.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
	'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
	#Firefox
	'Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.1)',
	'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
	'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)',
	'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',
	'Mozilla/5.0 (Windows NT 6.2; WOW64; Trident/7.0; rv:11.0) like Gecko',
	'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
	'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0)',
	'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',
	'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)',
	'Mozilla/5.0 (Windows NT 6.1; Win64; x64; Trident/7.0; rv:11.0) like Gecko',
	'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)',
	'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)',
	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)'
] 