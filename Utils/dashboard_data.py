from enum import Enum

from Utils import cloud as cloud_utils


class CounterName(Enum):
	profiles = 'profiles'
	facebook_logins = 'facebook_logins'
	google_logins = 'google_logins'
	external_enrichments = 'external_enrichments'
	tags_per_profile = 'tags_per_profile'

class DashboardData(object):
	def __init__(self, company_id, profiles=0, facebook_logins=0, 
				google_logins=0, external_enrichments=0, widgets=None):
		self._company_id = company_id
		self._counter_profiles = profiles
		self._counter_facebook_logins = facebook_logins
		self._counter_google_logins = google_logins
		self._external_enrichments = external_enrichments
		self._widgets = widgets

	@staticmethod
	def increment_counter(company_id, counter_name, delta):
		cloud_utils.BatchFirestoreWrite().update_company_stats(company_id, counter_name, delta)

	def get_company_id(self):
		return self._company_id

	def to_dict(self):
		return {
			'company_id': self._company_id,
			'counters': {
				CounterName.profiles.value: self._counter_profiles,
				CounterName.facebook_logins.value: self._counter_facebook_logins,
				CounterName.google_logins.value: self._counter_google_logins,
				CounterName.external_enrichments.value: self._external_enrichments,
				# CounterName.tags_per_profile.value: self._tags_per_profile,
			},
			'widgets': self._widgets
		}

	def __str__(self):
		return str(self.to_dict())

	def __repr__(self):
		return str(self)

