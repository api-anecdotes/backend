import Utils.consts as consts


def craft_segment_query(tags, potential_only=True):
	query = []
	for group in tags:
		if 0 == len(group):
			continue

		group_queries = []
		for tag in group:
			tag = '_'.join([sub_tag.capitalize() for sub_tag in tag.split(' ')])
			tag = tag.replace('>', '_').replace('<', '_').replace('+', '_').replace('-', '_')

			if tag[0].isdigit():
				tag = '_{}'.format(tag)

			if 'Budget' in tag:
				tag_condition = 'Budget like "{}"'.format(tag)
			elif 'Gender' in tag:
				tag_condition = 'Gender like "{}"'.format(tag)
			elif 'Frequency' in tag:
				tag_condition = 'Frequency like "{}"'.format(tag)
			elif 'Age_Range_' in tag:
				tag_condition = 'Age_Range like "{}"'.format(tag[len('Age_Range_'):])
			elif tag.lower() in list(consts.TAGS_VALIDATOR.keys()):
				tag_condition = '{} {}'.format(tag, consts.TAGS_VALIDATOR.get(tag.lower()))

			group_queries.append(tag_condition)

		query.append(r'({})'.format(' OR '.join(group_queries)))

	query_str = ' AND '.join(query)

	field = consts.GENERIC_BQ_QUERY_COUNT.format(field='*', name='potential')
	if not potential_only:
		field = 'personal_ids'

	return consts.GENERIC_BQ_QUERY_SEGMENT.format(
		field=field,
		table=consts.GENERIC_BQ_QUERY_SEGMENT_CONDITIONS.format(conditions=query_str))
