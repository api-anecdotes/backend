from Utils.dashboard_widgets import PieChartWidget1, PieChartWidget2, StackBar100Widget1, StackBar100Widget2, HeatMapWidget1


PIE_CHARTS = [
	# Family reservation times
	PieChartWidget1({
		'Last_Minute_Planner': 'Last minute',
		'Short_Term_Planner': 'Short term',
		'Long_Term_Planner': 'Long term',
		'Extra_Long_Term_Planner': 'Extra long term',
	},
	'Reservation Times for Family Trips',
	'Family_Traveller IS NOT NULL'),

	# Couples reservation times
	PieChartWidget1({
		'Last_Minute_Planner': 'Last minute',
		'Short_Term_Planner': 'Short term',
		'Long_Term_Planner': 'Long term',
		'Extra_Long_Term_Planner': 'Extra long term',
	},
	'Reservation Times for Couples Trips',
	'Couple_Traveller IS NOT NULL'),

	# Singles reservation times
	PieChartWidget1({
		'Single_Traveller': 'Single Traveller',
		'Couple_Traveller': 'Couple Traveller',
		'Family_Traveller': 'Family Traveller',
		'Group_Trip': 'Group Trip',
	},
	'Travellers Type',
	'1 = 1'),

	# Recurring customers' by budget
	PieChartWidget2(
		'Budget',
		'Recurring Customers Budget',
		'Recurrence_Counter > 1'),

	# Age Range Distribution
	PieChartWidget2(
		'Age_Range',
		'Age Range Distribution',
		'1 = 1'),

	# Gambling Age Range
	PieChartWidget2(
		'Age_Range',
		'Gambling Age Range',
		'batumi > 0 OR bucharest > 0 OR prague > 0 OR sochi > 0 OR varna > 0 OR sofia > 0 OR bourgas > 0'),

]

STACKBAR100_CHARTS = [
	# Budget reservation channel
	StackBar100Widget1({
		'Call_Center': 'Call Center',
		'OTA': 'OTA',
		'Generic_Direct': 'Generic Direct',
		'Direct_Web': 'Direct Web',
		'Indirect': 'Indirect',
		'Coupons': 'Coupons',
	},
	'Budget by Reservation Channels',
	'Budget'),

	# Budget Geo-Zone Age-Range
	StackBar100Widget1({
		'Western_Europe': 'Western Europe',
		'Eastern_Europe': 'Eastern Europe',
		'Southern_Europe': 'Southern Europe',
		'Northern_Europe': 'Northern Europe',
		'Middle_East': 'Middle East',
		'Africa': 'Africa',
		'Asia': 'Asia',
		'Eastern_Asia': 'Eastern Asia',
		'South_East_Asia': 'South East Asia',
	},
	'Geographic Zone by Age',
	'Age_Range'),

	# Frequency per package type
	StackBar100Widget1({
		'Flight_Only': 'Flight Only',
		'Flight_Hotel': 'Flight Hotel',
		'Group_Trip': 'Group Trip',
		'Organized_Tour': 'Organized Tour',
	},
	'Frequency per Package Type',
	'Frequency'),

	# Ancillaries - Packages
	StackBar100Widget2({
		'Flight_Only': 'Flight Only',
		'Flight_Hotel': 'Flight Hotel',
		'Group_Trip': 'Group Trip',
		'Organized_Tour': 'Organized Tour',
	},
	'Ancillary per Package Type',
	['Meal', 'Seat', 'Luggage', 'Multiple_Luggage']),
]

HEATMAP_CHARTS = [

	# Destinations - Destinations
	HeatMapWidget1({
		'x': ['varna', 'baku', 'sochi', 'belgrad', 'batumi', 'tbilisi', 'rhodes', 'tirana', 'larnaca', 'bucharest', 'prague'],
		'y': ['varna', 'baku', 'sochi', 'belgrad', 'batumi', 'tbilisi', 'rhodes', 'tirana', 'larnaca', 'bucharest', 'prague'],
		'filter': 'Recurrence_Counter > 1'
	},
	'Destinations correlation among recurring customers'),

	# Destinations - Reservation Channels
	HeatMapWidget1({
		'x': ['varna', 'baku', 'sochi', 'belgrad', 'batumi', 'tbilisi', 'bourgas', 'rhodes', 'tirana', 'athens', 'larnaca', 'bucharest', 'prague', 'tenerife'],
		'y': ['Call_Center', 'Direct_Web', 'OTA', 'Coupons', 'Indirect'],
		'filter': '1 = 1'
	},
	'Destinations by Reservation Channels'),
]
ALL_WIDGETS = PIE_CHARTS + STACKBAR100_CHARTS + HEATMAP_CHARTS
