from random import getrandbits
import dateutil.parser as time_parser

import Utils.cloud as cloud_utils
from Utils.dashboard_data import DashboardData
from Tagging.data_origin import OriginCompany
from Utils.tagging_helpers import get_all_tags
from abc import ABC, abstractmethod


class BaseSource(ABC):
	def __init__(self, origin_company, origin_type, token=None, should_override_data=False):
		self._data = {}
		self._travel_primitives = []
		self._user_local_id = None
		self._generic = {}
		self._token = token
		self._origin_company = origin_company or OriginCompany.UNKNOWN_COMPANY.name
		self._last_updated = None
		self._tags = get_all_tags()
		self._origin = {self._origin_company: [origin_type]}
		self._is_valid_profile = True

		# keep the token (hashed) so that we know we processed it. If there is no token, generate a random one
		self._processing_id = cloud_utils.get_hash(token if token else str(getrandbits(256)))
		self._read_remote_source = should_override_data or not cloud_utils.file_exists(blob_name=self._processing_id)

	def _upload_raw_data_to_storage(self):
		cloud_utils.upload_raw_data(blob_name=self._processing_id, data=self._data, origin=self._origin_company)

	def _update_statistic_counter(self, origin_type):
		DashboardData.increment_counter(self._origin_company, origin_type, 1)

	def get_data_origin(self):
		return self._origin

	def get_raw_data(self):
		return self._data

	def get_user_local_id(self):
		return self._user_local_id

	def get_travel_primitives(self):
		return self._travel_primitives

	def get_generic_info(self):
		return self._generic

	def get_tags(self):
		return self._tags

	def get_last_updated(self):
		return self._last_updated

	def set_last_updated(self, last_updated):
		self._last_updated = last_updated

	def is_valid(self):
		return self._is_valid_profile

	# To be implemented by every source implementation
	@abstractmethod
	def read_raw_data(self):
		pass

	@abstractmethod
	def stage_raw_data_for_analysis(self):
		pass

