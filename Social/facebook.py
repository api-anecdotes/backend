import logging
import dateutil.parser as time_parser
from dateutil.relativedelta import relativedelta
import datetime
import pytz

import facebook
from facebook import GraphAPIError
import requests
from collections import defaultdict

from Utils.source import BaseSource
from Utils.cloud import BatchFirestoreWrite
from Utils.dashboard_data import CounterName
from Utils.tagging_helpers import build_generics_with_origin, add_tags_from_item_list_of_texts, add_tags_by_categories
from Utils.consts import FB_GENERIC_KEYS, FIRST_PAGE
from Traveler.identifiers import get_or_create_local_id
from Tagging.travel_primitives import Location
from Tagging.data_origin import OriginType
from Tagging.tags import InterestTags
from Tagging.consts import text_per_interest_tag, FB_FIELDS_TO_GENERIC_KEYS


logging.basicConfig(level=10)

MIN_FANS_PER_PAGE = 100
MAX_PAGES_TO_TRAVERSE = 20
YEARS_BACK_TO_TRAVERSE = 3

class Facebook(BaseSource):
	def __init__(self, token=None, app_token=None, should_override_data=True, origin_company=None):
		super().__init__(origin_company, OriginType.FACEBOOK.name, token, should_override_data)
		self._likes = defaultdict(int)
		self._app_token = app_token

	def _read_personal_ids(self):
		if self._token is None or self._token == '' or not self._read_remote_source:
			logging.debug('Skip read_personal_ids, token = {}'.format(self._token))
			return

		self._update_statistic_counter(CounterName.facebook_logins.value)

		try:
			graph = facebook.GraphAPI(access_token=self._token, version="2.12")
			personal_ids = graph.get_object(id='me', fields='id, name, email', locale='en_US')
			self._data['personal_ids'] = personal_ids
		except Exception as exc:
			logging.error('FB Error ' + str(exc))

	def extract_personal_ids(self):
		self._read_personal_ids()
		personal_ids = self._data.get('personal_ids', {})

		fb_id = personal_ids.get('id', '').lower()
		name = personal_ids.get('name', '').lower()
		email = personal_ids.get('email', '').lower()
		all_personal_ids = [fb_id, name, email]
		valid_personal_ids = [id for id in all_personal_ids if id != '']

		valid_identifiers = []
		if name and email:
			valid_identifiers.append(name+email)
		if name and fb_id:
			valid_identifiers.append(name+fb_id)

		# we have 2 types of 'identifiers':
		# 	(1) "personal_ids": non-unique (e.g. email, name)
		# 	(2) "identifiers": unique (e.g. name+email)
		self._data['personal_ids'] = valid_personal_ids
		logging.debug("facebook personal_ids: {}".format(self._data['personal_ids']))

		self._data['identifiers'] = valid_identifiers
		logging.debug("facebook identifiers: {}".format(self._data['identifiers']))

		if not valid_personal_ids:
			self._is_valid_profile = False

		# retrieve local id, and update the identifiers and personal_ids in cloud
		self._user_local_id = get_or_create_local_id(
			company_id=self._origin_company,
			identifiers=valid_identifiers,
			should_hash=True)
		logging.debug("local_id: {}".format(self._user_local_id))

		BatchFirestoreWrite().update_personal_ids(
			local_id=self._user_local_id,
			personal_ids=valid_personal_ids,
			should_hash=True)

	def read_raw_data(self):
		if self._token is None or self._token == '' or not self._read_remote_source:
			logging.debug('Skip read raw data, token = {}'.format(self._token))
			return

		try:
			graph = facebook.GraphAPI(access_token=self._token, version="2.12")

			# used for date limit on facebook query - currently we fetch without date limit
			# date_limit = datetime.datetime.now() - relativedelta(years=YEARS_BACK_TO_TRAVERSE)
			# date_limit = pytz.utc.localize(date_limit)
			# last_updated = self.get_last_updated()
			# # don't read items we already read in the past
			# if last_updated and last_updated > date_limit:
			# 	date_limit = last_updated

			generics_query = 'languages{name}'
			# feed_query = 'feed.since({start_time}){{id,created_time,place,message,message_tags,description}}'\
			# 				.format(start_time=date_limit.strftime("%Y-%m-%d"))
			feed_query = 'feed{id,created_time,place,message,message_tags,description}'
			likes_query = 'likes{fan_count,name,about,category_list,created_time}'

			me_info = graph.get_object(
				id='me',
				locale='en_US',
				fields='{generic},{feed},{likes}'.format(generic=generics_query, feed=feed_query, likes=likes_query))

			self._data['me_info'] = me_info

			if 'feed' in me_info:
				self._data['feed'] = self._get_data_pages(me_info['feed'], MAX_PAGES_TO_TRAVERSE)
			if 'likes' in me_info:
				self._data['likes'] = self._get_data_pages(me_info['likes'], MAX_PAGES_TO_TRAVERSE)

		except Exception as exc:
			logging.error('FB Error ' + str(exc))

		self._upload_raw_data_to_storage()

	def stage_raw_data_for_analysis(self):
		basic_info = self._data.get('me_info', {})

		# Extract generic info
		fb_generics = {key: value for (key, value) in basic_info.items() if key in FB_GENERIC_KEYS and value is not None}
		# fix languages structure
		if 'languages' in fb_generics:
			all_languages = [language['name'] for language in fb_generics['languages']]
			final_languages = []
			for language in all_languages:
				if ' language' in language:
					language = language.replace(' language', '')
				final_languages.append(language)
			final_languages = list(set(final_languages))
			fb_generics['languages'] = final_languages

		generics = {FB_FIELDS_TO_GENERIC_KEYS[key] : value for (key, value) in fb_generics.items()}
		self._generic = build_generics_with_origin(generics, self._origin)

		# Extract likes
		likes = self._data.get('likes', [])
		for like in likes:
			# check some threshold for page validity
			fan_count = like.get('fan_count', 0)
			if fan_count > MIN_FANS_PER_PAGE:
				self._add_tag_by_like_texts(like)

			# filter likes by their creation time to prevent double-processing of same like
			# fb_last_update = self.get_last_updated()
			# like_created_time_str = (like.get('created_time', None))
			# if like_created_time_str:
			# 	like_created_time = time_parser.parse(like_created_time_str)
			# 	# Only process likes that were'nt processed before
			# 	if (like_created_time and not fb_last_update) or (like_created_time > fb_last_update):
			# 		# check some threshold for page validity
			# 		fan_count = like.get('fan_count', 0)
			# 		if fan_count > MIN_FANS_PER_PAGE:
			# 			self._add_tag_by_category(like)

		# Extract location events from feed
		feed = self._data.get('feed', [])
		for item in feed:
			place = item.get('place', None)
			if place is not None:
				feed_item_created_time = time_parser.parse(item.get('created_time'))
				categories = self._get_place_categories(place, should_fallback=True)
				location = place.get('location', None)
				if location is not None:
					self._travel_primitives.append(Location(
						start=feed_item_created_time,
						end=feed_item_created_time,
						city=location.get('city', ''),
						country=location.get('country', ''),
						latitude=location.get('latitude', ''),
						longitude=location.get('longitude', ''),
						categories=categories,
						name=place.get('name', ''),
						data_origin=self._origin))

	def _get_data_pages(self,
						fb_data_object,
						max_pages_to_read=MAX_PAGES_TO_TRAVERSE,
						should_limit_by_time=False,
						time_limit=None,
						time_field_name=None):
		data = []
		page_counter = FIRST_PAGE
		# Keep paginating requests until finished or reached max_pages
		while (page_counter <= max_pages_to_read):
			try:
				if should_limit_by_time:
					# check first item for timestamp, break if we reached an item which is too old
					first_item_created_time = time_parser.parse(fb_data_object['data'][0][time_field_name])
					if first_item_created_time < time_limit:
						# if the first item is not new - none of the data after it it is new
						if page_counter == FIRST_PAGE:
							return []
						else:
							break

				# Sometimes facebook returns "next" page eventhough there's no more data
				if len(fb_data_object['data']) == 0:
					break
				else:
					data.extend(fb_data_object['data'])
					page_counter += 1

				# request the next page of data.
				fb_data_object = requests.get(fb_data_object['paging']['next']).json()

			except KeyError as e:
				# When there are no more pages - break
				break
		return data

	def _get_place_categories(self, place_object, should_fallback=True):
		try:
			location = place_object.get('location', None)
			name = place_object.get('name', None)
			if location is None or name is None:
				return []

			center = str(location['latitude']) + "," + str(location['longitude'])
			graph = facebook.GraphAPI(self._token, version="2.12")
			search_results = graph.search(type='place',
								  center=center,
								  fields='category_list',
								  q=name,
								  locale='en_US',
								  distance=1).get('data', [])
			if not search_results:
				if should_fallback:
					# make another search, with a less-strict distance from coordinates
					search_results = graph.search(type='place',
												  center=center,
												  fields='category_list',
												  q=name,
												  locale='en_US',
												  distance=10).get('data', [])
					if not search_results:
						return []
				else:
					return []

			# The first match is the most accurate
			matching_place = search_results[0]

			# retrieve category list
			category_list = matching_place.get('category_list', [])

			# # TODO: this should be app_token when we'll be granted the "Page Public Content Access" permission
			# graph = facebook.GraphAPI(access_token='2706529986034687|ja93XzTDWFLFyHkzk_V5C2wpvl8', version="2.12")
			# place_info = graph.get_object(
			#	 place_object.get('id', None),
			#	 fields='category_list')
			#category_list = place_info.get('category_list', [])

			return list(set([category.get('name').lower() for category in category_list]))

		except GraphAPIError as e:
			print(repr(e))
			return []

	def _add_tag_by_like_texts(self, like_object):
		# for every tag, go over all text fields, and check whether they appear in one of the like's text fields
		like_texts = []
		like_categories = []
		text_fields = ['name', 'about']
		for text_field in text_fields:
			text = like_object.get(text_field, None)
			if text is not None:
				like_texts.append(text.lower())
		if like_object.get('category_list', None) is not None:
			for category in like_object['category_list']:
				like_categories.append(category['name'].lower())

		# tag by like categories
		description = "Like '{}' categories: {}".format(like_object.get('name', "error getting name"), like_categories)
		add_tags_by_categories(like_categories, self._tags, self._origin, description=description)

		# tag interests by like name and 'about'
		description = "liked page: '{name}'".format(name=like_object.get('name', 'error getting page name'))
		add_tags_from_item_list_of_texts(
			like_texts,
			text_per_interest_tag,
			InterestTags,
			self._tags,
			self.get_data_origin(),
			description=description)