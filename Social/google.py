import re
import pytz
import logging
import dateutil.parser

from datetime import datetime, timedelta
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build

import Utils.cloud as cloud_utils
from Utils.dashboard_data import CounterName
from Utils.source import BaseSource
from Utils.tagging_helpers import add_tags_from_item_list_of_texts
from Social import google_consts
from Traveler.identifiers import get_or_create_local_id
from Tagging.travel_primitives import Flight, Accommodation, Ticket
from Tagging.data_origin import OriginType
from Tagging.tags import InterestTags
from Tagging.consts import text_per_interest_tag
from Datasets.airport_data import AIRPORT_DATA


class Google(BaseSource):
	def __init__(self, token=None, should_override_data=True, origin_company=None):
		super().__init__(origin_company, OriginType.GOOGLE.name, token, should_override_data)
		self._irrelevant_read_keys = ['etag', 'hangoutLink', 'conferenceData', 'htmlLink', 'iCalUID']

	# Collection: read raw data from source
	def read_raw_data(self):
		if self._token is None or self._token == '' or not self._read_remote_source:
			logging.info({'message': 'Skip read raw data, token = {}'.format(self._token)})
			return

		self._update_statistic_counter(CounterName.google_logins.value)

		all_events = []
		try:
			creds = Credentials(token=self._token)

			userinfo_service = build('oauth2', 'v2', credentials=creds)
			self._data['userinfo'] = userinfo_service.userinfo().get().execute()
			
			calendar_service = build('calendar', 'v3', credentials=creds)
			
			past_time = (datetime.utcnow()-timedelta(days=365*3)).isoformat() + 'Z'
			page_token = None
			while True:
				# TODO: read only valuable fields
				if page_token is None:
					events_result = calendar_service.events().list(calendarId='primary', 
						singleEvents=True, maxResults=2500, orderBy='startTime',
						timeMin=past_time).execute()
				else:
					events_result = calendar_service.events().list(pageToken=page_token).execute()
					
				# save only valuable fields
				for item in events_result['items']:
					for key in self._irrelevant_read_keys:
						try:
							item.pop(key)
						except KeyError:
							pass
					all_events.append(item)
					
				page_token = events_result.get('nextPageToken')
				if not page_token:  
					break
			
		except Exception as exc:
			logging.error({'message': 'GG Error',
							'exc': str(exc)})

		logging.info('Read {} events from Google calendar'.format(str(len(all_events))))
		self._data['events'] = all_events
		self._upload_raw_data_to_storage()

	# Analysis: staging #1
	def stage_raw_data_for_analysis(self):
		# Extract identifiers
		userinfo = self._data.get('userinfo', {})

		gg_id = userinfo.get('id', '').lower()
		name = userinfo.get('name', '').lower()
		if userinfo.get('verified_email', False):
			email = userinfo.get('email', '').lower()
		else:
			email = ''
		all_personal_ids = [gg_id, name, email]
		valid_personal_ids = [id for id in all_personal_ids if id != '']

		valid_identifiers = []
		if name and email:
			valid_identifiers.append(name+email)
		if name and gg_id:
			valid_identifiers.append(name+gg_id)

		# we have 2 types of 'identifiers':
		# 	(1) "personal_ids": non-unique (e.g. email, name)
		# 	(2) "identifiers": unique (e.g. name+email)
		self._data['personal_ids'] = valid_personal_ids
		logging.debug("google personal_ids: {}".format(self._data['personal_ids']))

		self._data['identifiers'] = valid_identifiers
		logging.debug("google identifiers: {}".format(self._data['identifiers']))

		if not valid_personal_ids:
			self._is_valid_profile = False

		# retrieve local id, and update the identifiers and personal_ids in cloud
		self._user_local_id = get_or_create_local_id(
			company_id=self._origin_company,
			identifiers=valid_identifiers,
			should_hash=True)
		logging.debug("local_id: {}".format(self._user_local_id))

		cloud_utils.BatchFirestoreWrite().update_personal_ids(
			local_id=self._user_local_id,
			personal_ids=valid_personal_ids,
			should_hash=True)

		locale_consts = google_consts.LocaleRegex(userinfo.get('locale'))

		# Extract flights
		self._flight = []
		for event in self._data.get('events', {}):
			# TODO: Filter by: summary, not 'rejected' events, creator self=True,
			
			organizer = event.get('organizer', {})
			if organizer.get('email', '') == 'unknownorganizer@calendar.google.com':
				event['created_by_google'] = True
			else:
				event['organizer'] = organizer

			creation_time_str = event.get('created')
			event['creation_time_date'] = dateutil.parser.parse(creation_time_str)

			start_str = event.get('start', {}).get('dateTime')
			if start_str:
				event['start_date'] = dateutil.parser.parse(start_str).astimezone(pytz.utc)
			else:
				start_str = event.get('start', {}).get('date')
				if start_str:
					event['start_date'] = pytz.utc.localize(dateutil.parser.parse(start_str)).astimezone(pytz.utc)

			end_str = event.get('end', {}).get('dateTime')
			if end_str:
				event['end_date'] = dateutil.parser.parse(end_str).astimezone(pytz.utc)
			else:
				end_str = event.get('end', {}).get('date')
				if start_str:
					event['end_date'] = pytz.utc.localize(dateutil.parser.parse(end_str)).astimezone(pytz.utc)
			
			# Extract travel primitives
			travel_primitive = self._extract_travel_object(event, locale_consts)
			if travel_primitive:
				self._travel_primitives.append(travel_primitive)

			# Extract interests
			event_texts = [
				event.get('summary', '').lower(),
				event.get('description', '').lower(),
				event.get('location', '').lower()
			]

			description = "event summary: '{summary}', event location: {location}".format(
				summary=event.get('summary', 'error getting event summary'),
				location=event.get('location', 'error getting event location'))
			add_tags_from_item_list_of_texts(
				event_texts,
				text_per_interest_tag,
				InterestTags,
				self._tags,
				self.get_data_origin(),
				description=description)

		logging.info('Detected total {} travel primitives'.format(len(self._travel_primitives)))

	def _match_any_regex(self, regex_list, text):
		for regex in regex_list:
			result = regex.search(text)
			if result != None:
				return result.groups()
		return None

	def _extract_travel_object(self, event, locale_consts):
		# Flight
		try:
			flight_dst_city, flight_num = None, None

			flight_summary_match =  locale_consts.get_flight_regex().search(event.get('summary', ''))
			if flight_summary_match != None:
				flight_dst_city, flight_num = flight_summary_match.groups()

			else:
				flight_summary_match = self._match_any_regex(locale_consts.get_all_flight_regex(), event.get('summary', ''))
				if flight_summary_match != None:
					flight_dst_city, flight_num = flight_summary_match

			if flight_num != None:
				flight_dst_airport = None
				location_match =  re.search(r'([A-Z][A-Z][A-Z])$', event.get('location', ''))
				if location_match is None:
					flight_src_city, flight_src_airport = None, None
				else:
					flight_src_airport = location_match.groups()[0]
				
				if flight_src_airport != None and AIRPORT_DATA.get(flight_src_airport) != None:
					flight_src_city = AIRPORT_DATA[flight_src_airport].get('municipality', '')

				airline_id, route_id = flight_num.split(' ')
				airports = cloud_utils.get_route(airline_id, route_id)
				if airports != None:		
					if airports[0] != flight_src_airport and airports[1] != flight_src_airport:
						logging.error("Read invalid data from FR24; {} & {}  != {}".format(airports[0], airports[1], flight_src_airport))
					elif airports[0] == flight_src_airport:
						flight_dst_airport = airports[1]
						flight_dst_city = AIRPORT_DATA.get(flight_dst_airport, {}).get('municipality', '')
					else:
						flight_dst_airport = airports[0]
						flight_dst_city = AIRPORT_DATA.get(flight_dst_airport, {}).get('municipality', '')

				return Flight(event.get('start_date'), event.get('end_date'), flight_src_city, flight_src_airport,
					flight_dst_city, flight_dst_airport, flight_num, self._origin)
				
		except Exception as exc:
			logging.error({'message': 'Flight object extraction', 'exc': str(exc)})
			pass

		# Accommodation
		try:
			stay_description = None
			accommodation_summary_match =  locale_consts.get_accommodation_regex().search(event.get('summary', ''))
			if accommodation_summary_match != None:
				stay_description = accommodation_summary_match.groups()[0]

			else:
				accommodation_summary_match = self._match_any_regex(locale_consts.get_all_accommodation_regex(),
																	event.get('summary', ''))
				if accommodation_summary_match != None:
					stay_description = accommodation_summary_match[0]

			if stay_description != None:
				return Accommodation(event.get('start_date'), event.get('end_date'),
					stay_description, event.get('location', ''), {self._origin_company : [OriginType.GOOGLE.name]})

		except Exception as exc:
			logging.error({'message': 'Accommodation object extraction', 'exc': str(exc)})
			pass

		# Tickets / Event / Reservations
		try:
			if event.get('created_by_google', False):
				# TODO: filter also by location? other event fingerprints?
				return Ticket(event.get('start_date'), event.get('end_date'), 
					event.get('summary', ''), event.get('location', ''), {self._origin_company : [OriginType.GOOGLE.name]})

		except Exception as exc:
			logging.error({'message': 'Ticket object extraction', 'exc': str(exc)})
			pass

		return None