import re

class LocaleRegex(object):
	def __init__(self, locale):
		self._default_locale = 'en'
		self._locale_hebrew = 'he'
		self._supported_locales = [self._default_locale, self._locale_hebrew]

		self._locale = self._default_locale
		if locale in self._supported_locales:
			self._locale = locale

		self._flight_regex = {
			self._default_locale: re.compile(r'Flight to (.*?) \((.*)\)'),
			self._locale_hebrew: re.compile(r'טיסה אל (.*?) \((.*)\)', re.UNICODE)
		}

		self._accommodation_regex = {
			self._default_locale: re.compile(r'Stay at (.*)'),
			self._locale_hebrew: re.compile(r'לינה ב-(.*)', re.UNICODE),
		}

	def get_flight_regex(self):
		return self._flight_regex[self._locale]

	def get_all_flight_regex(self):
		return list(self._flight_regex.values())

	def get_accommodation_regex(self):
		return self._accommodation_regex[self._locale]

	def get_all_accommodation_regex(self):
		return list(self._accommodation_regex.values())
