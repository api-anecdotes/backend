import logging
import itertools
from dateutil import parser as time_parser

from Tagging.travel_primitives import TravelPrimitive
from Tagging.data_origin import get_combined_origins
from Utils.tagging_helpers import get_all_tags, merge_all_tags, merge_generic_info
from Utils import cloud as cloud_utils


class TravelerProfile(object):
	def __init__(self, traveler_local_id):
		self._traveler_local_id = traveler_local_id
		self._timeline = []
		self._metadata = {}
		self._generic = {}
		self._tags = get_all_tags()

		self._load_profile()
		self._timeline = sorted(self._timeline, key=lambda x: x.get_start())

	def _load_profile(self):
		profile_dict = cloud_utils.get_traveler_profile(traveler_local_id=self._traveler_local_id)
		if profile_dict is None:
			return

		self._metadata = profile_dict.get('metadata', {})
		self._generic = profile_dict.get('generic', {})
		self._tags = profile_dict.get('tags', get_all_tags())
		raw_timeline = profile_dict.get('timeline', [])

		for raw_event in raw_timeline:
			primitive_obj = TravelPrimitive.primitive_factory(raw_event.get('type', None))
			if primitive_obj is None:
				logging.info({'message': 'raw primitive type mismatch', 'raw_event': str(raw_event)})
				continue

			raw_event.pop('type')
			raw_event.pop('id')

			travel_primitive = primitive_obj(**raw_event)
			# TODO: this should be False, unless for testing
			travel_primitive._is_object_new = False
			self._timeline.append(travel_primitive)

	def get_last_source_update(self, source):
		last_update = self._metadata.get('last_updated', None)
		if last_update:
			return last_update.get(source, None)
		return None

	def add_travel_primitives(self, travel_primitives):
		# Merge with local primitives
		if travel_primitives is None or 0 == len(travel_primitives):
			return False
		
		# Remove duplicates
		update = []
		for new_primitive in travel_primitives:
			new_primitive_id = new_primitive.get_id()
			duplicate = False

			for old_primitive in self._timeline:
				if old_primitive.get_id() == new_primitive_id:
					old_primitive._origin = get_combined_origins(old_primitive, new_primitive)
					duplicate = True
					break
			
			if duplicate:
				continue
			update.append(new_primitive)

		if 0 == len(update):
			return False

		self._timeline = sorted(self._timeline + update, key=lambda x: x.get_start())
		return True

	def update_last_source_update(self, source, timestamp):
		if 'last_updated' in self._metadata:
			self._metadata['last_updated'].update({source: timestamp})
		else:
			self._metadata['last_updated'] = {source: timestamp}

	def merge_generic_info(self, generic_info):
		merge_generic_info(self._generic, generic_info)

	def merge_tags(self, new_tags):
		merge_all_tags(self._tags, new_tags)

	def update_remote_profile(self):
		cloud_utils.BatchFirestoreWrite().update_traveler_profile(
			traveler_local_id=self._traveler_local_id,
			profile_dict={
				'metadata': self._metadata,
				'tags': self._tags,
				'generic': self._generic,
				'timeline': [event.to_dict() for event in self._timeline]})

	def get_timeline(self):
		return self._timeline

	def get_generic_info(self):
		return self._generic

	def get_tags(self):
		return self._tags
				


