import uuid

import Utils.cloud as cloud_utils
from Utils.dashboard_data import CounterName

def get_or_create_local_id(company_id, identifiers, should_hash=False):
	if not identifiers:
		return None

	if should_hash:
		identifiers = [cloud_utils.get_hash(identity) for identity in identifiers]
	local_id = cloud_utils.get_traveler_local_id(identifiers)

	# New customer
	if local_id is None:
		local_id = str(uuid.uuid4())
		cloud_utils.BatchFirestoreWrite().update_company_stats(company_id, CounterName.profiles.value, 1)

	cloud_utils.BatchFirestoreWrite().update_traveler_identifiers(identifiers=identifiers, local_id=local_id)
	return local_id