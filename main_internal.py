import os
import json
import random
import copy
import uuid
import logging

from flask import Flask, request
from flask_cors import CORS

import Utils.segment_builder as segment_builder
from Datasets.israir_catalog import ISRAIR_PRODUCTS_CATALOG
from Recommendations.engine import RecommendationsEngine
from Utils import cloud as cloud_utils
from Utils.dashboard_data import DashboardData
from Utils.tagging_helpers import get_flat_tags
from Utils.widgets_config import ALL_WIDGETS
from Tagging.tags import *


app = Flask(__name__)
CORS(app)


@app.route('/dashboard_data', methods=['GET'])
def get_dashboard_data():   
	company = request.args['company']

	counters_data = {}
	widgets_data = []
	counters_data = cloud_utils.get_counters_data(company)
	for widget in ALL_WIDGETS:
		try:
			widget.prepare_data()
			widgets_data.append(widget.get_widget_view_dict())

		except Exception as exc:
			print(exc)

	dashboard_data = DashboardData(company_id=company, widgets=widgets_data, **counters_data)

	return json.dumps(dashboard_data.to_dict()), 200

@app.route('/customer_id', methods=['GET'])
def get_customer_id():
	try:
		identifier = request.args.get('identifier')
		if identifier != None:
			identifier = identifier.lower()
			local_id = cloud_utils.get_traveler_local_id(identifiers=[cloud_utils.get_hash(identifier)], fallback_to_personal_ids=True)
			if local_id != None:
				return str(local_id), 200
	except:
		pass

	return 'Unknown Customer', 404

@app.route('/customer', methods=['GET'])
def get_customer():
	customer = { 
		'generic': {},
		'tags': {},
		}

	try:
		customer_id = request.args['customer_id']
		if customer_id != None:
			raw_profile = cloud_utils.get_traveler_profile(customer_id)

			if raw_profile != None:
				# Filter and convert profile data
				customer['generic'][GenericKeys.AgeRange.name] = raw_profile.get('generic', {}).get(GenericKeys.AgeRange.value, {}).get('data', None)
				customer['generic'][GenericKeys.Gender.name] = raw_profile.get('generic', {}).get(GenericKeys.Gender.value, {}).get('data', None)
				customer['generic'][GenericKeys.Profession.name] = raw_profile.get('generic', {}).get(GenericKeys.Profession.value, {}).get('data', None)
				customer['generic'][GenericKeys.Frequency.name] = raw_profile.get('generic', {}).get(GenericKeys.Frequency.value, {}).get('data', None)
				customer['generic'][GenericKeys.Budget.name] = raw_profile.get('generic', {}).get(GenericKeys.Budget.value, {}).get('data', None)
				customer['generic'][GenericKeys.Languages.name] = raw_profile.get('generic', {}).get(GenericKeys.Languages.value, {}).get('data', [])
				
				customer['generic'][GenericKeys.HotLead.name] = False
				hot_lead = raw_profile.get('generic', {}).get(GenericKeys.HotLead.value, {}).get('data', None)
				if hot_lead != None:
					customer['generic'][GenericKeys.HotLead.name] = True
					# TODO: remove hot lead is not hot anymore

				customer['tags'] = get_flat_tags(raw_profile.get('tags', {}))
	except:
		pass

	return json.dumps(customer), 200

def _internal_to_view_prod(product):
	tags = []
	raw_tags = product.get('tags', {})

	for name, value in raw_tags.get('raw_tags', {}).items():
		tags.append('{} {}'.format(name.value, value.value))

	for generic_key, generic_value in raw_tags.get('generic', {}).items():
		tags.append('{}: {}'.format(generic_key.value, generic_value.value))

	for tag in raw_tags.get('tags', []):
		tags.append(tag.value)

	viewable_product = copy.deepcopy(product)
	viewable_product['tags'] = tags
	return viewable_product

@app.route('/recommendations', methods=['GET'])
def get_recommendations():
	products = list(ISRAIR_PRODUCTS_CATALOG.values())
	random.shuffle(products)
	rand_products = [_internal_to_view_prod(prod) for prod in products[:3]]
	
	try:
		identifier = request.args.get('identifier')
		if identifier is None or identifier == '':
			return json.dumps(rand_products), 200

		engine = RecommendationsEngine(ISRAIR_PRODUCTS_CATALOG)
		raw_recommendations = engine.get_recommendations(identifier,fb_shikutz=False)
		return json.dumps([_internal_to_view_prod(prod) for prod in raw_recommendations]), 200
	except:
		pass

	return json.dumps(rand_products), 200

@app.route('/segment', methods=['GET'])
def get_segments():
	company_id = request.args['company']
	
	try:
		segments = cloud_utils.get_segments(company_id)
		return json.dumps(segments), 200
	except:
		pass

	return json.dumps([]), 200

@app.route('/segment_potential', methods=['POST'])
def segment_potential():
	company_id = request.args['company']
	
	try:
		tags = request.get_json().get('tags', [])
		logging.debug('segment_potential, tags = {}'.format(str(tags)))
		segment_query = segment_builder.craft_segment_query(tags)
		logging.debug('segment_potential, query = {}'.format(segment_query))
		potential = cloud_utils.get_segment_potential(company_id, segment_query)
		logging.debug('segment_potential, potential = {}'.format(potential))

		return json.dumps(potential), 200
	except Exception as exc:
		logging.error('segment_potential, exc = {}'.format(str(exc)))

	return json.dumps('0'), 200


@app.route('/segment', methods=['POST'])
def add_segment():
	company_id = request.args['company']
	
	try:
		data = request.get_json()
		if data != None:
			data['id'] = str(uuid.uuid4())[:8]
			cloud_utils.BatchFirestoreWrite().update_segments(company_id, data)
		return '200'
	except:
		pass

	return '200'

@app.route('/segment_download', methods=['GET'])
def download_segments():
	company_id = request.args['company']
	
	try:
		all_segments = cloud_utils.get_segments(company_id)
		segment_id = request.args.get('id')
		
		segment = None
		for sample in all_segments:
			if sample['id'] == segment_id:
				segment = sample
				break
		
		if segment != None:
			tags = segment['tags']
			segment_query = segment_builder.craft_segment_query(tags, potential_only=False)
			segment['data'] = cloud_utils.get_segment_identifiers(company_id, segment_query)

			return json.dumps(segment), 200

	except:
		pass

	return '500'

if __name__ == "__main__":
	app.run(debug=True,host='0.0.0.0',port=int(os.environ.get('PORT', 8080)))