import os
import json
import random
import datetime
import logging

from functools import wraps
from flask import Flask, request, session, abort
from flask_cors import CORS

from Social.google import Google
from Social.facebook import Facebook
from Tagging.manager import TaggingManager
from Traveler.profile import TravelerProfile
from Tagging.destination import Destination
from Tagging.data_origin import OriginType
from Utils import cloud as cloud_utils

app = Flask(__name__)
app.config['SECRET_KEY'] = '(d$WxND7o83j4FViuopOAdlfkBaq345m2;lkm46dlfm@Q$#FMk!4o'
CORS(app)

def authorize(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		if 'organization' in session:
			return f(*args, **kwargs)
		elif 'X-Api-Key' in request.headers:
			key = request.headers.get('X-Api-Key')
			key_info = cloud_utils.get_access_key_info(key)
			
			if key_info is None:
				abort(401)

			company = key_info.get('company_id')
			if company is None:
				abort(401)

			session['organization'] = company
			return f(*args, **kwargs)

		else:
			abort(401)

	return decorated

@app.route('/process', methods=['POST'])
@authorize
def process():
	logging.debug("/process called")
	tagging_manager = None
	profile = None
	request_data = request.get_json()
	logging.debug("request_data: {}".format(request_data))

	gg_token = request_data.get('Google_Token', None)
	if gg_token != None:
		google = Google(gg_token, should_override_data=True)
		google.read_raw_data()
		google.stage_raw_data_for_analysis()

		profile = TravelerProfile(traveler_local_id=google.get_user_local_id())
		profile.merge_tags(google.get_tags())
		profile.merge_generic_info(google.get_generic_info())
		profile.update_last_source_update('GOOGLE', datetime.datetime.now())
		profile.add_travel_primitives(google.get_travel_primitives())

		tagging_manager = TaggingManager(profile, session['organization'], OriginType.GOOGLE.name)
		tagging_manager.analyze()

		profile.update_remote_profile()

	fb_token = request_data.get('Facebook_Token', None)
	fb_locations = []
	if fb_token != None:
		facebook = Facebook(fb_token, should_override_data=True)
		facebook.extract_personal_ids()
		if facebook.is_valid():
			profile = TravelerProfile(traveler_local_id=facebook.get_user_local_id())
			fb_last_updated = profile.get_last_source_update('FACEBOOK')
			facebook.set_last_updated(fb_last_updated)

			logging.debug("facebook read_raw_data")
			facebook.read_raw_data()

			logging.debug("facebook stage_raw_data_for_analysis")
			facebook.stage_raw_data_for_analysis()

			profile.update_last_source_update(OriginType.FACEBOOK.name, datetime.datetime.now())
			profile.merge_generic_info(facebook.get_generic_info())
			fb_locations = facebook.get_travel_primitives()
			profile.add_travel_primitives(fb_locations)
			profile.merge_tags(facebook.get_tags())

			tagging_manager = TaggingManager(profile, session['organization'], OriginType.FACEBOOK.name)
			tagging_manager.analyze()

			logging.debug("facebook update_remote_profile")
			profile.update_remote_profile()

	return 'OK', 200


@app.route('/remove', methods=['POST'])
def clean_data():
	# Should delete user data - GDPR compliant
	return 'OK', 200

@app.route('/user_response', methods=['POST'])
def dummy_poll_data():
	return 'OK', 200

if __name__ == "__main__":
	app.run(debug=True,host='0.0.0.0',port=int(os.environ.get('PORT', 8080)))