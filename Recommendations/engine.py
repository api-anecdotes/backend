import random

import Utils.cloud as cloud_utils
from Utils.tagging_helpers import get_all_tags, get_flat_tags
from Tagging.tags import InterestTags


class RecommendationsEngine(object):
	def __init__(self, catalog):
		self._catalog = catalog

	def _get_random_products(self):
		products = list(self._catalog.values())
		random.shuffle(products)
		return products[:3]

	def get_recommendations(self, identifier, fb_shikutz=False):
		local_id = cloud_utils.get_traveler_local_id(identifiers=[identifier], fallback_to_personal_ids=True)
		if local_id is None:
			return self._get_random_products()

		profile_dict = cloud_utils.get_traveler_profile(local_id)
		if profile_dict is None:
			return self._get_random_products()

		generic = profile_dict.get('generic', {})
		tags = profile_dict.get('tags', get_all_tags())
		flat_tags = get_flat_tags(tags)

		product_match = {}

		# TODO: remove this flow
		if fb_shikutz:
			import logging
			logging.debug('fb_shikutz! remove me!!!')

			for product_id, product_desc in self._catalog.items():
				product_tags = product_desc['tags']['tags']
				consumer_tags = flat_tags

				# beach
				if InterestTags.beach in product_tags and InterestTags.beach.value in consumer_tags:
					product_match[product_id] = 1

				# ski
				if InterestTags.ski_and_snowboard in product_tags and InterestTags.ski_and_snowboard.value in consumer_tags:
					product_match[product_id] = 1

			sorted_matching_products = product_match

		else:
			for product_id, product_desc in self._catalog.items():
				generic_match = self._get_product_to_consumer_generic_match(product_desc['tags']['generic'], generic)
				tags_match = self._get_product_to_consumer_tags_match(product_desc['tags']['tags'], flat_tags)
				total_match = (generic_match + tags_match) / \
					(len(list(product_desc['tags']['generic'].keys())) + len(list(product_desc['tags']['tags'])))

				if 0 < total_match:
					product_match[product_id] = total_match

			sorted_matching_products = {k: v for k, v in sorted(product_match.items(), key=lambda item: item[1], reverse=True)}

		i = 0
		matching_products = []
		for product_id, match_rate in sorted_matching_products.items():
			matching_products.append(self._catalog[product_id])
			#self._catalog.pop(product_id)

			i += 1
			if 3 <= i:
				break

		if 3 > len(matching_products):
			products = list(self._catalog.values())
			random.shuffle(products)
			matching_products.extend(products[:3-len(matching_products)])

		return matching_products

	def _get_product_to_consumer_generic_match(self, product_generic, consumer_generic):
		match_rate = 0
		for key, value in product_generic.items():
			if consumer_generic.get(key.name, None) == value.value:
				match_rate += 1

		return match_rate

	def _get_product_to_consumer_tags_match(self, product_tags, consumer_tags):
		match_rate = 0
		for tag in product_tags:
			if tag.value in consumer_tags:
				match_rate += 1

		return match_rate

