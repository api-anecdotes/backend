from abc import ABC, abstractmethod
from enum import Enum
import logging

from Utils import cloud as cloud_utils
from Tagging.tags import GeoZoneSignatureTags, InterestTags, HotelStyleTags,\
	ThemeTravelerTags, SiteTypesTags
from Tagging.consts import text_per_hotel_style_tag, text_per_interest_tag, text_per_site_type_tag
from Utils.tagging_helpers import get_all_tags, increment_tag, add_tags_from_item_list_of_texts, add_tags_by_categories
from Utils.consts import WeekDays, COUNTRY_TO_CONTINENT, AFTER_SHABBAT_TIME
from Tagging.fb_categories import FB_ACCOMMODATION_CATEGORIES, FB_AIRPORT_CATEGORIES, FB_SKI_LOCATION_CATEGORIES

class LocationCategory(Enum):
	NO_CATEGORY = 0
	HOTEL = 1
	AIRPORT = 2
	MUSEUM = 3
	THEME_PARK = 4
	STADIUM = 5
	NATURE = 6
	CASINO = 7
	CLUB = 8
	# etc.

class PageCategory(Enum):
	NO_CATEGORY = 0
	HOTEL = 1
	AIRPORT = 2
	MUSEUM = 3
	THEME_PARK = 4
	STADIUM = 5
	NATURE = 6
	CASINO = 7
	CLUB = 8
	SPORT = 9
	# etc.

class SportCategories(Enum):
	NO_CATEGORY = 0
	SKI = 1
	BASKETBALL = 2
	# etc.

class SportCategory(Enum):
	NO_CATEGORY = 0


class TravelPrimitive(ABC):
	def __init__(self, start, end, data_origin):
		self._start = start
		self._end = end
		# origin is a list of DataOrigins, each one is a tuple consists of the origin company (customer) and the origin
		# type (i.e. social media / on-premises data / etc.)
		self._origin = data_origin
		self._tags = get_all_tags()
		# This is needed for us not to process the same primitive multiple times and create double tags
		self._is_object_new = True

	def get_start(self):
		return self._start

	def get_end(self):
		return self._end

	def get_duration_seconds(self):
		return (self._end - self._start).total_seconds()

	def get_data_origin(self):
		return self._origin

	def get_tags(self):
		return self._tags

	def _is_new(self):
		return self._is_object_new

	@staticmethod
	def primitive_factory(primitive_type):
		if 'Flight' == primitive_type:
			return Flight

		if 'Accommodation' == primitive_type:
			return Accommodation

		if 'Ticket' == primitive_type:
			return Ticket

		if 'Location' == primitive_type:
			return Location

		return None

	@abstractmethod
	def to_dict(self):
		pass

	@abstractmethod
	def get_id(self):
		pass

	def __str__(self):
		return str(self.to_dict())

	def __repr__(self):
		return str(self)

class Location(TravelPrimitive):
	def __init__(self, start, end, city, country, latitude, longitude, categories, name, data_origin):
		super().__init__(start, end, data_origin)
		self._city = city
		self._country = country
		self._latitude = latitude
		self._longitude = longitude
		self._categories = categories
		self._name = name
		self._is_flight = False
		self._is_accommodation = False
		self._is_ticket = False

	def get_city(self):
		return self._city or ''

	def get_country(self):
		return self._country or ''

	def get_latitude(self):
		return self._latitude or ''

	def get_longitude(self):
		return self._longitude or ''

	def get_name(self):
		return self._name or ''

	def get_categories(self):
		return self._categories

	def get_id(self):
		return cloud_utils.get_hash('{start}{end}{latitude}{longitude}{city}{country}'.format(
			start=str(self._start),
			end=str(self._end),
			latitude=str(self._latitude),
			longitude=str(self._longitude),
			city=str(self._city),
			country=str(self._country)))

	def get_tags(self):
		self._tag_location()
		return self._tags

	def _tag_hotel_style(self, accommodation_category):
		add_tags_from_item_list_of_texts(
			[self.get_name().lower()],
			text_per_hotel_style_tag,
			HotelStyleTags,
			self._tags,
			self.get_data_origin(),
			description="Location '{}'".format(self.get_name()))

	def _tag_location(self):
		# Geographic zones
		country = self.get_country().lower()
		geo_zone = COUNTRY_TO_CONTINENT[country]
		if geo_zone:
			logging.debug("increment GeoZoneSignature tag '{tag}' due to '{description}'".format(
				tag=GeoZoneSignatureTags[geo_zone].value,
				description="Location '{}' tagged in {}, {}".format(self.get_name(), self.get_city(), country)))
			increment_tag(self._tags, GeoZoneSignatureTags[geo_zone], self._origin)

		# tag by location name
		location_name = self.get_name().lower()
		add_tags_from_item_list_of_texts(
			[location_name],
			text_per_interest_tag,
			InterestTags,
			self._tags,
			self.get_data_origin(),
			description="Location '{}'".format(
				self.get_name()))
		add_tags_from_item_list_of_texts(
			[location_name],
			text_per_site_type_tag,
			SiteTypesTags,
			self._tags,
			self.get_data_origin(),
			description="Location '{}'".format(
				self.get_name()))

		# tag by location categories
		location_categories = [category.lower() for category in self.get_categories()]
		description = "Location '{}' categories: {}".format(self.get_name(), location_categories)
		add_tags_by_categories(location_categories, self._tags, self._origin, description=description)

		# TODO: maybe remove the following flow (accommodation, airport)
		for category in location_categories:
			# accommodation
			for accommodation_category in FB_ACCOMMODATION_CATEGORIES:
				if category == accommodation_category:
					self._is_accommodation = True
					self._tag_hotel_style(accommodation_category)

			if self._is_accommodation:
				break

			# airport
			for airport_category in FB_AIRPORT_CATEGORIES:
				if category == airport_category.lower():
					self._is_flight = True

			if self._is_flight:
				break

	def to_dict(self):
		return {
			'start': self._start,
			'end': self._end,
			'city' : self._city,
			'country' : self._country,
			'latitude' : self._latitude,
			'longitude' : self._longitude,
			'type': 'Location',
			'categories': self._categories,
			'id': self.get_id(),
			'name' : self._name,
			'data_origin': self._origin,
		}

class Flight(TravelPrimitive):
	def __init__(self, start, end, src_city, src_airport, dst_city, dst_airport, flight_num, data_origin):
		super().__init__(start, end, data_origin)
		self._src_city = src_city
		self._src_airport = src_airport
		self._dst_city = dst_city
		self._dst_airport = dst_airport
		self._flight_num = flight_num
		self._tag_flight()

	def _tag_flight(self):
		# TAG: shabbat / non-shabbat flight
		if WeekDays.Saturday.value == self.get_start().weekday() and \
			self.get_start().hour < AFTER_SHABBAT_TIME or \
			WeekDays.Saturday.value == self.get_end().weekday() and \
			self.get_end().hour < AFTER_SHABBAT_TIME:
			increment_tag(self._tags, ThemeTravelerTags.ShabbatFlights, self._origin)
		else:
			increment_tag(self._tags, ThemeTravelerTags.NoShabbatFlights, self._origin)

	def get_src_city(self):
		return self._src_city

	def get_src_airport(self):
		return self._src_airport

	def get_dst_city(self):
		return self._dst_city

	def get_dst_airport(self):
		return self._dst_airport

	def get_flight_num(self):
		return self._flight_num

	def get_id(self):
		flight_id = str(self._src_city) + str(self._dst_city)
		if self._flight_num:
			flight_id = str(self._flight_num)

		return cloud_utils.get_hash('{start}{end}{flight_id}'.format(
			start=str(self._start),
			end=str(self._end),
			flight_id=flight_id))

	def to_dict(self):
		return {
			'start': self._start,
			'end': self._end,
			'src_city' : self._src_city,
			'src_airport' : self._src_airport,
			'dst_city' : self._dst_city,
			'dst_airport' : self._dst_airport,
			'flight_num' : self._flight_num,
			'type': 'Flight',
			'id': self.get_id(),
			'data_origin': self._origin,
		}

class Accommodation(TravelPrimitive):
	def __init__(self, start, end, place, location, data_origin, stars=None, experience=None):
		super().__init__(start, end, data_origin)
		self._place = place
		self._location = location
		self._stars = stars
		self._experience = experience

	def get_place(self):
		return self._place

	def get_location(self):
		return self._location

	def get_id(self):
		return cloud_utils.get_hash('{start}{end}{place}{location}'.format(
			start=str(self._start),
			end=str(self._end),
			place=self._place,
			location=self._location))

	def to_dict(self):
		return {
			'start': self._start,
			'end': self._end,
			'place': self._place,
			'location': self._location,
			'type': 'Accommodation',
			'id': self.get_id(),
			'data_origin': self._origin,
			'stars': self._stars,
			'experience': self._experience,
		}

	def get_tags(self):
		self._tag_accommodation()
		return self._tags

	def _tag_accommodation(self):
		# Tag Interest tags by hotel name
		add_tags_from_item_list_of_texts(
			[self.get_place()],
			text_per_interest_tag,
			InterestTags,
			self._tags,
			self.get_data_origin(),
			description="accommodation '{}'".format(self.get_place()))

		# Tag HotelStyle tags by hotel name
		add_tags_from_item_list_of_texts(
			[self.get_place()],
			text_per_hotel_style_tag,
			HotelStyleTags,
			self._tags,
			self.get_data_origin(),
			description="accommodation '{}'".format(self.get_place()))

class Ticket(TravelPrimitive):
	def __init__(self, start, end, description, location, data_origin):
		super().__init__(start, end, data_origin)
		self._description = description
		self._location = location

	def get_description(self):
		return self._description

	def get_location(self):
		return self._location

	def get_id(self):
		return cloud_utils.get_hash('{start}{end}{description}{location}'.format(
			start=str(self._start),
			end=str(self._end),
			description=self._description,
			location=self._location))

	def to_dict(self):
		return {
			'start': self._start,
			'end': self._end,
			'description': self._description,
			'location': self._location,
			'type': 'Ticket',
			'id': self.get_id(),
			'data_origin': self._origin,
		}