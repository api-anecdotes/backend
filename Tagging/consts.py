from enum import Enum
from collections import defaultdict
from Tagging.tags import HotelStyleTags, InterestTags, SiteTypesTags


class AnalysisConsts(Enum):
	max_connection_hours = 12
	max_journey_filghts_distance_days = 30
	max_accommodation_distance_days = 7
	max_destinations_distance_days = 5
	weekend_day_begin = 3

text_per_hotel_style_tag = defaultdict(list)
text_per_hotel_style_tag.update({
	HotelStyleTags.BasicHotelBrand: [],
	HotelStyleTags.ThreeStarsHotel: [],
	HotelStyleTags.BedAndBreakfast: ['bed and breakfast', 'b&b', 'b & b', 'bnb'],
	HotelStyleTags.AirportHotel: ['airport'],
	HotelStyleTags.BoutiqueHotel: ['boutique'],
	HotelStyleTags.SpaHotel: [' spa '],
	HotelStyleTags.LuxuryHotelBrand: [],
	HotelStyleTags.FiveStarsHotel: [],
	HotelStyleTags.PremiumHotelBrand: [],
	HotelStyleTags.FourStarsHotel: [],
	HotelStyleTags.AptHotel: ['apartment'],
	HotelStyleTags.CityCenterHotel: ['city center'],
	HotelStyleTags.Resort: ['resort'],
	HotelStyleTags.Airbnb: ['airbnb'],
	HotelStyleTags.LowCostLodging: ['hostel', 'lodge', 'inn', 'motel'],
})

text_per_interest_tag = defaultdict(list)
text_per_interest_tag.update({
	InterestTags.ski_and_snowboard:			[' ski ', 'snowboard', 'skiing', 'winter sport', ' סקי ', 'סנובורד'],
	InterestTags.scuba_diving:		[' diving ', ' dive ', 'scuba', 'צלילה', 'צולל'],
	InterestTags.swimming:		[' swimming ', ' swim ', ' שחיה ', 'שחייה', 'בריכה'],
	InterestTags.cycling:		['biking', ' bike ', ' cycling ', 'אופניים'],
	InterestTags.running:		['running ', ' run ', ' ריצה ', 'מרתון', 'marathon', 'לרוץ'],
	InterestTags.surfing:		[' surfing ', ' surf ', 'גלישת גלים'],
	InterestTags.israeli_music:	['israeli music'],
	InterestTags.football:		['football', 'soccer', 'champions league', 'premier league', 'la liga', ' messi ', 'ronaldo', 'כדורגל'],
	InterestTags.basketball:	['basketball', ' NBA ', 'euroleague', 'כדורסל'],
	InterestTags.tennis:		['tennis', 'federer', ' nadal ', 'djokovic', ' טניס '],
	InterestTags.boxing:		['boxing', 'kickboxing', ' אגרוף ', ' איגרוף '],
	InterestTags.golf:			[' golf ', 'גולף'],
	InterestTags.gambling:		['gambling', ' gamble ', ' casino', 'קזינו', 'הימורים'],
	InterestTags.art:			[' art ', 'אמנות', 'אומנות'],
	InterestTags.beach:			[' beach ', 'bahamas', ' חוף'],
	InterestTags.well_being:	['well being'],
	InterestTags.yoga:			['yoga', 'יוגה'],
	InterestTags.photography:	['photography', ' צילום'],
	InterestTags.fishing:		['fishing', ' לדוג ', ' דייג '],
	InterestTags.martial_arts:	['martial arts', 'karate', 'judo', 'taekwondo', 'jujutsu', 'אמנות לחימה', 'אמנויות לחימה', 'אומנויות לחימה', 'קראטה', 'קרב מגע', "ג'ודו", "ג'וג'יטסו"],
	InterestTags.rock_climbing:	['rock climbing', 'climbing wall', 'climbing gym', 'bouldering', 'קיר טיפוס', 'טיפוס סלע', 'בולדרינג'],
	InterestTags.music:			['music', 'מוזיקה', 'מוסיקה'],
	InterestTags.electronic_music:			['electronic music', 'DJ', "מוזיקה אלקטרונית", "דיג'יי", "די-ג'יי"],
	InterestTags.classic_music:			['classic music', "מוזיקה קלאסית", "מוסיקה קלאסית"],
	InterestTags.jazz:			['jazz', " ג'אז "],
	InterestTags.opera:			[' opera ', " אופרה "],
	InterestTags.live_shows:	['live show', 'amphitheater', 'comedy club', ' circus ', 'zappa', " זאפה ", "הופעה חיה", "אמפיתיאטרון", " קרקס "],
	InterestTags.dancing:		['dancing', ' ריקוד ', ' מחול '],
	InterestTags.movies:		['movies', ' קולנוע ', ' סינמה סיטי ', 'יס פלאנט'],
	InterestTags.meditation:	['meditation', 'headspace', 'מדיטציה'],
	InterestTags.alternative_health:	['alternative medicine', 'alternative health', 'shiatsu', 'naturopathy', 'reflexology', 'acupuncture', 'homeopathy', "רפואה אלטרנטיבית", "שיאצו", "נטורופתיה", "רפלקסולוגיה", "דיקור סיני", "הומאופתיה"],
	InterestTags.fitness:		['fitness', 'pilates', 'gym', 'holmes place', 'great shape', 'go active', ' כושר ', 'אימון', 'התעמלות', 'פילאטיס', 'הולמס פלייס', 'גו אקטיב', 'גרייט שייפ'],
	InterestTags.beauty_and_cosmetics:	['beauty', 'cosmetics', 'make up', 'pedicure', 'menicure', 'קוסמטיקה', ' טיפוח ', ' איפור ', 'מכון יופי', 'מניקור', 'פדיקור'],
	InterestTags.spa:	[' spa ', 'hammam', ' ספא ', 'חמאם'],
	InterestTags.massage:	[' massage ', "מסאז'", ' עיסוי ', "מסאג'"],
	InterestTags.art_gallery:	['art gallery', 'paintings gallery', 'גלריה לאמנות', 'גלריה לאומנות', 'גלריית ציורים', 'גלריית תמונות'],
	InterestTags.performance_art:	['performance art', 'dancing', ' תיאטרון'],
	InterestTags.museum:	['museum', 'מוזיאון'],
	InterestTags.temple:	['temple', 'synagogue', 'mosque', 'church', ' מקדש ', ' בית כנסת ', ' כנסייה ', ' כנסיה ', ' מסגד '],
	InterestTags.hiking:	[' hike ', 'hiking', ' הליכה ', ' מסלול '],
	InterestTags.trekking:	[' trek ', 'trekking', ' טרק '],
	InterestTags.nature:	['forest', 'river', 'national park', 'nature preserve', 'פארק לאומי', 'שמורת טבע', ' פריחה ', ' יער ', ' חורשה ', ' חורשת '],
	InterestTags.campground:	['campground', ' מחנה ', 'חניון לילה', ' מחנאות ', ' קמפינג '],
	InterestTags.park:	['park', ' פיקניק '],
	InterestTags.design:	['עיצוב ', ' מעצב '],
	InterestTags.technology:	['technology', 'high-tech', 'hi-tech', 'entrepreneurship', 'טכנולוגיה', 'הייטק', 'היי-טק', 'יזמות	', 'סטארטאפ'],
	InterestTags.bar:	[' bar ', ' pub ', ' פאב '],
	InterestTags.whisky:	[' whisky ', ' ויסקי ', ' וויסקי '],
	InterestTags.wine:	[' wine ', ' vineyard ', ' יין ', ' יינות ', ' יקב ', ' יקבים '],
	InterestTags.night_club:	['night club', 'dance bar', 'dance club', 'pickup bar', ' מועדון '],
	InterestTags.zoo:	['zoo', 'safari', 'aquarium', 'גן חיות', ' ספארי', 'אקווריום', 'גן החיות', 'פינת חי'],
	InterestTags.animals:	['וטרינר', 'ווטרינר', 'תנו לחיות לחיות'],
	InterestTags.amusement_park:	['amusement park', 'theme park', 'water park', 'eurodisney', 'disneyworld', 'פארק שעשועים', 'פארק מים', 'סופרלנד', 'לונה פארק'],
	InterestTags.cafe:	[' cafe ', 'בית קפה'],
	InterestTags.restaurant:	[' restaurant ', 'מסעדה'],
	InterestTags.cooking:	[' cooking ', 'בישול'],
})

text_per_site_type_tag = defaultdict(list)
text_per_site_type_tag.update({
	SiteTypesTags.FoodMarket:	[' food market ', 'שוק אוכל', 'שוק איכרים'],
	SiteTypesTags.Monuments:	[' monument', 'statue', ' פסל ', ' מונומנט '],
	SiteTypesTags.Castles:	[' castle ', ' palace ', ' טירה ', ' ארמון '],
	SiteTypesTags.FleaMarket:	[' flee market ', ' שוק הפשפשים ', ' שוק פשפשים ', ' שוק מציאות '],
	SiteTypesTags.ShoppingMall:	[' shopping mall ', ' קניון', ' שוק פשפשים ', ' שוק מציאות '],
})

FB_FIELDS_TO_GENERIC_KEYS = {
	'age_range' : 'Age Range',
	'gender' : 'Gender',
	'languages' : 'Languages',
	'birthday' : 'Birthday',
}