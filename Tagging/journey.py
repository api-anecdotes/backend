import pycountry
import logging
from datetime import timedelta

from Tagging.tags import JourneyTags, GeoZoneTags, ThemeTravelerTags, JourneyDurationTags
from Utils.tagging_helpers import get_all_tags, merge_all_tags, increment_tag, override_tag
from Utils.consts import WeekDays
from Utils.consts import COUNTRY_TO_CONTINENT
from Datasets.airport_data import AIRPORT_DATA


class Journey(object):
	def __init__(self):
		self._journey_origin_name = None
		self._destinations = []
		self._tags = get_all_tags()
		self._start = None
		self._is_object_new = True
		self._origins = {}
		self._home_country = None

	def set_home_country(self, home_country):
		self._home_country = home_country.lower()

	def _update_tags(self):
		journey_begin = self._destinations[0].get_start()
		journey_end = self._destinations[-1].get_end()

		# TAG: weekend
		if  journey_begin.weekday() in [WeekDays.Thursday.value, WeekDays.Friday.value] and \
			journey_end.weekday() in [WeekDays.Saturday.value, WeekDays.Sunday.value]:
			increment_tag(self._tags, ThemeTravelerTags.Weekend, self._origins)

		# TAG: trip duration
		if  (journey_end - journey_begin).total_seconds() <= timedelta(days=4).total_seconds():
			increment_tag(self._tags, JourneyDurationTags.short, self._origins)

		elif  (journey_end - journey_begin).total_seconds() <= timedelta(days=14).total_seconds():
			increment_tag(self._tags, JourneyDurationTags.moderate, self._origins)

		elif  (journey_end - journey_begin).total_seconds() > timedelta(days=14).total_seconds():
			increment_tag(self._tags, JourneyDurationTags.long, self._origins)

		# TAG: summer
		if  journey_begin.month in [7,8] and journey_end.month in [7,8]:
			increment_tag(self._tags, ThemeTravelerTags.Summer, self._origins)

		# TAG: winter
		if  journey_begin.month in [12,1,2] and journey_end.month in [12,1,2]:
			increment_tag(self._tags, ThemeTravelerTags.Winter, self._origins)

		# TAG: length
		total_trip_duration = (journey_end - journey_begin).days
		if 0 < total_trip_duration:
			override_tag(self._tags, JourneyTags.total_length, self._origins, [total_trip_duration])
		
		# TAGs: outbound travel day & month
		override_tag(self._tags, JourneyTags.journey_start_day, self._origins, [journey_begin.weekday()])
		override_tag(self._tags, JourneyTags.journey_start_month, self._origins, [journey_begin.month])

		# TAGs: inbound travel day & month
		override_tag(self._tags, JourneyTags.journey_end_day, self._origins, [journey_end.weekday()])
		override_tag(self._tags, JourneyTags.journey_end_month, self._origins, [journey_end.month])

		# TAG: days per destination - flights
		dest_length = []
		if 1 < len(self._destinations):
			journeys_iter = iter(self._destinations)
			destination = next(journeys_iter, None)
			while destination:
				next_destination = next(journeys_iter, None)
				if next_destination is None:
					break

				between_flights_duration = (next_destination.get_start() - destination.get_start()).days
				dest_length.append(between_flights_duration)
				destination = next_destination
		override_tag(self._tags, JourneyTags.per_destination_length, self._origins, dest_length)

		# add geo-zone tagging for every destination in the journey, except for the home destination
		for destination in self.get_destinations():
			try:
				dst_airport = destination.get_route()[-1].get_dst_airport()
				if dst_airport is None:
					# we can't tell what was the actual destination here, go to next destination
					continue
				country = pycountry.countries.get(alpha_2=AIRPORT_DATA[dst_airport]['iso_country']).name.lower()
				if self._home_country and country == self._home_country:
					# This is the flight to return home. If the journey has only one destination, we missed the flight
					# to the destination. The source of the first flight in the route is probably the actual destination
					if len(self._destinations) == 1:
						dst_airport = destination.get_route()[0].get_src_airport()
						country = pycountry.countries.get(alpha_2=AIRPORT_DATA[dst_airport]['iso_country']).name.lower()
					else:
						# This is just the home destination in the journey, so skip it
						continue
				geo_zone = COUNTRY_TO_CONTINENT[country]
				if geo_zone:
					logging.debug("increment GeoZone tag '{tag}' due to '{description}'".format(
						tag=GeoZoneTags[geo_zone].value,
						description="Journey flight to/from {}".format(country)))
					increment_tag(self._tags, GeoZoneTags[geo_zone], self._origins)
			except KeyError:
				# Are we sure about this? (Y)
				pass

		return self._tags

	def add_destination(self, destination_obj):
		if 0 == len(self._destinations):
			self._journey_origin_name = destination_obj.get_src_city() or destination_obj.get_name()
			self._start = destination_obj.get_start()

		self._destinations.append(destination_obj)
		self._origins = {**self._origins, **destination_obj.get_data_origin()}

		# update with destination existing tags, unless it was already tagged in the past
		if destination_obj._is_new():
			merge_all_tags(self._tags, destination_obj.get_tags())
		else:
			self._is_object_new = False

	def get_tags(self):	
		self._update_tags()
		return self._tags

	def get_start(self):
		return self._start

	def get_destinations(self):
		return self._destinations

	def _is_new(self):
		return self._is_object_new

	def __str__(self):
		if 1 < len(self._destinations):
			dst = str(list(map(lambda x:x.get_name(), self._destinations[:-1])))
		else:
			dst = str(list(map(lambda x:x.get_name(), self._destinations)))
		return 'src: {src}\ndst: {dst}\nTags: {tags}\n'.format(
				src=self._journey_origin_name,
				dst=dst,
				tags=str(self.get_tags()))

	def __repr__(self):
		return str(self)
