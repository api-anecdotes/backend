import logging
import math
import pycountry
from datetime import datetime, timedelta
from collections import defaultdict

from Tagging.consts import AnalysisConsts
from Tagging.destination import Destination
from Tagging.journey import Journey
from Tagging.data_origin import OriginCompany
from Tagging.travel_primitives import Flight, Accommodation, Ticket, Location
from Tagging.tags import JourneyTags, InterestTags, GeoZoneTags, HotelStyleTags, GenericKeys, GenericValues
from Utils.tagging_helpers import get_all_tags, merge_all_tags, build_generics_with_origin
from Datasets.airport_data import AIRPORT_DATA

class TaggingManager(object):
	def __init__(self, traveler_profile, origin_company, origin_type):
		self._tags = get_all_tags()
		self._generic = {}
		self._destinations = []
		self._journeys = []
		self._traveler_profile = traveler_profile
		self._origin_company = origin_company or OriginCompany.UNKNOWN_COMPANY.name
		self._origin = {self._origin_company: [origin_type]}
		self._home_country = None
		
	def get_tags(self):
		return self._tags

	def get_destinations(self):
		return self._destinations

	def get_journeys(self):
		return self._journeys

	def _destinations_to_journeys(self):
		self._journeys = []

		destinations_iter = iter(self._destinations)
		destination = next(destinations_iter, None)
		src_city = None
		previous_destination = None
		current_journey = None

		while destination:
			route = destination.get_route()
			local_city = route[0].get_src_city() if route else None
			dst_city = destination.get_name()

			if src_city is None:
				# destinations without flights, connection based on time gaps
				if current_journey != None and previous_destination != None:
					if (destination.get_start() - previous_destination.get_start()).total_seconds() > \
						timedelta(days=AnalysisConsts.max_destinations_distance_days.value).total_seconds():
						self._journeys.append(current_journey)
						current_journey = Journey()
					
					current_journey.add_destination(destination)
					src_city = local_city
					previous_destination = destination	
				else:
					# brand new journey
					current_journey = Journey()
					current_journey.add_destination(destination)
					src_city = local_city
					previous_destination = destination

			elif local_city == src_city:
				# new journey - previous journey without closure
				self._journeys.append(current_journey)
				current_journey = Journey()
				current_journey.add_destination(destination)
				src_city = local_city
				previous_destination = destination

			elif dst_city == src_city and \
					(destination.get_start() - previous_destination.get_start()).total_seconds() < \
					timedelta(days=AnalysisConsts.max_journey_filghts_distance_days.value).total_seconds():
				# complete journey
				current_journey.add_destination(destination)
				self._journeys.append(current_journey)
				current_journey = Journey()
				src_city = None
				previous_destination = None

			elif previous_destination != None and \
					(destination.get_start() - previous_destination.get_start()).total_seconds() < \
					timedelta(days=AnalysisConsts.max_journey_filghts_distance_days.value).total_seconds():
				# multi destination journey
				current_journey.add_destination(destination)
				previous_destination = destination

			else:
				# maybe a one way destination
				self._journeys.append(current_journey)
				current_journey = Journey()
				current_journey.add_destination(destination)
				src_city = local_city
				previous_destination = destination
				
			destination = next(destinations_iter, None)
		
		if current_journey != None and 0 < len(current_journey.get_destinations()):
			self._journeys.append(current_journey)
	
	def _handle_single_flight(self, flight, current_destination):
		last_flight = current_destination.get_last_flight()

		# TODO: Handle unreachable accommodation

		# First flight of destination
		if last_flight is None:
			current_destination.add_connection(flight)
			return None
		
		# Next flight
		if last_flight.get_dst_city() == flight.get_src_city():

			# Connection
			if (flight.get_start() - last_flight.get_end()).total_seconds() < \
				timedelta(hours=AnalysisConsts.max_connection_hours.value).total_seconds():
				current_destination.add_connection(flight)
				return None
			
		# Multi-destination (some / different cities)	
		self._destinations.append(current_destination)
		current_destination = Destination()
		current_destination.add_connection(flight)
		return current_destination

	def _handle_single_accommodation(self, accommodation, current_destination):
		last_flight = current_destination.get_last_flight()
		last_accommodation = current_destination.get_last_accommodation()

		# "Unreachable accommodation"
		if last_flight is None:

			# First accommodation
			if last_accommodation is None:
				current_destination.add_accommodation(accommodation)
				return None

			# Multiple accommodations in the same destination
			elif (accommodation.get_start() - last_accommodation.get_end()).total_seconds() < \
					timedelta(days=AnalysisConsts.max_accommodation_distance_days.value).total_seconds():
					current_destination.add_accommodation(accommodation)
					return None	
				
			else:
				self._destinations.append(current_destination)
				current_destination = Destination()
				current_destination.add_accommodation(accommodation)
				return current_destination

		# Accommodation short after a flight
		elif (accommodation.get_start() - last_flight.get_end()).total_seconds() < \
				timedelta(days=AnalysisConsts.max_accommodation_distance_days.value).total_seconds():
				current_destination.add_accommodation(accommodation)
				return None	

		# One way travel (really..?) and unreachable accommodation
		else:
			self._destinations.append(current_destination)
			current_destination = Destination()
			current_destination.add_accommodation(accommodation)
			return current_destination

	def _handle_single_location(self, location):
		# update with location existing tags, unless it was already tagged in the past
		if location._is_new():
			merge_all_tags(self._tags, location.get_tags())

	def _convert_location_to_iata_code(self, location):
		return 'N/A'

	def _timeline_to_destinations(self):
		self._destinations = []

		current_destination = Destination()
		primitives_iter = iter(self._traveler_profile.get_timeline())
		travel_primitive = next(primitives_iter, None)

		while travel_primitive:

			# handle flights
			if isinstance(travel_primitive, Flight):
				current_destination = self._handle_single_flight(travel_primitive, current_destination) \
					or current_destination

			# handle accommodation
			elif isinstance(travel_primitive, Accommodation):
				current_destination = self._handle_single_accommodation(travel_primitive, current_destination) \
					or current_destination

			# handle facebook location (could be accommodation / airport etc.)
			elif isinstance(travel_primitive, Location):
				self._handle_single_location(travel_primitive)

			travel_primitive = next(primitives_iter, None)
			continue

		if current_destination.get_name() != None:
			self._destinations.append(current_destination)

	def _merge_tags(self):
		# Update destination and journey tags
		for journey in self.get_journeys():
			# update with journey existing tags, unless it was already tagged in the past
			if journey._is_new():
				journey.set_home_country(self._home_country)
				merge_all_tags(self._tags, journey.get_tags())

	def _extract_generic_info(self):
		current_generic = self._traveler_profile.get_generic_info()
		new_generic = {}

		if self._home_country is not None:
			new_generic[GenericKeys.HomeCountry.value] = self._home_country

		yearly_frequency = defaultdict(int)
		hot_lead = None
		for journey in self._journeys:
			yearly_frequency[journey.get_start().year] += 1
			if journey.get_start() > datetime.now().astimezone():
				hot_lead = journey.get_start()

		# Travel frequency
		previous_journeys_count = len(self._traveler_profile.get_tags()[JourneyTags.__name__].get(JourneyTags.total_length.value, []))
		previous_frequency = current_generic.get(GenericKeys.Frequency.value)
		if 0 < len(list(yearly_frequency.keys())) + previous_journeys_count:
			new_frequency = 0
			if previous_frequency == GenericValues.FrequencyLow.value:
				new_frequency += previous_journeys_count # *1
			elif previous_frequency == GenericValues.FrequencyMedium.value:
				new_frequency += previous_journeys_count * 3
			elif previous_frequency == GenericValues.FrequencyHigh.value:
				new_frequency += previous_journeys_count * 6
			
			new_frequency += sum(list(yearly_frequency.values()))
			new_frequency = math.ceil(new_frequency / (len(list(yearly_frequency.keys())) + previous_journeys_count))

			if new_frequency < 2:
				new_generic[GenericKeys.Frequency.value] = GenericValues.FrequencyLow.value
			elif new_frequency < 4:
				new_generic[GenericKeys.Frequency.value] = GenericValues.FrequencyMedium.value
			else:
				new_generic[GenericKeys.Frequency.value] = GenericValues.FrequencyHigh.value

			# Hot lead
			if hot_lead != None:
				new_generic[GenericKeys.HotLead.value] = [hot_lead]

		self._generic = build_generics_with_origin(new_generic, self._origin)

	def _get_home_country_from_journeys(self):
		country_count = defaultdict(int)
		for journey in self._journeys:
			try:
				# get the journey origin country, and the journey end country
				destinations = journey.get_destinations()
				origin_airport = destinations[0].get_route()[0].get_src_airport()
				if origin_airport:
					origin_country = pycountry.countries.get(alpha_2=AIRPORT_DATA[origin_airport]['iso_country']).name.lower()
					if origin_country:
						country_count[origin_country] += 1

				end_airport = destinations[-1].get_route()[-1].get_dst_airport()
				if end_airport:
					end_country = pycountry.countries.get(alpha_2=AIRPORT_DATA[end_airport]['iso_country']).name.lower()
					if end_country:
						country_count[end_country] += 1
			except:
				continue


		if country_count:
			# return the country with the highest amount of appearances
			return max(country_count, key=country_count.get)
		else:
			return None

	def analyze(self):
		self._destinations = []

		# Iterate timeline of travel primitives and craft destinations
		self._timeline_to_destinations()

		# Split destinations to separate journeys
		self._destinations_to_journeys()

		# try to deduce home-country by most frequent destination
		self._home_country = self._get_home_country_from_journeys()

		# Extract and merge tags
		self._merge_tags()
		self._traveler_profile.merge_tags(self._tags)

		# Extract and merge generic info
		self._extract_generic_info()
		self._traveler_profile.merge_generic_info(self._generic)

		
