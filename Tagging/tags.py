from enum import Enum

class DestinationTags(Enum):
	direct = 'Direct'
	single_connection = 'Single Connection'
	multi_connection = 'Multi Connection'

class JourneyTags(Enum):
	total_length = 'Journey Length'
	per_destination_length = 'Destination Length'
	
	journey_start_day = 'Journey Start Day'
	journey_start_month = 'Journey Start Month'

	journey_end_day = 'Journey End Day'
	journey_end_month = 'Journey End Month'

class InterestTags(Enum):
	ski_and_snowboard = 'Ski and Snowboard'
	scuba_diving = 'Diving'
	boxing = 'Boxing'
	golf = 'Golf'
	tennis = 'Tennis'
	swimming = 'Swimming'
	trekking = 'Trekking'
	cycling = 'Biking'
	running = 'Running'
	surfing = 'Surfing'
	music = 'Israeli Music'
	jazz = 'Jazz'
	opera = 'Opera'
	electronic_music = 'Electronic Music'
	classic_music = 'Classic Music'
	dancing = 'Dancing'
	israeli_music = 'Israeli Music'
	live_shows = 'Live Shows'
	football = 'Football'
	basketball = 'Basketball'
	fitness = 'Fitness'
	gambling = 'Gambling'
	art = 'Art'
	night_club = 'Night Club'
	beach = 'Beach'
	well_being = 'Well Being'
	yoga = 'Yoga'
	culture = 'Culture'
	shopping = 'Shopping'
	sport = 'Sport'
	sport_events = 'Sport Events'
	photography = 'Photography'
	fishing = 'Fishing'
	martial_arts = 'Martial Arts'
	rock_climbing = 'Rock Climbing'
	movies = 'Movies'
	meditation = 'Meditation'
	alternative_health = 'Alternative Health'
	beauty_and_cosmetics = 'Beauty and Cosmetics'
	spa = 'Spa'
	massage = 'Massage'
	art_gallery = 'Art Gallery'
	performance_art = 'Performance Art'
	museum = 'Museum'
	temple = 'Temple'
	hiking = 'Hiking'
	nature = 'Nature'
	campground = 'Campground'
	park = 'Park'
	design = 'Design'
	technology = 'Technology'
	bar = 'Bar'
	whisky = 'Whisky'
	wine = 'Wine'
	zoo = 'Zoo'
	animals = 'Animals'
	amusement_park = 'Amusement Park'
	cafe = 'Cafe'
	restaurant = 'Restaurant'
	cooking = 'Cooking'

class BundlePackageTags(Enum):
	FlightAndHotel = 'Flight+Hotel'
	FlightAndCar = 'Flight+Car'
	FlightAndHotelAndCar = 'Flight+Hotel+Car'
	HotelAndCar = 'Hotel+Car'
	FlightOnly = 'Flight Only'
	Cruise = 'Cruise'
	CarRental = 'Car Rental'

class FlightAncillariesTags(Enum):
	Meal = 'Meal'
	Seat = 'Seat'
	Luggage = 'Luggage'
	MultipleLuggage = 'Multiple Luggage'
	Lounge = 'Lounge'

class GeoZoneTags(Enum):
	NorthAmerica = 'North-America'
	CentralAmerica = 'Central-America'
	SouthAmerica = 'South-America'
	WesternEurope = 'Western-Europe'
	EasternEurope = 'Eastern-Europe'
	SouthernEurope = 'Southern-Europe'
	NorthernEurope = 'Northern-Europe'
	MiddleEast = 'Middle-East'
	Africa = 'Africa'
	Asia = 'Asia'
	EasternAsia = 'Eastern-Asia'
	SouthEastAsia = 'South-East-Asia'
	Oceania = 'Oceania'
	Caribbean = 'Caribbean'
	Antarctica = 'Antarctica'
	Israel = 'Israel'

class GeoZoneSignatureTags(Enum):
	NorthAmerica = 'North-America'
	CentralAmerica = 'Central-America'
	SouthAmerica = 'South-America'
	WesternEurope = 'Western-Europe'
	EasternEurope = 'Eastern-Europe'
	SouthernEurope = 'Southern-Europe'
	NorthernEurope = 'Northern-Europe'
	MiddleEast = 'Middle-East'
	Africa = 'Africa'
	Asia = 'Asia'
	EasternAsia = 'Eastern-Asia'
	SouthEastAsia = 'South-East-Asia'
	Oceania = 'Oceania'
	Caribbean = 'Caribbean'
	Antarctica = 'Antarctica'
	Israel = 'Israel'

class HotelStyleTags(Enum):
	BasicHotelBrand = 'Basic Hotel Brand'
	ThreeStarsHotel = '3 Stars Hotel'
	FourStarsHotel = '4 Stars Hotel'
	FiveStarsHotel = '5 Stars Hotel'
	BedAndBreakfast = 'B & B'
	AirportHotel = 'Airport Hotel'
	BoutiqueHotel = 'Boutique Hotel'
	SpaHotel = 'Spa Hotel'
	LuxuryHotelBrand = 'Luxury Hotel Brand'
	PremiumHotelBrand = 'Premium Hotel Brand'
	AptHotel = 'Apartments Hotel'
	CityCenterHotel = 'City Center Hotel'
	Hostel = 'Hostel'
	Resort = 'Resort'
	LowCostLodging = 'Low Cost Lodging'
	CampgroundLodging = 'Campground Lodging'
	Airbnb = 'Airbnb'

class SiteTypesTags(Enum):
	FoodMarket= 'Food Market'
	Monuments = 'Monuments'
	Castles = 'Castles'
	FleaMarket= 'Flea Market'
	ClothingStore= 'Clothing Store'
	JewelryStore= 'Jewelry Store'
	ShoppingMall= 'Shopping Mall'

class ReservationTags(Enum):
	CallCenter = 'Call Center'
	DirectWeb = 'Direct Web'
	GenericDirect = 'Generic Direct'
	OTA = 'OTA'
	Coupons = 'Coupons'
	Indirect = 'Indirect'
	LastMinutePlanner = 'Last Minute Planner'
	ShortTermPlanner = 'Short Term Planner'
	LongTermPlanner = 'Long Term Planner'
	ExtraLongTermPlanner = 'Extra Long Term Planner'

class TravellerGroupTags(Enum):
	GroupTrip = 'Group Trip'
	OrganizedTour = 'Organized Tour'
	SingleTraveller = 'Single Traveller'
	CoupleTraveller = 'Couple Traveller'
	BoysTrip = 'Boys Trip'
	GirlsTrip = 'Grils Trip'
	FamilyTraveller = 'Family Traveller'
	Children1 = '1 Child'
	Children2 = '2 Children'
	Children3 = '3 Children'
	Children4 = '4 Children'
	Children5 = '5 Children'
	ChildrenM = 'Multiple Children'
	Infants1 = '1 Infant'
	Infants2 = '2 Infants'
	InfantsM = 'Multiple Infants'

class ExperienceTags(Enum):
	Spa = 'Spa'
	SummerSchool = 'Summer School'
	Kosher = 'Kosher'
	Kids = 'Kids'
	Vegan_Vegetarian = 'Vegan or Vegetarian'

class GenericKeys(Enum):
	AgeRange = 'Age Range'
	Gender = 'Gender'
	Languages = 'Languages'
	Profession = 'Profession'
	Frequency = 'Frequency'
	Budget = 'Budget'
	HotLead = 'Hot Lead'
	HomeCountry = "Home Country"

class GenericValues(Enum):
	AgeRange2 = '< 2'
	AgeRange12 = '2-12'
	AgeRange18 = '12-18'
	AgeRange45 = '18-45'
	AgeRange67 = '45-67'
	AgeRange120 = '> 67'
	GenderMale = 'Male'
	GenderFemale = 'Female'
	LanguageHebrew = 'Hebrew'
	LanguageEnglish = 'English'
	LanguageRussian = 'Russian'
	ProfessionHighTech = 'High-Tech'
	BudgetHigh = 'High Budget'
	BudgetMedium = 'Medium Budget'
	BudgetLow = 'Low Budget'
	FrequencyHigh = 'High Frequency'
	FrequencyMedium = 'Medium Frequency'
	FrequencyLow = 'Low Frequency'

class ThemeTravelerTags(Enum):
	SkiVacation = 'Ski Vacation'
	MusicFestival = 'Music Festival'
	Weekend = 'Weekend'
	LongTrip = 'Long Trips'
	Summer = 'Summer'
	Winter = 'Winter'
	NoShabbatFlights = "No Shabbat Flights"
	ShabbatFlights = "Shabbat Flights"

class DestinationTypeTags(Enum):
	casino = "Casino"
	family = "Family"
	ISRSpecial = "Premium"
	couples = "Couples"
	nature = "Nature"
	city_break = "City Break"

class AirportCityTag(Enum):
	TLV = 'tel aviv'
	OTP = 'bucharest'
	ATH = 'athens'
	LCA = 'cyprus'
	GYD = 'baku'
	BUS = 'batumi'
	BEG = 'belgrad'
	LJU = 'ljubljana'
	BOJ = 'bourgas'
	VRN = 'verona'
	MAD = 'madrid'
	TGD = 'montenegro'
	TIV = 'montenegro'
	PFO = 'cyprus'
	PRG = 'prague'
	RHO = 'rhodes'
	SKG = 'thessaloniki'
	AER = 'sochi'
	TBS = 'tbilisi'
	TFS = 'tenerife'
	VAR = 'varna'
	TIA = 'tirana'
	SOF = 'sofia'
	FCO = 'rome'
	SXF = 'berlin'
	LGW = 'london'
	STN = 'london'
	LTN = 'london'
	RVN = 'lapland'
	MUC = 'munich'
	XXX = 'no city'

airport_keys = [airport.name for airport in AirportCityTag]

class TripDurationTags(Enum):
	short = "Short Trip"
	moderate = "Moderate Trip"
	long = "Long Trip"

class JourneyDurationTags(Enum):
	short = "Short Journey"
	moderate = "Moderate Journey"
	long = "Long Journey"

class TourGuideTags(Enum):
	TourGuide = "Tour Guide"

class RecurringTravelerTags(Enum):
	RecurrenceCounter = "Recurrence Counter"

TAG_CATEGORIES = [DestinationTags, JourneyTags, InterestTags, BundlePackageTags, FlightAncillariesTags, GeoZoneTags,
				  GeoZoneSignatureTags, HotelStyleTags, ThemeTravelerTags, ExperienceTags, TravellerGroupTags,
				  ReservationTags, RecurringTravelerTags, AirportCityTag, DestinationTypeTags, TripDurationTags,
				  JourneyDurationTags, TourGuideTags, SiteTypesTags]