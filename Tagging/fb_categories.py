from Tagging.tags import InterestTags, ExperienceTags, FlightAncillariesTags, HotelStyleTags, SiteTypesTags

FB_ACCOMMODATION_CATEGORIES = [category.lower() for category in
							   ['Hotel & Lodging', 'Bed and Breakfast', 'Hostel', 'Cottage', 'Hotel',
							   'Beach Resort', 'Hotel Resort', 'Inn', 'Lodge', 'Motel', 'Service Apartments',
							   'Vacation Home Rental']]
FB_AIRPORT_CATEGORIES = [category.lower() for category in
						 ['Airport', 'Airport Gate', 'Airport Lounge', 'Airport Terminal']]

FB_SKI_LOCATION_CATEGORIES = [category.lower() for category in ['Ski & Snowboard School', 'Ski Resort']]

tag_to_categories = {
	InterestTags.sport : [category.lower() for category in ['Sports League', 'Sports Team', 'Sports Club', 'Sports', 'Sports Museum']],
	InterestTags.sport_events : [category.lower() for category in ['Stadium', 'Arena & Sports Venue', 'Sports Event']],
	InterestTags.football : [category.lower() for category in ['Football Stadium', 'Soccer Stadium']],
	InterestTags.basketball : [category.lower() for category in ['Basketball Stadium']],
	InterestTags.tennis : [category.lower() for category in ['Tennis Stadium']],
	InterestTags.boxing : [category.lower() for category in ['Boxing Studio']],
	InterestTags.golf : [category.lower() for category in ['Golf Instructor', 'Miniature Golf Course']],
	InterestTags.swimming : [category.lower() for category in ['Swimwear Store', 'Public Swimming Pool']],
	InterestTags.scuba_diving : [category.lower() for category in ['Scuba Diving Center', 'Diving Spot', 'Scuba Instructor', 'Snorkeling Spot']],
	InterestTags.ski_and_snowboard : [category.lower() for category in ['Ski & Snowboard Shop', 'Ski & Snowboard School', 'Ski Resort']],
	InterestTags.cycling : [category.lower() for category in ['Mountain Biking Shop', 'Bicycle Shop', 'Cycling Studio', 'Bike Trail']],
	InterestTags.surfing : [category.lower() for category in ['Surf Shop', 'Surfing Spot']],
	InterestTags.fishing : [category.lower() for category in ['Fishing Store', 'Fishing Spot']],
	InterestTags.martial_arts : [category.lower() for category in ['Martial Arts School', 'Tai Chi Studio']],
	InterestTags.rock_climbing : [category.lower() for category in ['Rock Climbing Gym', 'Rock Climbing Spot']],
	InterestTags.music : [category.lower() for category in ['Musician/Band', 'Musician', 'Band']],
	InterestTags.electronic_music : [category.lower() for category in ['DJ']],
	InterestTags.jazz : [category.lower() for category in ['Jazz & Blues Club']],
	InterestTags.opera : [category.lower() for category in ['Opera House']],
	InterestTags.live_shows : [category.lower() for category in ['Live Music Venue', 'Amphitheater', 'Comedy Club', 'Circus']],
	InterestTags.dancing : [category.lower() for category in ['Salsa Club', 'Dance School', 'Dance Studio', 'Dancer']],
	InterestTags.movies: [category.lower() for category in ['Movie Theater', 'Drive-In Movie Theater', 'Movie Genre', 'Actor']],
	InterestTags.gambling : [category.lower() for category in ['Casino']],
	InterestTags.yoga : [category.lower() for category in ['Yoga Studio']],
	InterestTags.fitness : [category.lower() for category in ['Sportswear Store', 'Fitness Boot Camp', 'Gym/Physical Fitness Center', 'Pilates Studio', 'Fitness Trainer', 'Gymnastics Center', 'Sports & Fitness Instruction', 'Fitness Venue', 'Fitness Model']],
	InterestTags.art : [category.lower() for category in ['Art', 'Artist', 'Art School', 'Art Tour Agency', 'Arts & Humanities Website']],
	InterestTags.trekking : [category.lower() for category in ['Outdoor Equipment Store', 'Glacier', 'Volcano', 'Fjord/Loch', 'Mountain']],
	InterestTags.photography : [category.lower() for category in ['Photography Museum', 'Photography Videography', 'Photographer', 'Camera Store', 'Camera/Photo']],
	InterestTags.night_club : [category.lower() for category in ['Dance & Night Club', 'Adult Entertainment Club']],
	InterestTags.meditation : [category.lower() for category in ['Meditation Center']],
	InterestTags.alternative_health : [category.lower() for category in ['Alternative & Holistic Health Service', 'Naturopath', 'Nutritionist', 'Health & Wellness Website']],
	InterestTags.beauty_and_cosmetics : [category.lower() for category in ['Beauty Salon', 'Hair Salon', 'Nail Salon', 'Makeup Artist', 'Beauty Supplier', 'Health/Beauty']],
	InterestTags.spa : [category.lower() for category in ['Spa', 'Day Spa', 'Health Spa']],
	InterestTags.massage : [category.lower() for category in ['Massage School', 'Massage Therapist', 'Massage Service']],
	InterestTags.art_gallery : [category.lower() for category in ['Art Gallery', 'Art Museum', 'Asian Art Museum', 'Contemporary Art Museum', 'Modern Art Museum', ]],
	InterestTags.performance_art : [category.lower() for category in ['Performance Art', 'Performing Arts School', 'Performing Arts']],
	InterestTags.temple : [category.lower() for category in ['Hindu Temple', 'Sikh Temple', 'Synagogue', 'Mosque', 'Buddhist Temple']],
	InterestTags.hiking : [category.lower() for category in ['Hiking Trail', 'Waterfall', 'River', 'Lake']],
	InterestTags.nature : [category.lower() for category in ['National Forest', 'National Park', 'Nature Preserve']],
	InterestTags.campground : [category.lower() for category in ['Campground']],
	InterestTags.park : [category.lower() for category in ['Park', 'Picnic Ground', 'Public Garden']],
	InterestTags.design : [category.lower() for category in ['Design Museum', 'Interior Design Studio', 'Designer']],
	InterestTags.technology : [category.lower() for category in ['Entrepreneur', 'Science', 'Technology & Engineering']],
	InterestTags.bar : [category.lower() for category in ['Beer Bar', 'Beer Garden', 'Champagne Bar', 'Cocktail Bar', 'Dive Bar', 'Gay Bar', 'Hookah Lounge', 'Hotel Bar', 'Irish Pub', 'Pub']],
	InterestTags.whisky : [category.lower() for category in ['Whisky Bar']],
	InterestTags.wine : [category.lower() for category in ['Wine Bar', 'Winery/Vineyard', 'Wine/Spirits', 'Wine', 'Beer & Spirits Store']],
	InterestTags.zoo : [category.lower() for category in ['Zoo']],
	InterestTags.animals : [category.lower() for category in ['Animal Shelter', 'Animal Rescue Service', 'Pet Service', 'Veterinarian', 'Pet Adoption Service']],
	InterestTags.amusement_park : [category.lower() for category in ['Amusement & Theme Park', 'Water Park']],
	InterestTags.cafe : [category.lower() for category in ['Cafe', 'Coffee Shop']],
	InterestTags.cooking: [category.lower() for category in ['Chef', 'Cooking School', 'Kitchen/Cooking', 'Food Website', 'Personal Chef']],

	ExperienceTags.Kids : [category.lower() for category in ['Baby Goods/Kids Goods', 'Kids Entertainment Service', "Baby & Children's Clothing Store", 'Babysitter']],
	ExperienceTags.Kosher : [category.lower() for category in ['Kosher Restaurant']],
	ExperienceTags.Vegan_Vegetarian : [category.lower() for category in ['Vegetarian/Vegan Restaurant', 'Health Food Store', 'Fruit & Vegetable Store']],

	FlightAncillariesTags.Lounge : [category.lower() for category in ['Airport Lounge']],

	HotelStyleTags.LowCostLodging : [category.lower() for category in ['Hostel', 'Inn', 'Lodge', 'Motel']],
	HotelStyleTags.BedAndBreakfast : [category.lower() for category in ['Bed and Breakfast']],
	HotelStyleTags.CampgroundLodging : [category.lower() for category in ['Cabin', 'Campground']],
	HotelStyleTags.Airbnb : [category.lower() for category in ['Vacation Home Rental', 'Service Apartments']],
	HotelStyleTags.Resort : [category.lower() for category in ['Beach Resort', 'Hotel Resort']],

	SiteTypesTags.FoodMarket: [category.lower() for category in ['Farmers Market', 'Fish Market']],
	SiteTypesTags.Monuments: [category.lower() for category in ['Landmark & Historical Place', 'Monument', 'Statue & Fountain']],
	SiteTypesTags.Castles: [category.lower() for category in ['Castle', 'Palace']],
	SiteTypesTags.FleaMarket: [category.lower() for category in ['Flea Market']],
}

# The inverted dictionary - for every category, match all the relevant tags
category_to_tags = {}
for tag, categories in tag_to_categories.items():
	for category in categories:
		category_to_tags.setdefault(category, []).append(tag)