from Tagging.tags import DestinationTags
from Tagging.data_origin import get_dict_with_origin
from Utils.tagging_helpers import get_all_tags, merge_all_tags, increment_tag


class Destination(object):
	def __init__(self):
		self._route = []
		self._accommodations = []
		self._tickets = []
		self._tags = get_all_tags()
		self._start = None
		self._end = None
		self._name = None
		self._src_city = None
		self._is_object_new = True
		self._origins = {}

	def _update_tags(self):
		# TAG: direct flight
		if 1 == len(self._route):
			increment_tag(self._tags, DestinationTags.direct, self._origins)
			
		# TAG: single connection
		elif 2 == len(self._route):
			increment_tag(self._tags, DestinationTags.single_connection, self._origins)

		# TAG: multiple connections
		elif 2 < len(self._route):
			increment_tag(self._tags, DestinationTags.multi_connection, self._origins)
		
		return self._tags

	def get_name(self):
		return self._name

	def get_route(self):
		return self._route

	def get_accommodations(self):
		return self._accommodations

	def get_tickets(self):
		return self._tickets

	def get_tags(self):
		self._update_tags()
		return self._tags

	def get_data_origin(self):
		return self._origins

	def get_last_flight(self):
		if 0 == len(self._route):
			return None
		return self._route[-1]

	def get_last_accommodation(self):
		if 0 == len(self._accommodations):
			return None
		return self._accommodations[-1]

	def get_start(self):
		return self._start

	def get_end(self):
		return self._end

	def get_src_city(self):
		return self._src_city

	def _is_new(self):
		return self._is_object_new

	def add_connection(self, flight):
		if 0 == len(self._route):
			self._src_city = flight.get_src_city()
			self._start = flight.get_start()

		self._route.append(flight)
		self._end = flight.get_end()
		self._name = flight.get_dst_city()
		self._origins = {**self._origins, **flight.get_data_origin()}

		# update with flight existing tags, unless it was already tagged in the past
		if flight._is_new():
			merge_all_tags(self._tags, flight.get_tags())
		else:
			self._is_object_new = False

	def add_accommodation(self, accommodation):
		self._accommodations.append(accommodation)
		self._origins = {**self._origins, **accommodation.get_data_origin()}
		self._end = accommodation.get_end()

		# "Unreachable" accommodation
		if 0 == len(self._route):
			self._name = accommodation.get_location()
			self._start = accommodation.get_start()

		# update with accommodation existing tags, unless it was already tagged in the past
		if accommodation._is_new():
			merge_all_tags(self._tags, accommodation.get_tags())
		else:
			self._is_object_new = False

	def add_ticket(self, ticket):
		self._tickets.append(ticket)
		self._origins = {**self._origins, **ticket.get_data_origin()}
		self._end = ticket.get_end()

		# "Unreachable" ticket
		if 0 == len(self._route) and 0 == len(self._accommodations):
			self._name = ticket.get_location()
			self._start = ticket.get_start()

		# update with accommodation existing tags, unless it was already tagged in the past
		if ticket._is_new():
			merge_all_tags(self._tags, ticket.get_tags())
		else:
			self._is_object_new = False

	def __str__(self):
		return 'Name:{name}:\nStart:{start},\nRoutes:{routes},\nAccommodations:{accommodations},\
			\nTickets:{tickets},\nOrigins:{origins}\n--> Tags:{tags}\n'.format(
			name=self._name,
			start=self._start,
			routes=str([str(x) for x in self._route]),
			accommodations=str([str(x) for x in self._accommodations]),
			tickets=str([str(x) for x in self._tickets]),
			origins=self._origins,
			tags=self._tags)

	def __repr__(self):
		return str(self)

		