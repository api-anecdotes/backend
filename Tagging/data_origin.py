from enum import Enum

class OriginCompany(Enum):
	NO_COMPANY = 0
	UNKNOWN_COMPANY = 1
	ISRAIR = '63c7c93c-0ffb-4d90-83ef-3ba6d4340e45'
	ANECDOTES = 4

class OriginType(Enum):
	NO_TYPE= 0
	UNKNOWN_TYPE= 1
	GOOGLE = 2
	FACEBOOK = 3
	ON_PREM = 4

# Update dictionary of origins: for every origin in old and new primitives, combine the origin
# types (Google/Facebook/etc.) for the same origin company (Israir/EL-AL/etc.)
# takes two items with an _origin member, and returns a combined origin of the two (with no duplication of fields)
def get_combined_origins(existing_item, new_item):
	combined_origin = {}
	for origin in (existing_item._origin, new_item._origin):
		for origin_company, origin_type_list in origin.items():
			combined_origin_type_list = combined_origin.setdefault(origin_company, [])
			for origin_type in origin_type_list:
				if origin_type not in combined_origin_type_list:
					combined_origin_type_list.append(origin_type)
	return combined_origin

# convert dictionary so every key points to a dictionary, which holds both data and data_origin
def get_dict_with_origin(dictionary, data_origin):
	dict_with_origin = {}
	for (key, value) in dictionary.items():
		# for every key generate a dict that represents the key, value and their data_origin
		dict_with_origin[key] = {'data': value, 'data_origin': data_origin}

	return dict_with_origin
