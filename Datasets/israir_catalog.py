from Tagging.tags import *

ISRAIR_PRODUCTS_CATALOG = {
1: {
	'name': 'Baku, Flight',
	'img': 'https://israir.co.il/clients/isra2016/gallery//BAKU/1917X720/Baku_234311770%201917_720.jpg',
	'url': 'https://israir.co.il/Flights/To/Baku.html',
	'tags': {
	  'city_tag': AirportCityTag('baku'),
	  'generic': {
		  GenericKeys.Budget: GenericValues.BudgetMedium,
	  },
	  'tags': [GeoZoneTags.EasternEurope, DestinationTypeTags.ISRSpecial , InterestTags.culture]
	},
},
2: {
	'name': 'Baku, Package',
	'img': 'https://israir.co.il/clients/isra2016/gallery//BAKU/1917X720/Baku_387797692%201917X720.jpg',
	'url': 'https://israir.co.il/packages/to/baku.html',
	'tags': {
	  'city_tag': AirportCityTag('baku'),
	  'generic': {
		  GenericKeys.Budget: GenericValues.BudgetMedium,
	  },
	  'tags': [GeoZoneTags.EasternEurope, BundlePackageTags.
			   FlightAndHotel , DestinationTypeTags.ISRSpecial , InterestTags.culture]
	},
},
3: {
	'name': 'Batumi, Flight',
	'img': 'https://silkroadexplore.com/wp-content/uploads/2016/01/Batumi-2012-105-800x600.jpg',
	'url': 'https://israir.co.il/flights/to/batumi.html',
	'tags': {
		'city_tag': AirportCityTag('bucharest'),
		'generic': {
		  GenericKeys.Budget : GenericValues.BudgetMedium,
		},
		'tags': [GeoZoneTags.EasternEurope,
				 InterestTags.beach , DestinationTypeTags.family , DestinationTypeTags.casino],
	},
},
4: {
	'name': 'Bucharest, Flight',
	'img': 'https://israir.co.il/clients/isra2016/gallery//BucharestV/1917X720/Bucharest_505413358%201917_720.jpg',
	'url': 'https://israir.co.il/flights/to/Bucharest.html',
	'tags': {
		'city_tag': AirportCityTag('bucharest'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetLow,
		},
		'tags': [GeoZoneTags.EasternEurope , DestinationTypeTags.casino , InterestTags.shopping],
	},
},
5: {
	'name': 'Bucharest, Package',
	'img': 'https://israir.co.il/clients/isra2016/gallery//Batumi/1917X720/Batumi_577111072%201917_720.jpg',
	'url': 'https://israir.co.il/Packages/To/Bucharest.html',
	'tags': {
		'city_tag': AirportCityTag('bucharest'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetLow,
		},
		'tags': [GeoZoneTags.EasternEurope, BundlePackageTags.FlightAndHotel , DestinationTypeTags.casino , InterestTags.shopping],
	},
},
6: {
	'name': 'Sochi, Flight',
	'img': 'https://www.skideal.co.il/sites/skideal/UserContent/images/Resorts/sochi/6.jpg?w=600',
	'url': 'https://israir.co.il/flights/to/sochi.html',
	'tags': {
		'city_tag': AirportCityTag('sochi'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetMedium,
		},
		'tags': [InterestTags.ski_and_snowboard, DestinationTypeTags.ISRSpecial, DestinationTypeTags.casino],
	},
},
7: {
	'name': 'Sochi, Package',
	'img': 'https://www.skideal.co.il/sites/skideal/UserContent/images/Resorts/sochi/sochi%20(38)(1).jpg?w=500',
	'url': 'https://www.skideal.co.il/russia',
	'tags': {
		'city_tag': AirportCityTag('sochi'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetMedium,
		},
		'tags': [InterestTags.ski_and_snowboard, BundlePackageTags.FlightAndHotel , DestinationTypeTags.ISRSpecial, DestinationTypeTags.casino],
	},
},
8: {
	'name': 'Rhodes, Flight',
	'img': 'https://www.mypremiumeurope.com/img/image_db/rhodes_beaches_lindos_beach_umbrellas-942.jpg',
	'url': 'https://israir.co.il/flights/to/rhodes.html',
	'tags': {
		'city_tag': AirportCityTag('rhodes'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetLow,
		},
		'tags': [InterestTags.beach , DestinationTypeTags.family, DestinationTypeTags.couples , ThemeTravelerTags.Summer],
	},
},
9: {
	'name': 'Rhodes, Package',
	'img': 'https://israir.co.il/clients/isra2016/gallery//Rhodes/1917X720/Rhodes_277638290%201917_720.jpg',
	'url': 'https://israir.co.il/packages/to/rhodes.html',
	'tags': {
		'city_tag': AirportCityTag('rhodes'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetLow,
		},
		'tags': [BundlePackageTags.FlightAndHotel, InterestTags.beach , DestinationTypeTags.family, DestinationTypeTags.couples , ThemeTravelerTags.Summer],
	},
},
10: {
	'name': 'Verona, Flight',
	'img': 'https://israir.co.il/clients/isra2016/gallery//VeronaV/1917X720/Verona_516926059%201917_720.jpg',
	'url': 'https://israir.co.il/flights/to/verona.html',
	'tags': {
		'city_tag' : AirportCityTag('verona'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetHigh,
		},
		'tags': [GeoZoneTags.WesternEurope],
	},
},
11: {
	'name': 'Munich, Flight',
	'img': 'https://israir.co.il/clients/isra2016/gallery//MunichV/1917X720/Munich_242486191%201917_720.jpg',
	'url': 'https://israir.co.il/flights/to/Munchen.html',
	'tags': {
		'city_tag': AirportCityTag('munich'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetHigh,
		},
		'tags': [GeoZoneTags.WesternEurope],
	},
},
12: {
	'name': 'Paphos, Flight',
	'img': 'https://israir.co.il/clients/isra2016/gallery//Paphos/1917X720/shutterstock_491994109%20%282%29.jpg',
	'url': 'https://israir.co.il/flights/to/Paphos.html',
	'tags': {
		'city_tag': AirportCityTag('cyprus'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetLow,
		},
		'tags': [InterestTags.beach , DestinationTypeTags.family , ThemeTravelerTags.Summer],
	},
},
13: {
	'name': 'Tenerife, Flight',
	'img': 'https://israir.co.il/clients/isra2016/gallery//Tenerife/1917X720/Tenerife_115588891.jpg',
	'url': 'https://israir.co.il/Flights/to/Tenerife.html',
	'tags': {
		'city_tag': AirportCityTag('tenerife'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetMedium,
		},
		'tags': [GeoZoneTags.WesternEurope, InterestTags.beach, DestinationTypeTags.family, DestinationTypeTags.nature],
	},
},
# 14: {
# 	'name': 'Sella Ronda, Package',
# 	'img': 'https://www.skideal.co.il/sites/skideal/UserContent/images/Resorts/Sella%20Ronda/Images/%D7%95%D7%95%D7%9C%D7%91%D7%95%20%D7%90%D7%93%D7%95%D7%9E%D7%94%20%D7%91%D7%A4%D7%90%D7%A8%D7%A7.jpg?w=600',
# 	'url': 'https://www.skideal.co.il/sella-ronda',
# 	'tags': {
# 		'city_tag': AirportCityTag('no city'),
# 		'generic': {
# 			GenericKeys.Budget: GenericValues.BudgetHigh,
# 		},
# 		'tags': [InterestTags.ski],
# 	},
# },
15: {
	'name': 'prague, Package',
	'img':
		'https://israir.co.il/clients/isra2016/gallery//PragueV/1917X720/Prague_276911168%201917_720.jpg',
	'url': ' https://www.israir.co.il/Packages/To/Prague.html',
	'tags': {
		'city_tag': AirportCityTag('prague'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetMedium,
		},
		'tags': [InterestTags.shopping, InterestTags.night_club, DestinationTypeTags.casino, DestinationTypeTags.city_break, BundlePackageTags.FlightAndHotel],
	},
},
16: {
	'name': 'varna, Package',
	'img':
		'https://israir.co.il/clients/isra2016/gallery//Varna/1917X720/Varna_591423533%201917_720.jpg',
	'url': ' https://www.israir.co.il/Packages/To/varna.html',
	'tags': {
		'city_tag': AirportCityTag('varna'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetMedium,
		},
		'tags': [InterestTags.beach, InterestTags.night_club, DestinationTypeTags.casino, DestinationTypeTags.family, BundlePackageTags.FlightAndHotel],
	},
},
17: {
	'name': 'burgas, Package',
	'img': 'https://israir.co.il/clients/isra2016/gallery//Burgas/1917X720/Burgas_395762284%201917_720.jpg',
	'url': 'https://www.israir.co.il/Packages/To/burgas.html',
	'tags': {
		'city_tag': AirportCityTag('bourgas'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetLow,
		},
		'tags': [InterestTags.beach, InterestTags.night_club, DestinationTypeTags.casino, DestinationTypeTags.family, BundlePackageTags.FlightAndHotel],
	},
},
18: {
	'name': 'athens, Package',
	'img': 'https://israir.co.il/clients/isra2016/gallery//AthensV/1917X720/Athens_141846352%201917_720.jpg',
	'url': 'https://www.israir.co.il/Packages/To/athens.html',
	'tags': {
		'city_tag': AirportCityTag('athens'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetLow,
		},
		'tags': [InterestTags.beach, InterestTags.night_club, InterestTags.culture, DestinationTypeTags.city_break, BundlePackageTags.FlightAndHotel],
	},
},
19: {
	'name': 'larnaca, Package',
	'img': 'https://israir.co.il/clients/isra2016/gallery//LarnacaV/1917X720/Larnaca_369500594%201917_720.jpg ',
	'url': 'https://www.israir.co.il/Packages/To/larnaca.html',
	'tags': {
		'city_tag': AirportCityTag('cyprus'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetLow,
		},
		'tags': [InterestTags.beach, InterestTags.night_club, ThemeTravelerTags.Summer, DestinationTypeTags.family, BundlePackageTags.FlightAndHotel],
	},
},
20: {
	'name': 'saloniki, Package',
	'img': 'https://israir.co.il/clients/isra2016/gallery//ThessalonikiV/1917X720/Thessaloniki_67327858%201917_720.jpg',
	'url':'https://www.israir.co.il/Packages/To/Thessaloniki.html',
	'tags': {
		'city_tag': AirportCityTag('thessaloniki'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetLow,
		},
		'tags': [InterestTags.beach, ThemeTravelerTags.Summer, DestinationTypeTags.city_break, BundlePackageTags.FlightAndHotel],
	},
},
21: {
	'name': 'Albania, Flight',
	'img': 'https://israir.co.il/clients/isra2016/gallery//Tirana/1917X720/shutterstock_331847861%20%282%29.jpg',
	'url': 'https://www.israir.co.il/Flights/To/Albania.html',
	'tags': {
		'city_tag': AirportCityTag('tirana'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetLow,
		},
		'tags': [InterestTags.shopping, InterestTags.beach, DestinationTypeTags.couples, DestinationTypeTags.nature],
	},
},
22: {
	'name': 'Montenegro, Package',
	'img': 'https://israir.co.il/clients/isra2016/gallery//more/shutterstock_80279815.jpg ',
	'url':'https://www.israir.co.il/Packages/To/Montenegro.html',
	'tags': {
		'city_tag': AirportCityTag('montenegro'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetMedium,
		},
		'tags': [InterestTags.beach, InterestTags.shopping , DestinationTypeTags.family, DestinationTypeTags.nature , BundlePackageTags.FlightAndHotel],
	},
},
23: {
	'name': 'belgrad, Package',
	'img': 'https://israir.co.il/clients/isra2016/gallery//Belgrade/1917X720/Belgrade_418638907%201917_720.jpg',
	'url':'https://www.israir.co.il/Packages/To/belgrad.html',
	'tags': {
		'city_tag': AirportCityTag('belgrad'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetMedium,
		},
		'tags': [InterestTags.beach, InterestTags.shopping, InterestTags.culture, DestinationTypeTags.city_break, DestinationTypeTags.nature, BundlePackageTags.FlightAndHotel],
	},
},
24: {
	'name': 'ljubljana, Flight',
	'img': 'https://israir.co.il/clients/isra2016/gallery//more/shutterstock_80279815.jpg ',
	'url':'https://www.israir.co.il/Flights/To/Ljubljana.html',
	'tags': {
		'city_tag': AirportCityTag('ljubljana'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetMedium,
		},
		'tags': [InterestTags.beach, InterestTags.shopping, DestinationTypeTags.family, DestinationTypeTags.nature],
	},
},
25: {
	'name': 'madrid, Package',
	'img': 'https://israir.co.il/clients/isra2016/gallery//more/shutterstock_80279815.jpg ',
	'url':'https://www.israir.co.il/Packages/To/Madrid.html',
	'tags': {
		'city_tag': AirportCityTag('madrid'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetMedium,
		},
		'tags': [InterestTags.sport, InterestTags.shopping, DestinationTypeTags.family, DestinationTypeTags.couples, DestinationTypeTags.city_break, BundlePackageTags.FlightAndHotel],
	},
},
26: {
	'name': 'madrid, Flight',
	'img': 'https://israir.co.il/clients/isra2016/gallery//more/shutterstock_80279815.jpg ',
	'url':'https://www.israir.co.il/Flights/To/Madrid.html',
	'tags': {
		'city_tag': AirportCityTag('madrid'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetMedium,
		},
		'tags': [InterestTags.sport, InterestTags.shopping, DestinationTypeTags.family, DestinationTypeTags.couples, DestinationTypeTags.city_break],
	},
},
27: {
	'name': 'lapland, Flight',
	'img': 'https://israir.co.il/clients/isra2016/gallery//Lapland/SNOW%20FINLAND_66229543.jpg',
	'url':'https://www.israir.co.il/Flights/To/Lapland.html',
	'tags': {
		'city_tag': AirportCityTag('lapland'),
		'generic': {
			GenericKeys.Budget: GenericValues.BudgetHigh,
		},
		'tags': [DestinationTypeTags.nature, DestinationTypeTags.ISRSpecial],
	},
},
}

ISRAIR_CATALOG_TAGGING = {}
relevant_categories = [DestinationTypeTags, InterestTags]
for item in ISRAIR_PRODUCTS_CATALOG.values():
	city_tag = item['tags']['city_tag']
	if city_tag and city_tag.name in ISRAIR_CATALOG_TAGGING:
		continue
	else:
		ISRAIR_CATALOG_TAGGING[city_tag.name] = [tag for tag in item['tags']['tags'] if tag.__class__ in relevant_categories]