import base64
import os
import sys
import logging
import pandas

from flask import Flask, request
from OnPrem.parser_israir import IsrairParser
from OnPrem.consts import IsrairParserConsts

logging.basicConfig(level=10)
app = Flask(__name__)

# USE THESE LINES TO CREATE: STORAGE STIGGER -> TOPIC MESSAGE -> CLOUD RUN INVOKATION
## gcloud projects add-iam-policy-binding revv-poc-1574867026155 --member=serviceAccount:service-1063241535274@gcp-sa-pubsub.iam.gserviceaccount.com --role=roles/iam.serviceAccountTokenCreator
## gcloud iam service-accounts create cloud-run-pubsub-invoker --display-name "Cloud Run Pub/Sub Invoker"
# gcloud run services add-iam-policy-binding on-prem-parser --member=serviceAccount:cloud-run-pubsub-invoker@revv-poc-1574867026155.iam.gserviceaccount.com --role=roles/run.invoker --platform managed
# gcloud pubsub subscriptions create inbox-data --topic on-prem-inbox-data --push-endpoint=https://on-prem-parser-nipvgsvkfa-ez.a.run.app/ --push-auth-service-account=cloud-run-pubsub-invoker@revv-poc-1574867026155.iam.gserviceaccount.com

# LOCALY TEST USING:
# data = pandas.read_csv(r"C:\Users\yairk\Desktop\Revving\Israir\Data\In\ISRAIR_BI_17.csv", delimiter=',', skip_blank_lines=True, header=0, encoding="ISO-8859-1", skipinitialspace=True, error_bad_lines=False)
# sub = data.iloc[:10]
# fdata = base64.b64encode(json.dumps(sub.to_dict()).encode('utf8'))
# requests.post('http://localhost:8083', json={'message': {'data': fdata, 'attributes': {'origin': '63c7c93c-0ffb-4d90-83ef-3ba6d4340e45', 'data_id': '123'}}})

PARSERS_MAP = {
	IsrairParserConsts.company_id.value: IsrairParser,
}

@app.route('/', methods=['POST'])
def index():
	envelope = request.get_json()
	if not envelope:
		msg = 'no Pub/Sub message received'
		logging.debug(f'error: {msg}')
		return f'Bad Request: {msg}', 400

	if not isinstance(envelope, dict) or 'message' not in envelope:
		msg = 'invalid Pub/Sub message format'
		logging.debug(f'error: {msg}')
		return f'Bad Request: {msg}', 400

	pubsub_message = envelope['message']

	if isinstance(pubsub_message, dict) and 'data' in pubsub_message:
		raw_data = base64.b64decode(pubsub_message['data']).decode('utf8')
		data = pandas.read_json(raw_data)
		attributes = pubsub_message.get('attributes', {})
		origin = attributes.get('origin')
		data_id = attributes.get('data_id')

		logging.debug(f'data len = {len(data)}')
		logging.debug(f'attributes = {attributes}')
		
		parser = PARSERS_MAP.get(origin)
		if parser is None:
			return f'No Matching Parser: {msg}', 400

		parser(data, data_id).parse()

	logging.debug('Done')
	return ('', 204)


if __name__ == "__main__":
	app.run(debug=True,host='0.0.0.0',port=int(os.environ.get('PORT', 8083)))