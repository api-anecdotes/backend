from enum import Enum

STREAM_BATCH_SIZE = 5

class IsrairParserConsts(Enum):
	company_id = '63c7c93c-0ffb-4d90-83ef-3ba6d4340e45'

	max_family_group_adults = 9
	hightech_emails = ['elbitsystems.com', 'amdocs.com', 'iai.co.il', 'intel.com', 'rafael.co.il', 'wix.com']
	russian_mails = ['mail.ru', 'yandex.ru', 'rambler.ru', 'lenta.ru', '.ru']
	max_name_length = 50
	min_year_of_birth = 1925
	max_id_appearance_per_year = 5
	min_price_per_reservation = 50