import logging
import pandas
import json
import uuid
from datetime import datetime
from Utils import cloud as cloud_utils
from Utils.consts import ON_PREM_DATA_TOPIC
from OnPrem.consts import STREAM_BATCH_SIZE

logging.basicConfig(level=10)


class Streamer(object):
	def __init__(self, bucket_id):
		self._bucket_id = bucket_id

	def _mark_processed(self, object_id, metadata, data):
		metadata['processing_timestamp'] = data
		cloud_utils.update_metadata(object_id, self._bucket_id, metadata)

	def handle_raw_data(self, object_id):
		metadata = cloud_utils.get_metadata(object_id, self._bucket_id)
		origin = metadata.get('origin')
		processing_timestamp = metadata.get('processing_timestamp')
		if processing_timestamp != None:
			return
		if origin is None:
			logging.error('Unknown origin')
			return

		self._mark_processed(object_id, metadata, '')
		raw_data_iter = pandas.read_csv(
			f'gs://{self._bucket_id}/{object_id}',
			delimiter=',',
			skip_blank_lines=True,
			header=0,
			encoding="ISO-8859-1", 
			skipinitialspace=True, 
			error_bad_lines=False, 
			chunksize=STREAM_BATCH_SIZE) 
			#converters=self._raw_converters, dtype=self._dtype

		attributes = {'origin': origin}
		for chunk in raw_data_iter:
			logging.debug(f'send chunk')
			json_data = json.dumps(chunk.to_dict())
			attributes['data_id'] = str(uuid.uuid4())
			cloud_utils.send_message(json_data, attributes, ON_PREM_DATA_TOPIC)

		self._mark_processed(object_id, metadata, str(datetime.now().astimezone()))
