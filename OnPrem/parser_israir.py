import copy
import traceback
import pytz
import logging
import pandas
import numpy as np

from datetime import datetime, timedelta

from Datasets.israir_catalog import ISRAIR_CATALOG_TAGGING
from Datasets.airport_data import AIRPORT_DATA
from Traveler.profile import TravelerProfile
from Traveler.identifiers import get_or_create_local_id
from Tagging.manager import TaggingManager
from Tagging.travel_primitives import Flight, Accommodation
from Tagging.data_origin import OriginType, OriginCompany
from Tagging.tags import TAG_CATEGORIES, ReservationTags, ExperienceTags, \
							HotelStyleTags, FlightAncillariesTags, BundlePackageTags, \
							GenericKeys, GenericValues, TravellerGroupTags, AirportCityTag, RecurringTravelerTags, \
							TripDurationTags, TourGuideTags, airport_keys

from Utils.tagging_helpers import build_tags_with_origin, build_generics_with_origin, increment_tag
from Utils.tagging_helpers import get_all_tags
from Utils.cloud import BatchFirestoreWrite
from OnPrem.parser_generic import GenericParser
from OnPrem.consts import IsrairParserConsts as consts
from OnPrem.utils import *


class IsrairParser(GenericParser):
	def __init__(self, data, data_id):
		super().__init__(OriginCompany.ISRAIR.value, OriginType.ON_PREM.name, data, data_id)
		self._bi_data_email = {}
		self._bi_data_phone = {}
		self._identifier_to_profile_num = {}

		self._output_tags_local = pandas.DataFrame()

		self._raw_converters = {
			'Date_Key': self._get_date_obj,
			'Order_Date_Key': self._get_date_obj,
			'Destination_Key': self._filter_dst,
			'DOB': self._get_date_obj,
			'Gender': lambda x: get_value_safe(x, str.lower),
			'Email': lambda x: get_value_safe(x, str.lower),
			'Phone_Num': str,
			'FirstName': str,
			'Surname': str,
			'Hotel_Name': lambda x: get_value_safe(x, str.capitalize, ''),
			'Hotel_City': lambda x: get_value_safe(x, str.capitalize, ''),
			'Hotel_Country': lambda x: get_value_safe(x, str.capitalize, ''),
			'Hotel_Stars': self._get_int_obj,
			'Male_Cnt': self._get_int_obj,
			'Female_Cnt': self._get_int_obj,
			'Chd_Cnt': self._get_int_obj,
			'Inf_Cnt': self._get_int_obj,
			'Days_Count': self._get_int_obj,
			'Meal_Cnt': self._get_int_obj,
			'Seat_Cnt': self._get_int_obj,
			'LuggageQty': self._get_int_obj,
			'Reserv_Income_Gross': self._get_float_obj,
		}

		self._dtype = {}

	def _get_date_obj(self, date_str):
		if date_str is None or (type(date_str) is float and np.isnan(date_str)):
			return None

		if float is type(date_str):
			date_str = str(int(date_str))

		# 2017 + 2019 data
		val = get_value_safe(date_str, lambda x: datetime.strptime(x, '%d/%m/%Y'))
		
		if val is None:
			# 2018 data
			val = get_value_safe(date_str, lambda x: datetime.strptime(x, '%Y-%m-%d'))

			if val is None:
				val = get_value_safe(date_str, lambda x: datetime.strptime(x, '%Y%m%d'))
				
				if val is None:
					return None

		# localize the naive datetime object
		return pytz.utc.localize(val)

	def _get_int_obj(self, str_obj):
		return get_value_safe(str_obj, int, 0)

	def _get_float_obj(self, str_obj):
		return get_value_safe(str_obj, float, 0)

	def _filter_dst(self, dst_name):
		if dst_name != 'No Dest' and len(dst_name) == 3:
			return dst_name
		else:
			return None

	def _handle_email_plus_name(self, name, email, personal_ids, identifiers):
		# validate email
		# TODO: Fix email typos somehow
		if (email != '') and (email != None) and ('@' in email):
			email = email.lower()
			if email.endswith(';'):
				email = email[:-1]
			if email.startswith(';'):
				email = email[1:]

			# check for multiple emails
			if ';' in email:
				emails = email.split(';')
				for email_address in emails:
					if '@' not in email_address:
						emails.remove(email_address)
				emails = list(filter(lambda x: x, emails))
			else:
				emails = [email]

			for email_address in emails:
				personal_ids.append(email)
				identifiers.append(name+email_address)

			if len(emails) == 0:
				self._no_proper_mail_counter += 1

	def _handle_phone_number_plus_name(self, name, phone_num, personal_ids, identifiers):
		# validate phone number
		if (phone_num != '') and (phone_num != None) and (9 <= len(phone_num) <= 10):
			# too good of a number - suspicious
			if phone_num.count(phone_num[3]) > 6:
				self._no_proper_phone_counter += 1
				return

			is_valid_phone_num = False
			if phone_num.startswith('0') == False:
				if 9 == len(phone_num):
					phone_num = '0' + phone_num
					is_valid_phone_num = True
			elif 10 == len(phone_num) and phone_num.startswith('0'):
				is_valid_phone_num = True

			if is_valid_phone_num:
				personal_ids.append(phone_num)
				identifiers.append(name+phone_num)
			else:
				self._no_proper_phone_counter += 1
		else:
			self._no_proper_phone_counter += 1

	def _discard_junk_data(self):
		# discard unreal ids, representing too many reservations in one year
		unreal_ids = ['Email', 'Phone_Num']
		for id in unreal_ids:
			# count number of appearances
			value_counts = self._raw_data[id].value_counts()
			# extract the junk ids that appear over a certain number of times
			junk_ids = value_counts[value_counts.gt(consts.max_id_appearance_per_year.value)]
			# replace all the junk ids with a None value
			if junk_ids.size > 0:
				logging.info('Dumping {} junking {}s that appeared too many times'.format(str(junk_ids.size), id))
				self._raw_data[id].replace(junk_ids.index, [None] * junk_ids.size, inplace=True)

		# discard negative values that don't make sense
		non_negative_fields = ['Reserv_Income_Gross','Male_Cnt', 'Female_Cnt', 'Chd_Cnt', 'Inf_Cnt', 'Days_Count',
							   'Meal_Cnt', 'Seat_Cnt', 'LuggageQty']
		for field in non_negative_fields:
			self._raw_data.loc[self._raw_data[field] < 0, field] = 0

		# discard too long name values
		length_restricted_fields = ['FirstName', 'Surname']
		for field in length_restricted_fields:
			self._raw_data.loc[self._raw_data[field].str.len() > consts.max_name_length.value, field] = ''

		# discard non-realistic date of birth
		self._raw_data.loc[self._raw_data['DOB'].dt.year < consts.min_year_of_birth.value, 'DOB'] = np.nan

		# discard non M/F gender
		self._raw_data.loc[~self._raw_data['Gender'].isin(['m', 'f']), 'Gender'] = np.nan

	def _load_data(self):
		for key, func in self._raw_converters.items():
			self._raw_data[key] = self._raw_data[key].apply(func)

		self._discard_junk_data()
		ids_blacklist = []
		for row in self._raw_data.itertuples(index=False):
			try:
				dict_row = row._asdict()

				status = dict_row.get('Status_Key', None)
				if status == 'XL':
					self._no_status_ok_counter += 1
					logging.info('Discard row because of STATUS CANCELED. data row as dict:{}'.format(dict_row))
					# canceled reservation - skip
					continue

				dict_row['Destination_Name'] = AIRPORT_DATA.get(dict_row['Destination_Key'], {}).get('municipality', '')
				first_name = dict_row['FirstName']
				last_name = dict_row['Surname']

				# pandas converts all missing/invalid name fields to an empty string
				if first_name == '':
					# no name - no profile
					self._no_first_name_counter += 1
					logging.info('Discard row because of FIRST NAME PROBLEM. data row as dict:{}'.format(dict_row))
					continue
				if last_name == '':
					# no name - no profile
					self._no_last_name_counter += 1
					logging.info('Discard row because of LAST NAME PROBLEM. data row as dict:{}'.format(dict_row))
					continue

				full_name = '{first_name} {last_name}'.format(first_name=first_name, last_name=last_name)
				email = dict_row['Email']
				phone_num = dict_row['Phone_Num']

				if email is None and phone_num is None:
					# no phone and no email - no profile
					self._no_mail_and_phone_counter += 1
					logging.info('Discard row because of EMAIL AND PHONE PROBLEM. data row as dict:{}'.format(dict_row))
					continue

				# we have 2 types of identifiers:
				# 	(1) "personal_ids": non-unique (e.g. email, phone, name)
				# 	(2) "identifiers": unique (e.g. name+phone, name+email)
				personal_ids = []
				identifiers = []
				self._handle_email_plus_name(full_name, email, personal_ids, identifiers)
				self._handle_phone_number_plus_name(full_name, phone_num, personal_ids, identifiers)
				personal_ids.append(full_name)

				if len(identifiers) == 0:
					self._no_identifiers_counter += 1
					logging.info('Discard row because of NO IDENTIFIERS. data row as dict:{}'.format(dict_row))
					continue

				# check if profile is blacklisted
				is_profile_blacklisted = False
				for identifier in identifiers:
					if identifier in ids_blacklist:
						is_profile_blacklisted = True
						break
				if is_profile_blacklisted:
					self._blacklisted_profile_counter += 1
					logging.info('Discard row because of KNOWN BLACKLISTED PROFILE. data row as dict:{}'.format(dict_row))
					continue

				# Assume new profile, then try to find existing profile
				matching_profile_num = self._profile_counter
				is_new_profile = True

				for identifier in identifiers:
					if identifier in self._identifier_to_profile_num:
						matching_profile_num = self._identifier_to_profile_num[identifier]
						is_new_profile = False

				# New profile
				if is_new_profile:
					# counter is only used for statistics
					self._unique_counter += 1

					self._data[matching_profile_num] =\
						{ 'customer_data': [dict_row],
						  'identifiers': identifiers,
						  'personal_ids' : personal_ids }

					for identifier in identifiers:
						self._identifier_to_profile_num[identifier] = matching_profile_num
					self._profile_counter += 1

				# Existing profile
				else:
					# check if this is a guide reservation, and this is recurring - blacklist the guide
					if len(self._data[matching_profile_num]['customer_data']) >= 2:
						price = float(dict_row.get('Reserv_Income_Gross', None))
						pnr_product = dict_row.get('Pnr_Product', None)
						if price is not None and pnr_product is not None and price == 0.0 and pnr_product == 'TOUR:NATOUR':
									ids_blacklist.extend(identifiers)
									ids_blacklist = list(set(ids_blacklist))
									# mark the profile as a tour guide so we'd know to ignore it
									self._data[matching_profile_num]['is_tour_guide'] = True
									self._blacklisted_profile_counter += 1
									logging.info('Discard row because of NEW BLACKLISTED PROFILE. data row as dict:{}'.format(dict_row))
									continue

					# counter is only used for statistics
					self._match_counter += 1

					self._data[matching_profile_num]['customer_data'].append(dict_row)

					all_identifiers = identifiers + self._data[matching_profile_num]['identifiers']
					updated_identifiers = list(set(all_identifiers))
					self._data[matching_profile_num]['identifiers'] = updated_identifiers

					all_personal_ids = personal_ids + self._data[matching_profile_num]['personal_ids']
					updated_personal_ids = list(set(all_personal_ids))
					self._data[matching_profile_num]['personal_ids'] = updated_personal_ids

			except Exception as exc:
				traceback.print_stack()
				logging.error(str(exc))

	def _get_proprietary_tags(self):
		for profile_number, all_data in self._data.items():
			ids = all_data['identifiers']
			tags = copy.deepcopy(get_all_tags())
			generic = {}
			spends_per_journey = []

			# tag tour guide
			if all_data.get('is_tour_guide', False):
				increment_tag(tags, TourGuideTags.TourGuide, self._origin)

			# Recurrence Counter - how many reservations did this profile have
			reservations_num = len(all_data['customer_data'])
			increment_tag(tags, RecurringTravelerTags.RecurrenceCounter, self._origin, reservations_num)

			for data in all_data['customer_data']:
				adults_count = data['Male_Cnt'] + data['Female_Cnt']
				children = data['Chd_Cnt']
				if consts.min_price_per_reservation.value < data['Reserv_Income_Gross']:
					if 0 == adults_count + children:
						spends_per_journey.append(data['Reserv_Income_Gross'])
					else:
						spends_per_journey.append(data['Reserv_Income_Gross'] / (adults_count + children))

				# City Tags
				city_airport = data.get('Destination_Key', '')
				if city_airport:
					city_airport = city_airport.upper()
					if city_airport in airport_keys:
						increment_tag(tags, AirportCityTag[city_airport], self._origin)

					# Relevant tags by city
					city_tags = ISRAIR_CATALOG_TAGGING.get(city_airport, [])
					for tag in city_tags:
						increment_tag(tags, tag, self._origin)

				# Reservation flow
				if 'Direct' == data['Agency_Type_Name']:
					if 'ISRAIR DIRECT' == data['Agency_Name']:
						increment_tag(tags, ReservationTags.CallCenter, self._origin)

					elif 'ISRAIR INTERNET' == data['Agency_Name']:
						increment_tag(tags, ReservationTags.DirectWeb, self._origin)

					else:
						increment_tag(tags, ReservationTags.GenericDirect, self._origin)

				# Booked via OTAs
				elif 'GROUPON' in data['Agency_Name']:
					increment_tag(tags, ReservationTags.OTA, self._origin)
					increment_tag(tags, ReservationTags.Coupons, self._origin)
				
				elif 'SMARTAIR' in data['Agency_Name']:
					increment_tag(tags, ReservationTags.OTA, self._origin)

				# Booked via travel agency
				else:
					increment_tag(tags, ReservationTags.Indirect, self._origin)

				# Spa Trip
				if 'SPA PACKAGE' == data['Pnr_Product']:
					increment_tag(tags, ExperienceTags.Spa, self._origin)

				# Summer School
				if 'TOUR:SUMMER SCHOOL' == data['Pnr_Product']:
					increment_tag(tags, ExperienceTags.SummerSchool, self._origin)

				# Trip duration tags
				trip_duration = data.get('Days_Count', None)
				if trip_duration:
					if trip_duration <= 4:
						increment_tag(tags, TripDurationTags.short, self._origin)
					elif trip_duration <= 14:
						increment_tag(tags, TripDurationTags.moderate, self._origin)
					else:
						increment_tag(tags, TripDurationTags.long, self._origin)

				# Flight Only
				if 'FLIGHT ONLY' == data['Pnr_Product']:
					increment_tag(tags, BundlePackageTags.FlightOnly, self._origin)

				# Flight + Hotel Package
				if 'PACKAGE HOLIDAY' == data['Pnr_Product']:
					increment_tag(tags, BundlePackageTags.FlightAndHotel, self._origin)

				# Flight + Hotel Package
				if 'CITY PACKAGE' == data['Pnr_Product']:
					increment_tag(tags, BundlePackageTags.FlightAndHotel, self._origin)

				if 'TOUR:NATOUR' == data['Pnr_Product']:
					increment_tag(tags, TravellerGroupTags.OrganizedTour, self._origin)

				# GROUP Reservation
				if 'GROUP' == data['Pnr_Product'] or adults_count > consts.max_family_group_adults.value:
					increment_tag(tags, TravellerGroupTags.GroupTrip, self._origin)

				# non-group traveler tags
				else:
					if 0 == data['Chd_Cnt'] and 0 == data['Inf_Cnt']:
						# up to 4 couples can travel together
						if data['Female_Cnt'] == data['Male_Cnt'] and adults_count < 10:
							increment_tag(tags, TravellerGroupTags.CoupleTraveller, self._origin)
						elif 1 == adults_count:
							increment_tag(tags, TravellerGroupTags.SingleTraveller, self._origin)
						elif 1 < data['Male_Cnt'] and 0 == data['Female_Cnt']:
							increment_tag(tags, TravellerGroupTags.BoysTrip, self._origin)
						elif 1 < data['Female_Cnt'] and 0 == data['Male_Cnt']:
							increment_tag(tags, TravellerGroupTags.GirlsTrip, self._origin)

					elif 1 <= adults_count <= consts.max_family_group_adults.value and (0 < data['Chd_Cnt'] or 0 < data['Inf_Cnt']):
						increment_tag(tags, TravellerGroupTags.FamilyTraveller, self._origin)

					if 0 < data['Chd_Cnt']:
						if 6 > data['Chd_Cnt']:
							tag = TravellerGroupTags['Children' + str(data['Chd_Cnt'])]
							increment_tag(tags, tag, self._origin)

						else:
							increment_tag(tags, TravellerGroupTags.ChildrenM, self._origin)

					if 0 < data['Inf_Cnt']:
						if 3 > data['Inf_Cnt']:
							tag = TravellerGroupTags['Infants' + str(data['Inf_Cnt'])]
							increment_tag(tags, tag, self._origin)

						else:
							increment_tag(tags, TravellerGroupTags.InfantsM, self._origin)

				# Kosher
				if 'kosher' in data['Hotel_Name'] or \
								'KOSHER HOLIDAYS LTD' == data['Agency_Name']:
					increment_tag(tags, ExperienceTags.Kosher, self._origin)

				# Age range
				if pandas.isnull(data['DOB']) == False:
					age = round((pytz.utc.localize(datetime.now()) - data['DOB']).days / 365)
					if age < 2:
						generic[GenericKeys.AgeRange.value] = GenericValues.AgeRange2.value
					elif age < 13:
						generic[GenericKeys.AgeRange.value] = GenericValues.AgeRange12.value
					elif age < 19:
						generic[GenericKeys.AgeRange.value] = GenericValues.AgeRange18.value
					elif age < 45:
						generic[GenericKeys.AgeRange.value] = GenericValues.AgeRange45.value
					elif age < 67:
						generic[GenericKeys.AgeRange.value] = GenericValues.AgeRange67.value
					else:
						generic[GenericKeys.AgeRange.value] = GenericValues.AgeRange120.value

				# Language
				generic[GenericKeys.Languages.value] = [GenericValues.LanguageHebrew.value]
				for email in ids:
					for mail_prov in consts.russian_mails.value:
						if email.endswith(mail_prov):
							generic[GenericKeys.Languages.value].append(GenericValues.LanguageRussian.value)
							break

					for high_email in consts.hightech_emails.value:
						if high_email in email:
							generic[GenericKeys.Profession.value] = GenericValues.ProfessionHighTech.value
							break

				# Gender
				if 'm' == data['Gender']:
					generic[GenericKeys.Gender.value] = GenericValues.GenderMale.value
				elif 'f' == data['Gender']:
					generic[GenericKeys.Gender.value] = GenericValues.GenderFemale.value

				# Reservation time
				if pandas.isnull(data['Date_Key']) == False and pandas.isnull(data['Order_Date_Key']) == False:
					reservation_delta = (-1) * (data['Order_Date_Key'] - data['Date_Key'])
					# Negative days counter
					if reservation_delta.days <= 7*2:
						increment_tag(tags, ReservationTags.LastMinutePlanner, self._origin)

					elif reservation_delta.days < 7*4*3:
						increment_tag(tags, ReservationTags.ShortTermPlanner, self._origin)

					elif reservation_delta.days < 7*4*6:
						increment_tag(tags, ReservationTags.LongTermPlanner, self._origin)

					else:
						increment_tag(tags, ReservationTags.ExtraLongTermPlanner, self._origin)

				# Hotel Tags
				if 0 < data['Hotel_Stars']:
					if 3 == data['Hotel_Stars']:
						increment_tag(tags, HotelStyleTags.ThreeStarsHotel, self._origin)

					elif 4 == data['Hotel_Stars']:
						increment_tag(tags, HotelStyleTags.FourStarsHotel, self._origin)

					elif 5 == data['Hotel_Stars']:
						increment_tag(tags, HotelStyleTags.FiveStarsHotel, self._origin)

				# Ancillaries Tags
				if 0 < data['Meal_Cnt']:
					increment_tag(tags, FlightAncillariesTags.Meal, self._origin)

				if 0 < data['Seat_Cnt']:
					increment_tag(tags, FlightAncillariesTags.Seat, self._origin)

				if 0 < data['LuggageQty']:
					if adults_count < data['LuggageQty']:
						increment_tag(tags, FlightAncillariesTags.MultipleLuggage, self._origin)
					else:
						increment_tag(tags, FlightAncillariesTags.Luggage, self._origin)

			total_journeys = len(all_data['customer_data'])
			if 2 > total_journeys:
				generic[GenericKeys.Frequency.value] = GenericValues.FrequencyLow.value

			elif 4 > total_journeys:
				generic[GenericKeys.Frequency.value] = GenericValues.FrequencyMedium.value

			else:
				generic[GenericKeys.Frequency.value] = GenericValues.FrequencyHigh.value

			avg_spend = int(sum(spends_per_journey) / total_journeys)
			if 200 > avg_spend:
				generic[GenericKeys.Budget.value] = GenericValues.BudgetLow.value

			elif 600 > avg_spend:
				generic[GenericKeys.Budget.value] = GenericValues.BudgetMedium.value

			else:
				generic[GenericKeys.Budget.value] = GenericValues.BudgetHigh.value

			generic = build_generics_with_origin(generic, self._origin)

			self._data[profile_number]['identifiers'] = ids
			self._data[profile_number]['tags'] = tags
			self._data[profile_number]['generic'] = generic

	
	def _write_csv_tags(self, tas_filename):
		self._output_tags_local.to_csv(tas_filename, index=False)

	def _build_travel_profiles_remote(self):
		for profile_number, data in self._data.items():
			try:
				# 1. Build primitives
				travel_primitives = self._get_travel_primitives(data['customer_data'])

				# 2. Get unique local_id from unique identifiers
				ids = data['identifiers']
				consumer_local_id = get_or_create_local_id('Israir Airlines LTD', identifiers=ids, should_hash=True)

				# 3. Update personal_ids dictionary (maps non-unique ids to all possible unique local_ids)
				personal_ids = data['personal_ids']
				BatchFirestoreWrite().update_personal_ids(consumer_local_id, personal_ids, should_hash=True)

				# 4. Build Profile from local_id and travel primitives
				profile = TravelerProfile(traveler_local_id=consumer_local_id)
				profile.update_last_source_update(OriginCompany.ISRAIR.value, datetime.now())
				profile.add_travel_primitives(travel_primitives)
			
				tagging_manager = TaggingManager(profile, OriginCompany.ISRAIR.name, OriginType.ON_PREM.name)
				tagging_manager.analyze()
				
				# 5. Merge proprietary tags to profiles' tags
				profile.merge_tags(data['tags'])
				profile.merge_generic_info(data['generic'])

				profile.update_remote_profile()

			except Exception as exc:
				logging.error(str(exc))
				traceback.print_exc()

	def _build_travel_profiles_local(self):
		self._output_tags_local = pandas.DataFrame()
		profile = TravelerProfile('Test User 1')
		for profile_number, data in self._data.items():
			try:
				# 1. Build primitives
				travel_primitives = self._get_travel_primitives(data['customer_data'])

				# 2. Build identity from ids

				# 3. Init dummy profile
				profile._timeline = []
				profile._metadata = {}
				profile._generic = {}
				profile._tags = get_all_tags()
				profile._timeline = []
				
				# 4. Build Profile from identity and travel primitives
				profile.add_travel_primitives(travel_primitives)
			
				tagging_manager = TaggingManager(profile, OriginCompany.ISRAIR.name, OriginType.ON_PREM.name)
				tagging_manager.analyze()
				
				# 4. Merge proprietary tags to profiles' tags
				profile.merge_tags(data['tags'])
				profile.merge_generic_info(data['generic'])

				output_dict = {	'internal_profile_number' : str(profile_number),
								   'personal_ids': data['personal_ids'],
								   'identifiers' : data['identifiers']}
				
				for key, value in profile.get_generic_info().items():
					output_dict[key] = value['data']
				
				all_tags = profile.get_tags()
				for category in TAG_CATEGORIES:
					for tag in category:
						if all_tags[category.__name__][tag.value]['data'] != 0:
							output_dict[tag.value] = all_tags[category.__name__][tag.value]['data']

				self._output_tags_local = self._output_tags_local.append(output_dict, ignore_index=True)

			except Exception as exc:
				logging.error(str(exc))
				traceback.print_exc()

	def _get_travel_primitives(self, customer_data):
			travel_primitives = []

			for data in customer_data:
				# Extract flights, assume no hotel without flight
				if data['Destination_Key'] == 'No Dest':
					continue

				if pandas.isnull(data['Destination_Name']):
					continue

				if pandas.isnull(data['Days_Count']) or data['Days_Count'] == 0:
					continue

				dst_airport = data['Destination_Key']
				dst_city = data['Destination_Name']
				start = data['Date_Key']
				duration = timedelta(days=data['Days_Count'])

				src_city = 'Tel Aviv'
				src_airport = 'TLV'
				outbound_flight_num = ''
				inbound_flight_num = ''

				outbound_flight = Flight(start, start, src_city, src_airport, dst_city, dst_airport, 
										outbound_flight_num, self._origin)
				inbound_flight = Flight(start+duration, start+duration, dst_city, dst_airport, 
										src_city, src_airport, inbound_flight_num, self._origin)
				travel_primitives.extend([outbound_flight, inbound_flight])

				# Extract hotel
				if pandas.isnull(data['Hotel_Name']) == False and data['Hotel_Name'] != '':
					accommodation = Accommodation(start, start+duration, data['Hotel_Name'], data['Hotel_City'],
													stars=data['Hotel_Stars'], data_origin=self._origin)
					travel_primitives.append(accommodation)

			return travel_primitives

	def __str__(self):
		return '\n'.join([	'profiles: ' + str(len(list(self._data.keys())))])


# ------------------------------------------ #
# ------------------WTF--------------------- #
# ------------------------------------------ #
# 1._status_key --> cancelled ??
# 3. RESERVATIONS ISRAIR ( CALL CENTER ) --> 90% call center??
# 4. WTF ISRAIR EMPLOYEED, GROUP, LAND SERVICE, FULL CHARTER, CONCORENT, CITY PACKAGE vs PACKAGE HOLIDAY
# 5. destination_key == No Dest ?!
# 6. All flight details (==PNR?), -- >flight_num, src_city, src_airport 
# 7. Flight + Car ?
# ------------------------------------------ #
# ------------------------------------------ #
# ------------------------------------------ #