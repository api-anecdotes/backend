import logging


def get_value_safe(input_data, transform_func, default=None):
	if input_data == '' or input_data is None:
		return default
	try:
		return transform_func(input_data)
	except:
		return default