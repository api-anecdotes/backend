import logging
import traceback
from abc import ABC, abstractmethod
from Utils import cloud as cloud_utils


class GenericParser(ABC):
	def __init__(self, origin_company, origin_type, data, data_id):
		self._origin_company = origin_company
		self._origin = {origin_company: [origin_type]}
		self._profile_counter = 0
		self._raw_data = data
		self._data = {}
		self._data_id = data_id
		
		# Statistics
		self._match_counter  = 0
		self._unique_counter  = 0
		self._no_status_ok_counter = 0
		self._no_first_name_counter = 0
		self._no_last_name_counter = 0
		self._no_mail_and_phone_counter = 0
		self._no_identifiers_counter = 0
		self._blacklisted_profile_counter = 0
		self._no_proper_mail_counter = 0
		self._no_proper_phone_counter = 0

	# Orchestrating function, Note: exception less parsing (handled within process_single_file)
	def parse(self):
		if self._is_processed():
			return
			
		# Do not process single file more than once even upon errors
		self._mark_processed_data()		
		self._process_data()

	def _is_processed(self):
		data_info = cloud_utils.get_on_prem_process_info(self._data_id)
		if data_info != None:
			return True
		
		cloud_utils.BatchFirestoreWrite().update_on_prem_process_info(self._data_id)
		return False

	def _mark_processed_data(self):
		pass

	def _process_data(self):
		try:
			logging.info('Load data; data_id={}'.format(self._data_id))
			self._load_data()

			# Statistics
			all_missing_profiles = self._no_first_name_counter + \
								   self._no_last_name_counter + \
								   self._no_mail_and_phone_counter + \
								   self._no_identifiers_counter + \
								   self._no_status_ok_counter + \
								   self._blacklisted_profile_counter

			high_level_stats = "\n\nMatching rows counter: {}\nUnique profiles counter: {}\nDiscarded rows counter: {}". \
				format(str(self._match_counter), str(self._unique_counter), str(all_missing_profiles))
			logging.info(high_level_stats)

			final_log_message = "no_first_name_counter: {}.\n" \
								"no_last_name_counter: {}.\n" \
								"no_status_ok_counter: {}.\n" \
								"blacklisted_profile_counter: {}.\n" \
								"no_mail_and_phone_counter: {}\n" \
								"no_proper_mail_counter: {}.\n" \
								"no_proper_phone_counter: {}.\n" \
								"total_no_identifiers_counter: {}.\n".format(
				str(self._no_first_name_counter),
				str(self._no_last_name_counter),
				str(self._no_status_ok_counter),
				str(self._blacklisted_profile_counter),
				str(self._no_mail_and_phone_counter),
				str(self._no_proper_mail_counter),
				str(self._no_proper_phone_counter),
				str(self._no_identifiers_counter))
			logging.info(final_log_message)

			logging.info('Get proprietry tags; data_id={}'.format(self._data_id))
			self._get_proprietary_tags()

			# logging.info('Build travel profiles; filename={}'.format(filename))
			self._build_travel_profiles_remote()

			# logging.info('Build travel profiles; data_id={}'.format(self._data_id))
			# self._build_travel_profiles_local()

			logging.info('Done; data_id={}'.format(self._data_id))

		except Exception as exc:
			logging.error('Failed to parse file; data_id={}; exc={}'.format(self._data_id, str(exc)))
			traceback.print_exc()

	#--------- TO BE IMPLEMENTED BY PARSERS ---------#
	@abstractmethod
	def _load_data(self):
		pass

	@abstractmethod
	def _get_proprietary_tags(self):
		pass

	@abstractmethod
	def _build_travel_profiles_remote(self):
		pass

	@abstractmethod
	def _build_travel_profiles_local(self):
		pass

	@abstractmethod
	def _write_csv_tags(self, tas_filename):
		pass
	#-----------------------------------------------#